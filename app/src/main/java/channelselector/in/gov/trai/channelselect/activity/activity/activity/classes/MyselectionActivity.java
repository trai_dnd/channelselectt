package channelselector.in.gov.trai.channelselect.activity.activity.activity.classes;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import channelselector.in.gov.trai.channelselect.R;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.Extras.Global;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.adapters.RecyclerMyChoice;


public class MyselectionActivity extends AppCompatActivity {
    int Freeqty,paidqty,bouquetqty,uniqueChannel;
    String paidchannel,baouquetchannel;
    double totalPrice;
    private TextView tv_free_qty,tv_paid_qty,tv_bouquet_qty,tv_price_paid,tv_bouquet_paid,tv_price_monthly,tv_price_ncf,tv_qty_unique,tv_price_free;
    ArrayList<String> arrChannels = new ArrayList<String>();
    ArrayList<String> arrImageUrl = new ArrayList<String>();
    List<String> listDataHeader;
    private RecyclerView recycler_mychoice;
    private RecyclerMyChoice adapter;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myselection);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Freeqty = Global.Freechannels;
        paidqty = Global.PaidChannels;
        bouquetqty = Global.TotalBouquet;
        uniqueChannel = Global.channelCount;
        totalPrice =Global.TotalSubscriptionPrice;
        paidchannel = String.valueOf(Double.parseDouble(new DecimalFormat("##.##").format(Global.paidchannelPrice)));
        baouquetchannel = String.valueOf(Global.bouquetprice);
        view();
        setvalue();
        setHashMap();
    }

    private void setHashMap() {
        arrChannels.add("DD Bangla");
        arrChannels.add("DD Bharati");
        arrChannels.add("DD Bihar");
        arrChannels.add("DD Chandana");
        arrChannels.add("DD Girnar");
        arrChannels.add("DD India");
        arrChannels.add("DD Kashir");
        arrChannels.add("DD Kisan");
        arrChannels.add("DD Madhya Pradesh");
        arrChannels.add("DD Malayalam");
        arrChannels.add("DD National");
        arrChannels.add("DD News");
        arrChannels.add("DD North East");
        arrChannels.add("DD Oriya");
        arrChannels.add("DD Podhigai");
        arrChannels.add("DD Punjabi");
        arrChannels.add("DD Rajasthan");
        arrChannels.add("DD Sahyadri");
        arrChannels.add("DD Saptagiri");
        arrChannels.add("DD Sports");
        arrChannels.add("DD Urdu");
        arrChannels.add("DD Uttar Pradesh");


        // imageUrls...

        arrImageUrl.add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcShhgEo4TZwX5B2OlxnypPSi1hhnBRNbXMSehSul_CY_wDWXL1WuDwqag-T");
        arrImageUrl.add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR7awt0W_Sz52PwPG2QG5ClCE6EXiV7UpvR6FEjEws-H6LFFQRfXizPGc4D");
        arrImageUrl.add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS1Ls2tGthYXBbiIRIKeSrHeJVdWqDvpr2bxjJ8bZcQ9SBM9pP01m_plR0N");
        arrImageUrl.add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTir3DnOD61K1Nn3EcW-WDh6mcBCdXjl7ipgNG3SAQgdt1R1IL5K3FXZhzr8g");
        arrImageUrl.add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTA_kPfhyRl7s7Kj5qzA1W2_Kpe_Y2kdEKaACaJPS5Z3O7VO1e1xxdvTJk");
        arrImageUrl.add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR8JRUck2pkUDdYiCg6yM1PaHYOJH6yk0TF-IqHpAVAKL9Ui2Hm38LCQQ");
        arrImageUrl.add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSSuPSdR6onPQUFZYxnZOxof8vbVfmoVgdKJsqtPBTn3ko-VEWTOPe_3Q");
        arrImageUrl.add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSkpKAeTGASeLrFNWyGC-EB-hJ8nXyfghFzb4lqiSF5tA21A11WQCgQCwZQ");
        arrImageUrl.add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRpH8j2WLzczGKNib3JipJQteMy8BJrrmy2INoS112HkYHowpjXafiP5KI");
        arrImageUrl.add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTqv01LF-NQiNmoV7eo6gR6WhzJnz_alVS7Gw_83BE6vs14W-MbC1h3XEtNdQ");
        arrImageUrl.add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT_ViO2rfzFOEogMskxs-Zau_T2WOI84nYTby0djiLe1ekVA4J0kjLJ2oA");
        arrImageUrl.add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSqPiCgMKgTPUPD8oSWuc2iotFvPoun7PRvlJWBIXTUaRCWr-D3KzniwOms");
        arrImageUrl.add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR_cMK6y-L-pbp4PLU4Ej8UOyVpbyqByOm09ex7NR3XZYkfzvuQx5UQOi0");
        arrImageUrl.add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRotLKTDOip2UTzs1QDqEhyEE4ujE9NrpcqY2q7nMOUoLIqchzxFdhIrKI7");
        arrImageUrl.add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSqaikwkceM8DBqzcAJteIYcqreesVZo0VOeQx24LloRcPiAVVAl2wawgE");
        arrImageUrl.add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQEjELC-yvjRiVojIAoGUfEGt6tBzFpZeJbv2pbxvtSrscf-Etz7pOwpnFg");
        arrImageUrl.add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRmo-tbI5CIvjwMC6_U_F_2IRZ6P5PZaR_6qopB4o40LPG-aFbA349N0fmBXw");
        arrImageUrl.add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQH872RTi67WVTlIm-GspNGqQkI4aIHeKwXCr0O7SdCK3r06cwC1LcfmMs");
        arrImageUrl.add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQTWzWzv7e9-6x1RVRuAe8jQfW42n6d_2sg2WWTVkTBAv11h1aDZYI5kg");
        arrImageUrl.add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR_nN5jGB6G-moYzIrUa11clachPU4-8uHCI0nm8o1kAAZt_mkv5SfLzpM");
        arrImageUrl.add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS2lM8HeqZaH4O1B73gh1lBCqSLy0iZ03tTJTozGbkW6sg0XNyUOIkMKp0");
        arrImageUrl.add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTps_xHXmjg7jbtp1Mq8kh7h8dgJ7Plrq3DgH6Sz2FclsgfJ2m1mQwusQ");

        listDataHeader = new ArrayList<String>();
        listDataHeader.add("*Mandatory Channels");

        adapter = new RecyclerMyChoice(MyselectionActivity.this,arrChannels,arrImageUrl,listDataHeader);
        recycler_mychoice.setAdapter(adapter);

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();

                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    private void setvalue() {
        tv_free_qty.setText(String.valueOf(Freeqty+25));
        tv_paid_qty.setText(String.valueOf(paidqty));
        tv_bouquet_qty.setText(String.valueOf(bouquetqty));
        tv_qty_unique.setText(String.valueOf(uniqueChannel));
        tv_price_paid.setText(getResources().getString(R.string.Rs)+" "+paidchannel);
        tv_bouquet_paid.setText(getResources().getString(R.string.Rs)+" "+baouquetchannel);
        tv_price_free.setText((getResources().getString(R.string.Rs)+" "+0.0));
        tv_price_monthly.setText(String.valueOf(getResources().getString(R.string.Rs)+" "+totalPrice));
        if(uniqueChannel<100) {
            tv_price_ncf.setText(getResources().getString(R.string.Rs) + " " + "130");
        }else{
            double count = getuniquechannelcount(uniqueChannel);
            tv_price_ncf.setText(String.valueOf(getResources().getString(R.string.Rs) + " " +Double.parseDouble(new DecimalFormat("##.##").format(count))));
        }
    }

    private double getuniquechannelcount(double length) {
        if(length<100){
            return 130;
        }else{
            return 130 + Math.ceil((length-100)/25)*20;
        }
    }

    private void view() {

        tv_free_qty = (TextView)findViewById(R.id.tv_free_qty);
        tv_paid_qty = (TextView)findViewById(R.id.tv_paid_qty);
        tv_bouquet_qty = (TextView)findViewById(R.id.tv_bouquet_qty);
        tv_price_paid = (TextView)findViewById(R.id.tv_price_paid);
        tv_bouquet_paid = (TextView)findViewById(R.id.tv_bouquet_paid);
        tv_price_monthly = (TextView)findViewById(R.id.tv_price_monthly);
        tv_price_ncf     = (TextView)findViewById(R.id.tv_price_ncf);
        tv_qty_unique    = (TextView)findViewById(R.id.tv_qty_unique);
        recycler_mychoice = (RecyclerView)findViewById(R.id.recycler_mychoice);
        tv_price_free = (TextView)findViewById(R.id.tv_price_free);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recycler_mychoice.setLayoutManager(layoutManager);
    }
}

