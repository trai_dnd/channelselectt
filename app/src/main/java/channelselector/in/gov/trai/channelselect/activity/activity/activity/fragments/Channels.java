package channelselector.in.gov.trai.channelselect.activity.activity.activity.fragments;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.RelativeLayout;

import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import channelselector.in.gov.trai.channelselect.R;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.DTO.ChannelDto;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.Extras.Const;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.Extras.Global;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.adapters.ExpandableChannelsAdapter;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.classes.Review;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.webservicehandler.GetWebService;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.webservicehandler.HeaderGet;

public class Channels extends Fragment {
    private RelativeLayout mainLay;
    List<String> listDataHeader;
    HashMap<String, ArrayList<ChannelDto>> listDataChild;
    HashMap<String, ArrayList<ChannelDto>> listDataChildpaid;
    HashMap<String, ArrayList<ChannelDto>> listDataChildfree;
    private ArrayList<HashMap<String,String>>arr_Data = new ArrayList<HashMap<String,String>>();
    private ArrayList<HashMap<String,String>>arr_freeData = new ArrayList<HashMap<String,String>>();
    private ArrayList<ChannelDto>channels = new ArrayList<ChannelDto>();
    private ExpandableChannelsAdapter adapter;
    private ExpandableListView expandable_channel;
    String Flag ,BouquetFlag;
    private SearchView searchView = null;
    private SearchView.OnQueryTextListener queryTextListener;
    private RelativeLayout rel_bottom;
    String operator,FlagSucess,accessToken;
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(
                R.layout.activity_channels, container, false);
        view(v);
        operator    = Prefs.getString(Const.OPERATORNAME, "operator");
        accessToken = Prefs.getString(Const.ACCESSTOKEN, "token");
        getChannels();
        events();
        //operator = Global.operatorname;
        /*view(v);
        getChannels();
        events();*/
        return v;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    private void view(View v) {
        mainLay = (RelativeLayout)v.findViewById(R.id.mainLay);
        expandable_channel = (ExpandableListView)v.findViewById(R.id.expandable_channel);
        rel_bottom = (RelativeLayout)v.findViewById(R.id.rel_bottom);
    }

    private void events() {

        rel_bottom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getActivity(),Review.class));

                /*cofirmaddedData =Global.confirmAddedData;
                if(cofirmaddedData!=null && cofirmaddedData.size()>0){
                    startActivity(new Intent(getActivity(),Review.class));
                }else{
                     Toast.makeText(getActivity(),"Please select any channel for add", Toast.LENGTH_SHORT).show();
                }*/


            }
        });
    }

    private void getChannels() {
        Flag = Global.addtionPartFlag;
        BouquetFlag = Global.addtionalBouquetFlag;
        FlagSucess = Global.success;
        if (FlagSucess != null && FlagSucess.equalsIgnoreCase("setSuccess")) {
            if (Const.isNetworkAvailable(getActivity())) {

                new GetChannelTask().execute();
            } else {
                Const.messageViewRe(mainLay, getString(R.string.network));
            }
        }
        else if(Flag!=null && Flag.equalsIgnoreCase("Remove Added Channel")){
            listDataHeader = new ArrayList<String>();
            listDataHeader.add("Paid Channel");
            listDataHeader.add("Free Channel");
            listDataChild = Global.channelData;
            /* Flag= "used";*/
            Global.addtionPartFlag = Flag;
            channels = Global.setChannels;
            adapter = new ExpandableChannelsAdapter(getActivity(),listDataChild,listDataHeader, channels);
            expandable_channel.setAdapter(adapter);
            expandable_channel.expandGroup(0);

        }else if (BouquetFlag!=null && BouquetFlag.equalsIgnoreCase("Remove Added Bouquet")){
            listDataHeader = new ArrayList<String>();
            listDataHeader.add("Paid Channel");
            listDataHeader.add("Free Channel");
            listDataChild = Global.channelData;
            /* Flag= "used";*/
            Global.addtionPartFlag = Flag;
            channels = Global.setChannels;
            adapter = new ExpandableChannelsAdapter(getActivity(),listDataChild,listDataHeader, channels);
            expandable_channel.setAdapter(adapter);
            expandable_channel.expandGroup(0);
        }else {

            if(Global.setChannels!=null && Global.setChannels.size()>0){

                listDataHeader = new ArrayList<String>();
                listDataHeader.add("Paid Channel");
                listDataHeader.add("Free Channel");

                listDataChild = new HashMap<String, ArrayList<ChannelDto>>();
                listDataChild.put("Free Channel",Global.freedtochannel);
                listDataChild.put("Paid Channel",Global.paidDtochannel);


                channels = Global.setChannels;
                adapter = new ExpandableChannelsAdapter(getActivity(),listDataChild,listDataHeader, channels);
                expandable_channel.setAdapter(adapter);
                expandable_channel.expandGroup(0);


            }else{
                if (Const.isNetworkAvailable(getActivity())) {

                    new GetChannelTask().execute();
                } else {
                    Const.messageViewRe(mainLay, getString(R.string.network));
                }
            }


        }

    }

    ProgressDialog progressDialog;

    public class GetChannelTask extends AsyncTask<String, Void, String> {
        String url;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if(operator.equalsIgnoreCase("Tata Sky")){
                url = Const.getChannelsTata;
            }else{
                url = Const.getChannels;
            }


            Log.e("URL=>", "" + url);
            progressDialog = Const.loader(progressDialog, getActivity());
        }

        @Override
        protected String doInBackground(String... strings) {

            HeaderGet o = new HeaderGet();
            String res = null;
            try {
                res = o.getData(url,accessToken);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return res;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            Log.e("RES=>", "" + s);
            try {
                String result = s;
                JSONObject object = new JSONObject(result);
                String status = object.getString("status");
                String channel_id;
                String Broadcaster;
                String Channel ;
                String category;
                String language;
                String HD;
                String ImageUrl;
                listDataHeader = new ArrayList<String>();
                ArrayList<ChannelDto> top250 = new ArrayList<ChannelDto>();
                ArrayList<ChannelDto> paidDto = new ArrayList<ChannelDto>();
                ArrayList<ChannelDto> freeDto = new ArrayList<ChannelDto>();
                ArrayList<String>arr_bouquet = new ArrayList<String>();
                listDataChildfree = new HashMap<String, ArrayList<ChannelDto>>();
                listDataChildpaid = new HashMap<String, ArrayList<ChannelDto>>();
                if (status.equalsIgnoreCase("200")) {
                    JSONArray jsonArray = object.getJSONArray("channels");
                    for(int i =0 ; i<jsonArray.length();i++){
                        JSONObject obj = jsonArray.getJSONObject(i);
                        String price   = obj.getString("price");

                        if(price.equalsIgnoreCase("0.0")){
                            channel_id  = obj.getString("channel_id");
                            Broadcaster = obj.getString("broadcaster");
                            Channel = obj.getString("channel");
                            category        = obj.getString("category");
                            language  = obj.getString("language");
                            price   = obj.getString("price");
                            ImageUrl   = obj.getString("imageurl");
                            HD        = obj.getString("hd");
                            if(ImageUrl.contains("http")){
                                //do nothing...
                            }else{
                                ImageUrl = "http://59.179.24.45:8081"+"/"+obj.getString("imageurl");
                            }

                            ChannelDto freeData = new ChannelDto();
                            freeData.setChannel_id(channel_id);
                            freeData.setBroadcaster(Broadcaster);
                            freeData.setChannel(Channel);
                            freeData.setCategory(category);
                            freeData.setLanguage(language);
                            freeData.setPrice(price);
                            freeData.setImageurl(ImageUrl);
                            freeData.setHd(HD);
                            freeData.setSelected(false);
                            freeDto.add(freeData);
                            Global.newDataFree = freeDto;
                            //listDataChildfree.put("Free Channel", top250);

                            HashMap<String,String>freedata = new HashMap<String,String>();
                            freedata.put("Channel_id",channel_id);
                            freedata.put("Broadcaster",Broadcaster);
                            freedata.put("Channel", Channel);
                            freedata.put("category",category);
                            freedata.put("language",language);
                            freedata.put("HD",HD);
                            freedata.put("price",price);
                            freedata.put("ImageUrl",ImageUrl);
                            freedata.put("Selectedd","false");
                            arr_freeData.add(freedata);





                        }else{
                            channel_id  = obj.getString("channel_id");
                            Broadcaster = obj.getString("broadcaster");
                            Channel = obj.getString("channel");
                            category        = obj.getString("category");
                            language  = obj.getString("language");
                            price   = obj.getString("price");
                            ImageUrl   = obj.getString("imageurl");
                            HD        = obj.getString("hd");
                            if(ImageUrl.contains("http")){
                                //do nothing...
                            }else{
                                ImageUrl = "http://59.179.24.45:8081"+"/"+obj.getString("imageurl");
                            }


                            ChannelDto paidDtos = new ChannelDto();
                            paidDtos.setChannel_id(channel_id);
                            paidDtos.setBroadcaster(Broadcaster);
                            paidDtos.setChannel(Channel);
                            paidDtos.setCategory(category);
                            paidDtos.setLanguage(language);
                            paidDtos.setHd(HD);
                            paidDtos.setPrice(price);
                            paidDtos.setImageurl(ImageUrl);
                            paidDtos.setSelected(false);
                            paidDto.add(paidDtos);
                            Global.newDataPaid = paidDto;
                            //listDataChildpaid.put("Paid Channel", top250);

                            HashMap<String,String>piadData = new HashMap<String,String>();
                            piadData.put("Channel_id",channel_id);
                            piadData.put("Broadcaster",Broadcaster);
                            piadData.put("Channel",Channel);
                            piadData.put("category",category);
                            piadData.put("language",language);
                            piadData.put("HD",HD);
                            piadData.put("price",price);
                            piadData.put("ImageUrl",ImageUrl);
                            piadData.put("Selectedd","false");
                            arr_Data.add(piadData);



                        }

                    }//end

                    listDataHeader.add("Paid Channel");
                    listDataHeader.add("Free Channel");
                    /*tDataChild = new HashMap<String, ArrayList<HashMap<String,String>>>();
                    tDataChild.put("Paid Channel", arr_Data);
                    tDataChild.put("Free Channel", arr_freeData);*/

                        if(freeDto!=null && freeDto.size()>0){
                             for(int i =0 ; i<freeDto.size();i++){
                                 String ch_id = freeDto.get(i).getChannel_id();
                                 if(Global.freemysubs!=null && Global.freemysubs.size()>0){
                                     for(int j =0; j<Global.freemysubs.size();j++){
                                         String channeId = Global.freemysubs.get(j).getChannel_id();
                                         if(ch_id.equals(channeId)){
                                             freeDto.remove(i);
                                         }
                                     }
                                 }
                             }
                        }

                    if(freeDto!=null && freeDto.size()>0){
                        for(int i =0 ; i<freeDto.size();i++){
                            String ch_id = freeDto.get(i).getChannel_id();
                            if(Global.freemysubs!=null && Global.freemysubs.size()>0){
                                for(int j =0; j<Global.freemysubs.size();j++){
                                    String channeId = Global.freemysubs.get(j).getChannel_id();
                                    if(ch_id.equals(channeId)){
                                        freeDto.remove(i);
                                    }
                                }
                            }
                        }
                    }

                    if(paidDto!=null && paidDto.size()>0){
                        for(int i =0 ; i<paidDto.size();i++){
                            String ch_id = paidDto.get(i).getChannel_id();
                            if(Global.paidmysubs!=null && Global.paidmysubs.size()>0){
                                for(int j =0; j<Global.paidmysubs.size();j++){
                                    String channeId = Global.paidmysubs.get(j).getChannel_id();
                                    if(ch_id.equals(channeId)){
                                        paidDto.remove(i);
                                    }
                                }
                            }
                        }
                    }

                    if(paidDto!=null && paidDto.size()>0){
                        for(int i =0 ; i<paidDto.size();i++){
                            String ch_id = paidDto.get(i).getChannel_id();
                            if(Global.paidmysubs!=null && Global.paidmysubs.size()>0){
                                for(int j =0; j<Global.paidmysubs.size();j++){
                                    String channeId = Global.paidmysubs.get(j).getChannel_id();
                                    if(ch_id.equals(channeId)){
                                        paidDto.remove(i);
                                    }
                                }
                            }
                        }
                    }

                    listDataChild = new HashMap<String, ArrayList<ChannelDto>>();
                    listDataChild.put("Free Channel",freeDto);
                    listDataChild.put("Paid Channel",paidDto);
                    if(freeDto!=null && freeDto.size()>0){
                        channels.addAll(freeDto);
                    }if(paidDto!=null && paidDto.size()>0){
                        channels.addAll(paidDto);
                    }

                    Global.setChannels = channels;

                    Global.channelData = listDataChild;
                    Global.searchMainChannelData = listDataChild;
                    Global.freedtochannel = freeDto;
                    Global.paidDtochannel = paidDto;
                    adapter = new ExpandableChannelsAdapter(getActivity(),listDataChild,listDataHeader,channels);
                    expandable_channel.setAdapter(adapter);
                    expandable_channel.expandGroup(0);


                } else {
                    String message = object.getString("message");

                    Const.messageViewRe(mainLay, message);
                }

            } catch (Exception e) {

            }

        }


    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main_menu, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);

        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));

            queryTextListener = new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextChange(String newText) {
                    Log.i("onQueryTextChange", newText);
                    String text = newText;
                    adapter.filterData(newText);
                    expandAll();
                    return false;
                }
                @Override
                public boolean onQueryTextSubmit(String query) {
                    Log.i("onQueryTextSubmit", query);
                    Const.hideKeyboard(getActivity());
                    adapter.filterData(query);
                    expandAll();
                    if(adapter.getGroupCount()==0) {
                        Const.messageViewRe(mainLay,getString(R.string.no_data__found));
                    }
                    return false;
                }
            };
            searchView.setOnQueryTextListener(queryTextListener);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void expandAll() {
        int count = adapter.getGroupCount();
        for (int i = 0; i < count; i++){
            expandable_channel.expandGroup(i);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                // Not implemented here
                return false;
            default:
                break;
        }
        searchView.setOnQueryTextListener(queryTextListener);
        return super.onOptionsItemSelected(item);
    }
}