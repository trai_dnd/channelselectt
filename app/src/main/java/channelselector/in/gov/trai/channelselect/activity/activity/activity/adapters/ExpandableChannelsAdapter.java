package channelselector.in.gov.trai.channelselect.activity.activity.activity.adapters;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.text.InputFilter;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import channelselector.in.gov.trai.channelselect.R;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.DTO.ChannelDto;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.Extras.Const;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.Extras.Global;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.classes.ChannelsBouquetActivity;


public class ExpandableChannelsAdapter extends BaseExpandableListAdapter {
    private LayoutInflater inflater;
    private Context _context;
    private List<String> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<String, ArrayList<ChannelDto>> _listDataChild;
    private HashMap<String, ArrayList<ChannelDto>> originalList = new HashMap<String, ArrayList<ChannelDto>>();
    private SparseBooleanArray expandState = new SparseBooleanArray();
    int child =0;
    private ArrayList<HashMap<String,String>>confirmData= new ArrayList<HashMap<String,String>>();
    private ArrayList<ChannelDto> filterDto;
    ArrayList<ChannelDto> freeDto ;
    ArrayList<ChannelDto> paidDto;
    int n = 0;
    int f = 0;
    double tot;

    public ExpandableChannelsAdapter(Context context, HashMap<String, ArrayList<ChannelDto>> listChildData,
                                     List<String> listDataHeader, ArrayList<ChannelDto> channels) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
        this.originalList = Global.channelData;
        this.filterDto = channels;
        inflater = LayoutInflater.from(context);
        for (int i = 0; i < listChildData.size(); i++) {
            expandState.append(i, false);
        }
        if(Global.channelData!=null && Global.channelData.size()>0){
            this._listDataChild = Global.channelData;
        }if(Global.confirmAddedData!=null && Global.confirmAddedData.size()>0){
            confirmData = Global.confirmAddedData;
        }if(Global.addPaidChannel>0){
            n = Global.addPaidChannel;
        }if(Global.addFreeChannel>0){
            f = Global.addFreeChannel;
        }if(Global.TotalSubscriptionPrice>0){
            tot = Global.TotalSubscriptionPrice;
        }if(f==0 && n==0 ){
            if(Global.TotalSubscriptionPrice>0){
                if(ChannelsBouquetActivity.getlayout_main()!=null){
                    ChannelsBouquetActivity.getlayout_main().setVisibility(View.VISIBLE);
                }else{
                    //do nohthing...
                }
                if(ChannelsBouquetActivity.getPaidText()!=null){
                    String set_paid = String.valueOf(Global.PaidChannels);
                    ChannelsBouquetActivity.getPaidText().setText(set_paid);
                }else{
                    // do nothing...
                }
                if(ChannelsBouquetActivity.getFreeText()!=null){
                    String set_free = String.valueOf(Global.Freechannels);
                    ChannelsBouquetActivity.getFreeText().setText(set_free+" +"+ 25);
                }else{
                    //do nothing...
                }
                if(ChannelsBouquetActivity.getTotalPrice()!=null){
                    ChannelsBouquetActivity.getTotalPrice().setText(String.valueOf(tot));
                }else{
                    // do nothing...
                }
            }else {
                if (ChannelsBouquetActivity.getlayout_main() != null) {
                    ChannelsBouquetActivity.getlayout_main().setVisibility(View.GONE);
                } else {
                    //do nohthing...
                }
            }
        }else{
            if(ChannelsBouquetActivity.getlayout_main()!=null){
                ChannelsBouquetActivity.getlayout_main().setVisibility(View.VISIBLE);
            }else{
                //do nohthing...
            }
            if(ChannelsBouquetActivity.getPaidText()!=null){
                String set_paid = String.valueOf(Global.PaidChannels);
                ChannelsBouquetActivity.getPaidText().setText(set_paid);
            }else{
                // do nothing...
            }
            if(ChannelsBouquetActivity.getFreeText()!=null){
                String set_free = String.valueOf(Global.Freechannels);
                ChannelsBouquetActivity.getFreeText().setText(set_free+"+"+25);
            }else{
                //do nothing...
            }
            if(ChannelsBouquetActivity.getTotalPrice()!=null){
                ChannelsBouquetActivity.getTotalPrice().setText(String.valueOf(tot));
            }else{
                // do nothing...
            }if(ChannelsBouquetActivity.getBouquetText()!=null){
                String total_bouquet = String.valueOf(Global.TotalBouquet);
                ChannelsBouquetActivity.getBouquetText().setText(total_bouquet);
            }
        }
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        //final String childText = (String) getChild(groupPosition, childPosition);
        child = childPosition;
       // final String price = _listDataChild.get(_listDataHeader.get(groupPosition)).get(childPosition).price;

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.row_expand, null);
        }
        final ImageView iv_channels       = (ImageView)convertView.findViewById(R.id.iv_channels);
        TextView  tv_channel     = (TextView) convertView.findViewById(R.id.tv_channel);
        TextView tv_price     = (TextView) convertView.findViewById(R.id.tv_price);
        TextView tv_language     = (TextView) convertView.findViewById(R.id.tv_language);
        TextView bouqet_channel_name = (TextView)convertView.findViewById(R.id.bouqet_channel_name);
        final CheckBox checkbox = (CheckBox)convertView.findViewById(R.id.checkbox);
        checkbox.setTag(_listDataChild.get(_listDataHeader.get(groupPosition)).get(childPosition));
        tv_channel.setFilters(new InputFilter[]{ new InputFilter.LengthFilter(25) });
        //  set data from global value.......//


        if(_listDataChild.get(_listDataHeader.get(groupPosition)).get(childPosition).getSelected()){
            checkbox.setChecked(true);
        }else{
            checkbox.setChecked(false);
        }
        checkbox.setTag(_listDataChild.get(_listDataHeader.get(groupPosition)).get(childPosition));
        checkbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (_listDataChild.get(_listDataHeader.get(groupPosition)).get(childPosition).getSelected()) {
                    _listDataChild.get(_listDataHeader.get(groupPosition)).get(childPosition).setSelected(false);
                    checkbox.setChecked(false);
                    String Channel = _listDataChild.get(_listDataHeader.get(groupPosition)).get(childPosition).getChannel();
                    String price = _listDataChild.get(_listDataHeader.get(groupPosition)).get(childPosition).getPrice();
                    String HD = _listDataChild.get(_listDataHeader.get(groupPosition)).get(childPosition).getHd();
                    String Flag = "Remove Added Channel";
                    //Global.addtionPartFlag = Flag;
                    if (confirmData != null && confirmData.size() > 0) {
                        for (int i = 0; i < confirmData.size(); i++) {
                            if (Channel.equalsIgnoreCase(confirmData.get(i).get("Channel"))) {
                                confirmData.remove(i);
                               // Global.confirmAddedData = confirmData;
                            }
                        }
                    }
                    if (price.equalsIgnoreCase("0.0")) {
                        if (f >= 0) {
                            if (f == 0) {
                                //do nothing...
                            } else {
                                f = f - 1;
                                int count = Global.channelCount-1;
                                Global.channelCount = count;
                                String freeChannellist = String.valueOf(n);
                                int freeChannel = Global.Freechannels-1;
                                String set_free = String.valueOf(freeChannel);
                                Global.addFreeChannel = Integer.parseInt(freeChannellist);
                                if(ChannelsBouquetActivity.getFreeText()!=null){
                                    ChannelsBouquetActivity.getFreeText().setText(set_free+"+"+25);
                                    Global.Freechannels = Integer.parseInt(set_free);
                                }else{
                                    // do nothing...
                                }
                            }
                        } else {
                            // do nothing...
                        }
                        if(Global.TotalSubscriptionPrice>0){
                            ChannelsBouquetActivity.getlayout_main().setVisibility(View.VISIBLE);
                        }else{
                            if(ChannelsBouquetActivity.getlayout_main()!=null){
                                ChannelsBouquetActivity.getlayout_main().setVisibility(View.GONE);
                            }else{
                                //do nohthing...
                            }
                        }

                    } else {
                        if (n >= 0) {
                            if (n == 0) {
                                //do nothing...
                            } else {
                                n = n - 1;
                                if(HD.equalsIgnoreCase("HD")){
                                    int count = Global.channelCount-2;
                                    Global.channelCount = count;
                                    Log.d("channel_count", String.valueOf(Global.channelCount));
                                }else{
                                    int count = Global.channelCount-1;
                                    Global.channelCount = count;
                                    Log.d("channel_count", String.valueOf(Global.channelCount));
                                }
                                String paychannellist = String.valueOf(n);
                                int payChannel =Global.PaidChannels-1;
                                String set_paid = String.valueOf(payChannel);
                                Global.addPaidChannel = Integer.parseInt(paychannellist);
                                if(ChannelsBouquetActivity.getPaidText()!=null){
                                    ChannelsBouquetActivity.getPaidText().setText(set_paid);
                                    Global.PaidChannels = Integer.parseInt(set_paid);
                                }else{
                                    // do nothing...
                                }
                            }

                            if(Global.TotalSubscriptionPrice>0){
                                tot = Global.TotalSubscriptionPrice;
                            }
                            double s = Double.parseDouble(price);
                            if(Global.channelCount<100){
                                Global.paidchannelPrice = Global.paidchannelPrice-s;
                                double Totalchannelbouquet = Global.paidchannelPrice+ Global.bouquetprice;
                                double Final = 130+Totalchannelbouquet;
                                double gt = Final*18/100;
                                double setPricetotal = Final+gt;
                                tot = setPricetotal;
                                Global.TotalSubscriptionPrice = Double.parseDouble(new DecimalFormat("##.##").format(setPricetotal));
                                if(ChannelsBouquetActivity.getTotalPrice()!=null){
                                    ChannelsBouquetActivity.getTotalPrice().setText(String.valueOf(Double.parseDouble(new DecimalFormat("##.##").format(setPricetotal))));
                                }
                                Global.countcheckDecrement=0;
                            }else{
                                Global.paidchannelPrice = Global.paidchannelPrice-s;
                                double n = Global.channelCount;
                                double setPricetotal = Const.getuniquechannelcount(Global.channelCount);
                                /*double k = 20.0;
                                double gt = k*18/100;
                                double lgt = k+gt;
                                double gst = s*18/100;
                                double sn = s+gst;
                                    double pricetotal = tot-lgt;
                                    double setPricetotal = pricetotal-sn;*/
                                    tot = setPricetotal;
                                    //Global.countcheckDecrement = p;
                                    Global.TotalSubscriptionPrice = Double.parseDouble(new DecimalFormat("##.##").format(setPricetotal));
                                    if(ChannelsBouquetActivity.getTotalPrice()!=null){
                                        ChannelsBouquetActivity.getTotalPrice().setText(String.valueOf(Double.parseDouble(new DecimalFormat("##.##").format(setPricetotal))));

                                }

                            }


                        } else {
                            // do nothing...
                        }

                        if(Global.TotalSubscriptionPrice>0){
                            ChannelsBouquetActivity.getlayout_main().setVisibility(View.VISIBLE);
                        }else{
                            if(ChannelsBouquetActivity.getlayout_main()!=null){
                                ChannelsBouquetActivity.getlayout_main().setVisibility(View.GONE);
                            }else{
                                //do nohthing...
                            }
                        }
                    }
                } else {
                    _listDataChild.get(_listDataHeader.get(groupPosition)).get(childPosition).setSelected(true);
                    String Broadcaster = _listDataChild.get(_listDataHeader.get(groupPosition)).get(childPosition).getBroadcaster();
                   String  Channel = _listDataChild.get(_listDataHeader.get(groupPosition)).get(childPosition).getChannel();
                    String category        =  _listDataChild.get(_listDataHeader.get(groupPosition)).get(childPosition).getCategory();
                    String language  =  _listDataChild.get(_listDataHeader.get(groupPosition)).get(childPosition).getLanguage();
                    String HD        =  _listDataChild.get(_listDataHeader.get(groupPosition)).get(childPosition).getHd();
                    String price   = _listDataChild.get(_listDataHeader.get(groupPosition)).get(childPosition).getPrice();
                    String ImageUrl   =  _listDataChild.get(_listDataHeader.get(groupPosition)).get(childPosition).getImageurl();
                    String Channel_Id = _listDataChild.get(_listDataHeader.get(groupPosition)).get(childPosition).getChannel_id();
                    String selection = String.valueOf(_listDataChild.get(_listDataHeader.get(groupPosition)).get(childPosition).getSelected());
                    String Flag = "Remove Added Channel";
                    Global.addtionPartFlag = Flag;
                    HashMap<String,String>addedData = new HashMap<String,String>();
                    addedData.put("Broadcaster",Broadcaster);
                    addedData.put("Channel_Id",Channel_Id);
                   addedData.put("Channel",Channel);
                    addedData.put("category",category);
                    if(language!=null){
                        addedData.put("language",language);
                    }
                    if(HD!=null){
                        addedData.put("HD",HD);
                    }

                    addedData.put("price",price);
                    addedData.put("ImageUrl",ImageUrl);
                   addedData.put("selection",selection);
                   confirmData.add(addedData);
                   Global.confirmAddedData = confirmData;
                    Global.channelData = _listDataChild;
                    if(ChannelsBouquetActivity.getlayout_main()!=null){
                        ChannelsBouquetActivity.getlayout_main().setVisibility(View.VISIBLE);
                    }else{
                        //do nohthing...
                    }
                    if(price.equalsIgnoreCase("0.0")){
                        f = f + 1;

                        int count = Global.channelCount+1;
                        Global.channelCount = count;
                        String freeChannellist = String.valueOf(n);
                        int freeChannel = Global.Freechannels+1;
                        String set_free = String.valueOf(freeChannel);
                        Global.addFreeChannel = Integer.parseInt(freeChannellist);
                        if(ChannelsBouquetActivity.getFreeText()!=null){
                            ChannelsBouquetActivity.getFreeText().setText(set_free+"+"+25);
                            Global.Freechannels = Integer.parseInt(set_free);
                        }else{
                            // do nothing...
                        }
                    }else{
                        n = n + 1;
                        String payChannellist = String.valueOf(n);
                        if(HD.equalsIgnoreCase("HD")){
                            int count = Global.channelCount+2;
                            Global.channelCount = count;
                            Log.d("channel_count", String.valueOf(Global.channelCount));
                        }else{
                            int count = Global.channelCount+1;
                            Global.channelCount = count;
                        }
                        int payChannel = Global.PaidChannels+1;
                        String set_paid = String.valueOf(payChannel);
                        Global.addPaidChannel = Integer.parseInt(payChannellist);
                        Log.d("paychannel", String.valueOf(Global.addPaidChannel));
                        if(ChannelsBouquetActivity.getPaidText()!=null){
                            ChannelsBouquetActivity.getPaidText().setText(set_paid);
                            Global.PaidChannels = Integer.parseInt(set_paid);
                        }else{
                            // do nothing...
                        }
                        if(Global.TotalSubscriptionPrice>0){
                            tot = Global.TotalSubscriptionPrice;
                        }
                        double s = Double.parseDouble(price);
                        if(Global.channelCount<100){
                            Global.paidchannelPrice = Global.paidchannelPrice+s;
                            double gst = s*18/100;
                            double pricetotal =tot+s+gst;
                            tot = pricetotal;
                            Global.TotalSubscriptionPrice = Double.parseDouble(new DecimalFormat("##.##").format(pricetotal));
                            if(ChannelsBouquetActivity.getTotalPrice()!=null){
                                ChannelsBouquetActivity.getTotalPrice().setText(String.valueOf(Double.parseDouble(new DecimalFormat("##.##").format(pricetotal))));
                            }
                            Global.countcheckIncrement=0;
                        }else{
                            Global.paidchannelPrice = Global.paidchannelPrice+s;
                            double n = Global.channelCount;
                            double setPricetotal = Const.getuniquechannelcount(Global.channelCount);
                            /*double k = 20.0;
                            double gt = k*18/100;
                            double lgt = k+gt;
                            double gst = s*18/100;
                            double sn = s+gst;
                                double pricetotal = tot+sn+lgt;
                                double setPricetotal = pricetotal;*/
                                tot = setPricetotal;
                                /*Global.countcheckIncrement = p;*/
                                Global.TotalSubscriptionPrice = Double.parseDouble(new DecimalFormat("##.##").format(setPricetotal));
                                if(ChannelsBouquetActivity.getTotalPrice()!=null){
                                    ChannelsBouquetActivity.getTotalPrice().setText(String.valueOf(Double.parseDouble(new DecimalFormat("##.##").format(setPricetotal))));

                            }


                        }
                    }

                }

                notifyDataSetChanged();
            }
        });

        tv_language.setFilters(new InputFilter[]{ new InputFilter.LengthFilter(25) });
        tv_language.setText(_listDataChild.get(_listDataHeader.get(groupPosition)).get(childPosition).getLanguage());
        String price =_listDataChild.get(_listDataHeader.get(groupPosition)).get(childPosition).getPrice();
        if(price!=null && price.equalsIgnoreCase("0.0")){

            tv_price.setText("");
        }else{
            tv_price.setText((_context.getResources().getString(R.string.Rs)+" "+_listDataChild.get(_listDataHeader.get(groupPosition)).get(childPosition).getPrice()));
        }
        tv_channel.setFilters(new InputFilter[]{ new InputFilter.LengthFilter(25) });
        tv_channel.setText(_listDataChild.get(_listDataHeader.get(groupPosition)).get(childPosition).getChannel());
        String path = _listDataChild.get(_listDataHeader.get(groupPosition)).get(childPosition).getImageurl();
        Glide.with(_context)
                .asBitmap()
                .load(path)
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                        iv_channels.setImageBitmap(resource);
                    }
                });

       /* TextView txtListChild = (TextView) convertView
                .findViewById(R.id.lblListItem);*/



        //txtListChild.setText(childText);
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group, null);
        }

        ImageView iconExpand = (ImageView) convertView.findViewById(R.id.iv_arrow_expand);
        ImageView iconCollapse = (ImageView) convertView.findViewById(R.id.iv_arrow_Collapse);
        if (isExpanded ) {
            //animateExpand(iconCollapse);
            expandState.put(groupPosition, true);
            iconExpand.setVisibility(View.GONE);
            iconCollapse.setVisibility(View.VISIBLE);
        } else {
            //animateCollapse(iconExpand);
            expandState.put(groupPosition, false);
            iconExpand.setVisibility(View.VISIBLE);
            iconCollapse.setVisibility(View.GONE);
        }

        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.lblListHeader);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);

        return convertView;
    }

    private void animateExpand(View v) {
        ObjectAnimator anim = ObjectAnimator.ofFloat(v, "rotation", 0, 360);
        anim.setDuration(300);
        anim.start();
    }

    private void animateCollapse(View v) {
        ObjectAnimator anim = ObjectAnimator.ofFloat(v, "rotation", 90, 0);
        anim.setDuration(300);
        anim.start();
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public void filterData(String query) {
        query = query.toLowerCase(Locale.getDefault());
        Log.v("MyListAdapter", String.valueOf(_listDataChild.size()));
        _listDataChild.clear();

        if(query.length() == 0){
            paidDto =Global.newDataPaid;
            freeDto = Global.newDataFree;
            _listDataChild.put("Free Channel",freeDto);
            _listDataChild.put("Paid Channel",paidDto);
        }
        else {
            freeDto   = new ArrayList<ChannelDto>();
            paidDto   = new ArrayList<ChannelDto>();
                for (ChannelDto wp : filterDto) {
                    if (wp.getChannel().toLowerCase(Locale.getDefault())
                            .contains(query)) {
                        if(wp.getPrice().equalsIgnoreCase("0.0")){

                            freeDto.add(wp);

                        }else{
                            paidDto.add(wp);

                        }

                    }
                }
            _listDataChild.put("Free Channel",freeDto);
            _listDataChild.put("Paid Channel",paidDto);
            }

        notifyDataSetChanged();


    }
}


