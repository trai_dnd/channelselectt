package channelselector.in.gov.trai.channelselect.activity.activity.activity.classes;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import channelselector.in.gov.trai.channelselect.R;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.adapters.PastRequestRecycler;

public class PastRequest extends AppCompatActivity {
    private RecyclerView rec_past_request;
    ArrayList<String>array_data = new ArrayList<String>();
    private PastRequestRecycler pastRequestRecycler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_past_request);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        view();
        setRecyclerView();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    private void setRecyclerView() {

        array_data = getArrayList("ArrayData");
        if(array_data!=null){
            if(array_data.size()>0){
                pastRequestRecycler = new PastRequestRecycler(PastRequest.this,array_data);
                rec_past_request.setAdapter(pastRequestRecycler);
            }
        }
    }

    public ArrayList<String> getArrayList(String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(PastRequest.this);
        Gson gson = new Gson();
        String json = prefs.getString(key, null);
        Type type = new TypeToken<ArrayList<String>>() {
        }.getType();
        return gson.fromJson(json, type);
    }

    private void view() {

        rec_past_request    = (RecyclerView)findViewById(R.id.rec_past_request);
        LinearLayoutManager layoutManager = new LinearLayoutManager(PastRequest.this);
        rec_past_request.setLayoutManager(layoutManager);
    }
}
