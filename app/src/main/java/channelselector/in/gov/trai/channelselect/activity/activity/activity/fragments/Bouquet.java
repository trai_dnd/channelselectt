package channelselector.in.gov.trai.channelselect.activity.activity.activity.fragments;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.RelativeLayout;

import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import channelselector.in.gov.trai.channelselect.R;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.DTO.BouquetDto;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.DTO.BouquetSubDto;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.Extras.Const;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.Extras.Global;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.MainActivity;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.adapters.BouquetRecycler;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.classes.Review;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.webservicehandler.GetWebService;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.webservicehandler.HeaderGet;

public class Bouquet extends Fragment {

    private RecyclerView rec_bouquet;
    private RelativeLayout mainLay;
    private List<BouquetDto> bouquetDtos = new ArrayList<BouquetDto>();
    private List<BouquetSubDto> bouquetSubDtos = new ArrayList<BouquetSubDto>();
    private BouquetRecycler bouquetRecycler;
    String Flag ,BouquetFlag;
    private List<BouquetSubDto> subbouquet = new ArrayList<BouquetSubDto>();
    private SearchView searchView = null;
    private SearchView.OnQueryTextListener queryTextListener;
    private RelativeLayout rel_bottom;
    String operator,FlagSucess,accessToken;
    private List<BouquetDto> gbouquetDtos = new ArrayList<BouquetDto>();
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(
                R.layout.activity_bouquet, container, false);
       // operator = Global.operatorname;
        view(v);
        // setAdapter();
        operator    = Prefs.getString(Const.OPERATORNAME, "operator");
        accessToken = Prefs.getString(Const.ACCESSTOKEN, "token");
        getbouquet();
       events();
        return v;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }
    private void events() {

        rel_bottom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getActivity(),Review.class));
            }
        });
    }

    private void getbouquet() {
        BouquetFlag = Global.addtionalBouquetFlag;
        Flag = Global.addtionPartFlag;
        subbouquet = Global.bouquetSubDtos;
        gbouquetDtos = Global.FreshBouquet;
        FlagSucess = Global.success;
        if (FlagSucess != null && FlagSucess.equalsIgnoreCase("setSuccess")) {
            if (Const.isNetworkAvailable(getActivity())) {
                Global.success ="done";
                new GetBouquetTask().execute();
            } else {
                Const.messageViewRe(mainLay, getString(R.string.network));
            }
        }
        else if(Flag!=null && Flag.equalsIgnoreCase("Remove Added Channel")){
            bouquetDtos = Global.selectedBouquet;

            if(bouquetDtos!=null && bouquetDtos.size()>0){
                for(int i =0;i<bouquetDtos.size();i++){
                    String bouquetName = bouquetDtos.get(i).getBouquet_id();
                    if(Global.bouquetData!=null && Global.bouquetData.size()>0){
                        for(int j =0; j <Global.bouquetData.size();j++){
                            if(Global.bouquetData.get(j).getBouquet_id().equalsIgnoreCase(bouquetName)){
                                bouquetDtos.remove(i);
                            }else{
                                // do nothing....
                            }
                        }
                    }
                }
            }

            bouquetRecycler = new BouquetRecycler(getActivity(),bouquetDtos,subbouquet);
            rec_bouquet.setAdapter(bouquetRecycler);

        }
        else if(BouquetFlag!=null && BouquetFlag.equalsIgnoreCase("Remove Added Bouquet")){

            bouquetDtos = Global.selectedBouquet;
            //Flag = "used";
            if(bouquetDtos!=null && bouquetDtos.size()>0){
                for(int i =0;i<bouquetDtos.size();i++){
                    String bouquetName = bouquetDtos.get(i).getBouquet_id();
                    if(Global.bouquetData!=null && Global.bouquetData.size()>0){
                        for(int j =0; j <Global.bouquetData.size();j++){
                            if(Global.bouquetData.get(j).getBouquet_id().equalsIgnoreCase(bouquetName)){
                                bouquetDtos.remove(i);
                            }else{
                                // do nothing....
                            }
                        }
                    }
                }
            }
            bouquetRecycler = new BouquetRecycler(getActivity(),bouquetDtos,subbouquet);
            rec_bouquet.setAdapter(bouquetRecycler);

        }else {
            if(subbouquet!=null && gbouquetDtos!=null){

                if(gbouquetDtos.size()>0 && gbouquetDtos.size()>0){
                    if(gbouquetDtos!=null && gbouquetDtos.size()>0){
                        for(int i =0;i<gbouquetDtos.size();i++){
                            String bouquetName = gbouquetDtos.get(i).getBouquet_id();
                            if(Global.bouquetData!=null && Global.bouquetData.size()>0){
                                for(int j =0; j <Global.bouquetData.size();j++){
                                    if(Global.bouquetData.get(j).getBouquet_id().equalsIgnoreCase(bouquetName)){
                                        gbouquetDtos.remove(i);
                                    }else{
                                        // do nothing....
                                    }
                                }
                            }
                        }
                    }
                    bouquetRecycler = new BouquetRecycler(getActivity(),gbouquetDtos,subbouquet);
                    rec_bouquet.setAdapter(bouquetRecycler);
                }else{
                    if (Const.isNetworkAvailable(getActivity())) {

                        new GetBouquetTask().execute();
                    } else {
                        Const.messageViewRe(mainLay, getString(R.string.network));
                    }
                }
            }else{
                if (Const.isNetworkAvailable(getActivity())) {

                    new GetBouquetTask().execute();
                } else {
                    Const.messageViewRe(mainLay, getString(R.string.network));
                }
            }


        }

    }

    ProgressDialog progressDialog;

    public class GetBouquetTask extends AsyncTask<String, Void, String> {
        String url;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if(operator.equalsIgnoreCase("Tata Sky")){
               url = Const.getBouquetTaata;
                //url="http://192.168.7.187:8082/api/feedback";
            }else{
                url = Const.getBouquet;
            }


            Log.e("URL=>", "" + url);
            progressDialog = Const.loader(progressDialog, getActivity());
        }

        @Override
        protected String doInBackground(String... strings) {

            HeaderGet o = new HeaderGet();
            String res = null;
            try {
                res = o.getData(url,accessToken);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return res;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            Log.e("RES=>", "" + s);
            try {
                String result = s;
                JSONObject obj = new JSONObject(result);
                String status = obj.getString("status");
                if (status.equalsIgnoreCase("200")) {
                    JSONArray jsonArray = obj.getJSONArray("bouquet");
                    for (int i = 0; i < jsonArray.length(); i++) {

                        JSONObject object = jsonArray.getJSONObject(i);
                        String bouquet_id = object.getString("bouquet_id");
                        String bouquet = object.getString("bouquet");
                        String broadcaster = object.getString("broadcaster");
                        String bouquetPrice = object.getString("bouquet_price");
                        String totalchannel = object.getString("total_channel");

                        BouquetDto bk = new BouquetDto();
                        bk.setBouquet_id(bouquet_id);
                        bk.setBouquet(bouquet);
                        bk.setBroadcaster(broadcaster);
                        bk.setBouquetprice(bouquetPrice);
                        bk.setTotalchannel(totalchannel);
                        bk.setSelected(false);
                        bouquetDtos.add(bk);

                        if(object.has("bouquetchannel")){

                            JSONArray jsonArraymember = object.getJSONArray("bouquetchannel");
                            if(jsonArraymember.length()>0) {

                                for (int j = 0; j < jsonArraymember.length(); j++) {

                                    JSONObject products = jsonArraymember.getJSONObject(j);
                                    String channel_id = products.getString("channel_id");
                                    String broadcasterch = products.getString("broadcaster");
                                    String channel = products.getString("channel");
                                    String category = products.getString("category");
                                    String language = products.getString("language");
                                    String price = products.getString("price");
                                    String imageurl = products.getString("imageurl");
                                    String hd = products.getString("hd");
                                    if(imageurl.contains("http")){
                                        //do nothing...
                                    }else{
                                        imageurl = "http://59.179.24.45:8081"+"/"+products.getString("imageurl");
                                    }

                                    BouquetSubDto prodto = new BouquetSubDto();
                                    prodto.setChannel_id(channel_id);
                                    prodto.setBroadcaster(broadcasterch);
                                    prodto.setChannel(channel);
                                    prodto.setCategory(category);
                                    prodto.setLanguage(language);
                                    prodto.setPrice(price);
                                    prodto.setImageurl(imageurl);
                                    prodto.setHd(hd);
                                    prodto.setBouquet_id(bouquet_id);
                                    bouquetSubDtos.add(prodto);

                                }
                            }else{

                                //do nothing...
                            }

                        }
                    }

                    if(bouquetDtos!=null && bouquetDtos.size()>0){
                        for(int i =0;i<bouquetDtos.size();i++){
                            String bouquetName = bouquetDtos.get(i).getBouquet_id();
                            if(Global.bouquetData!=null && Global.bouquetData.size()>0){
                                for(int j =0; j <Global.bouquetData.size();j++){
                                    if(Global.bouquetData.get(j).getBouquet_id().equalsIgnoreCase(bouquetName)){
                                        bouquetDtos.remove(i);
                                    }else{
                                        // do nothing....
                                    }
                                }
                            }
                        }
                    }

                    if(bouquetDtos!=null && bouquetDtos.size()>0){
                        for(int i =0;i<bouquetDtos.size();i++){
                            String bouquetName = bouquetDtos.get(i).getBouquet_id();
                            if(Global.bouquetData!=null && Global.bouquetData.size()>0){
                                for(int j =0; j <Global.bouquetData.size();j++){
                                    if(Global.bouquetData.get(j).getBouquet_id().equalsIgnoreCase(bouquetName)){
                                        bouquetDtos.remove(i);
                                    }else{
                                        // do nothing....
                                    }
                                }
                            }
                        }
                    }

                    Global.selectedBouquet = bouquetDtos;
                    Global.FreshBouquet = bouquetDtos;
                    Global.bouquetSubDtos = bouquetSubDtos;


                    bouquetRecycler = new BouquetRecycler(getActivity(),bouquetDtos,bouquetSubDtos);
                    rec_bouquet.setAdapter(bouquetRecycler);
                }else{
                    String message = obj.getString("message");

                    Const.messageViewRe(mainLay, message);
                }
            }catch (Exception e) {

            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main_menu, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);

        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));

            queryTextListener = new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextChange(String newText) {
                    Log.e("SearchText=>",""+newText.length());
                    int setDefault=0;
                    // getActivity().setTitle("");
                    if(newText.length()==1&&setDefault==0)
                    {

                        ++setDefault;
                    }

                    bouquetRecycler.filterData(newText);
                    return false;
                }
                @Override
                public boolean onQueryTextSubmit(String query) {
                    Const.hideKeyboard(getActivity());
                    bouquetRecycler.filterData(query);
                    //getActivity().setTitle("");
                    /*total_records.setText(getString(R.string.total_records)+" "+adapter.getItemCount());*/
                    if(bouquetRecycler.getItemCount()==0) {
                        Const.messageViewRe(mainLay,getString(R.string.no_data__found));
                    }
                    return false;
                }
            };
            searchView.setOnQueryTextListener(queryTextListener);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                // Not implemented here
                return false;
            default:
                break;
        }
        searchView.setOnQueryTextListener(queryTextListener);
        return super.onOptionsItemSelected(item);
    }

    private void view(View v) {

        rec_bouquet = (RecyclerView) v.findViewById(R.id.rec_bouquet);
        mainLay           =  (RelativeLayout)v.findViewById(R.id.mainLay);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rec_bouquet.setLayoutManager(layoutManager);
        rel_bottom = (RelativeLayout)v.findViewById(R.id.rel_bottom);
    }
}
