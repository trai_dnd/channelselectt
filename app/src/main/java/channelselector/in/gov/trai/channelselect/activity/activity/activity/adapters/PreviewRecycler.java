package channelselector.in.gov.trai.channelselect.activity.activity.activity.adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import channelselector.in.gov.trai.channelselect.R;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.DTO.MySubsFree;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.DTO.MySubsPiad;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.Extras.Global;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.classes.ChannelsBouquetActivity;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.classes.MySubscription;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.classes.Review;


public class PreviewRecycler extends RecyclerView.Adapter<PreviewRecycler.MyViewHolder> {

    private Context context;

    private MyViewHolder viewHolder;
    private int position;
    int pos;
    List<String> parent_data;
    ArrayList<HashMap<String,String>> freeData;
    ArrayList<HashMap<String,String>> paidChannel;
    ArrayList<String> bouquetName;
    ArrayList<MySubsFree> freeDto;
    ArrayList<MySubsPiad> paidDto;
    private ChildPreviewRecycler subAdapter;
    private MySubscriptionBouque buquetAdapter;
    ArrayList<HashMap<String,String>> confirmAddedChannel;
    ArrayList<HashMap<String,String>> confirmaddedBouquetData;
    ArrayList<HashMap<String,String>> totalchannelData;
    ArrayList<String>free;
    ArrayList<String>paid;
    ArrayList<String>total = new ArrayList<String>();
    private ChildPreviewRecyclersubscription adapter;
    ArrayList<HashMap<String,String>> mysubscfree;
    ArrayList<HashMap<String,String>> mysubscpaid;
    ArrayList<HashMap<String,String>> mybiuqueList;
    ArrayList<HashMap<String,String>> mySubsFreePaid;
    String FlagRemove,set_bouquet,setPaid,setFree,setPrice;

    public PreviewRecycler(Context context, List<String> listDataHeader) {

        this.context = context;
        this.parent_data =listDataHeader;
        this.confirmAddedChannel = Global.confirmAddedData;
        this.confirmaddedBouquetData=Global.confirmAddedBouquetData;
        this.mysubscfree = Global.mysubscfree;
        this.mysubscpaid = Global.mysubscpaid;
        this.mybiuqueList = Global.deletedbouquesubscription;
        //FlagRemove = getIntent().getStringExtra("Remove Channel");
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
// infalte the item Layout
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_subscriber, parent, false);
// set the view's size, margins, paddings and layout parameters
        MyViewHolder viewHolder = new MyViewHolder(v); // pass the view to View Holder


        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder viewHolder, final int position) {
        //Log.e("DATATEST=>",userData.get(position).getBroadcaster());
        this.viewHolder = viewHolder;
        this.position = position;
        set_bouquet = String.valueOf(Global.TotalBouquet);
        Review.getBouquetText().setText(set_bouquet);
        setPaid = String.valueOf(Global.PaidChannels);
        Review.getPaidText().setText(setPaid);
        setFree = String.valueOf(Global.Freechannels);
        Review.getFreeText().setText(setFree+"+"+25);
        setPrice = String.valueOf(Global.TotalSubscriptionPrice);
        Review.getTv_price().setText(context.getResources().getString(R.string.Rs)+" "+setPrice);
        //Global.CalculatedPrice = setPrice;

        viewHolder.tv_choce_name.setText(parent_data.get(position));

        viewHolder.img_drop_down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(parent_data.get(position).equalsIgnoreCase("Dropped Current Subscription")){

                    viewHolder.iv_arrow_Collapse.setVisibility(View.VISIBLE);
                    viewHolder.img_drop_down.setVisibility(View.GONE);

                    mySubsFreePaid = new ArrayList<HashMap<String,String>>();
                    if(mysubscfree!=null && mysubscfree.size()>0){
                        mySubsFreePaid.addAll(mysubscfree);
                    }if(mysubscpaid!=null && mysubscpaid.size()>0){

                        mySubsFreePaid.addAll(mysubscpaid);
                    }if(mybiuqueList!=null && mybiuqueList.size()>0){

                        mySubsFreePaid.addAll(mybiuqueList);
                    }
                    Global.setfinal = mySubsFreePaid;
                    viewHolder.rec_paid_Data_recycler.setVisibility(View.VISIBLE);
                    LinearLayoutManager layoutManager = new LinearLayoutManager(context.getApplicationContext());
                    viewHolder.rec_paid_Data_recycler.setLayoutManager(layoutManager);
                    if(mySubsFreePaid!=null && mySubsFreePaid.size()>0){
                        adapter = new ChildPreviewRecyclersubscription(context,mySubsFreePaid);
                        viewHolder.rec_paid_Data_recycler.setAdapter(adapter);
                    }else{
                        final Dialog dialog = new Dialog(context); // Context, this, etc.
                        dialog.setContentView(R.layout.custom_dialog_add_channel);
                        dialog.setTitle(R.string.addchannel);
                        dialog.setCancelable(false);
                        TextView dialog_info = (TextView)dialog.findViewById(R.id.dialog_info);
                        dialog_info.setText("You have not dropped any Channel/Bouquet from your current subscription if you want to drop click drop else cancel");
                        Button dialog_add = (Button)dialog.findViewById(R.id.dialog_add);
                        dialog_add.setText("DROP");
                        dialog_add.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                                Intent intent = new Intent(context, MySubscription.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                context.startActivity(intent);

                            }
                        });
                        Button dialog_cancel = (Button)dialog.findViewById(R.id.dialog_cancel);
                        dialog_cancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                viewHolder.iv_arrow_Collapse.setVisibility(View.GONE);
                                viewHolder.img_drop_down.setVisibility(View.VISIBLE);
                                dialog.dismiss();
                            }
                        });
                        dialog.show();
                    }

                }if(parent_data.get(position).equalsIgnoreCase("Added Channel / Bouquet")){
                    viewHolder.rec_paid_Data_recycler.setVisibility(View.VISIBLE);
                    viewHolder.iv_arrow_Collapse.setVisibility(View.VISIBLE);
                    viewHolder.img_drop_down.setVisibility(View.GONE);
                    LinearLayoutManager layoutManager = new LinearLayoutManager(context.getApplicationContext());
                    viewHolder.rec_paid_Data_recycler.setLayoutManager(layoutManager);
                    confirmAddedChannel = Global.confirmAddedData;
                    confirmaddedBouquetData=Global.confirmAddedBouquetData;
                    totalchannelData = new ArrayList<HashMap<String,String>>();
                    if(confirmAddedChannel!=null && confirmAddedChannel.size()>0){
                        totalchannelData.addAll(confirmAddedChannel);
                    }if(confirmaddedBouquetData!=null && confirmaddedBouquetData.size()>0){
                        totalchannelData.addAll(confirmaddedBouquetData);
                    }
                   Global.confirmAddedData = totalchannelData;
                   if(totalchannelData!=null && totalchannelData.size()>0){
                       if(totalchannelData!=null){
                           for(int i =0 ;i<totalchannelData.size();i++){
                               for(int j=i+1;j<totalchannelData.size();j++){
                                   if(totalchannelData.get(i).get("Channel").equalsIgnoreCase(totalchannelData.get(j).get("Channel"))){
                                       totalchannelData.remove(j);
                                       j--;
                                   }
                               }
                           }
                       }
                       subAdapter = new ChildPreviewRecycler(context,totalchannelData);
                       viewHolder.rec_paid_Data_recycler.setAdapter(subAdapter);
                   }else{
                      // Toast.makeText(context,"No channel selected", Toast.LENGTH_SHORT).show();
                       final Dialog dialog = new Dialog(context); // Context, this, etc.
                       dialog.setContentView(R.layout.custom_dialog_add_channel);
                       dialog.setTitle(R.string.addchannel);
                       dialog.setCancelable(false);
                       TextView dialog_info = (TextView)dialog.findViewById(R.id.dialog_info);
                       dialog_info.setText("You have not added any Channel/Bouquet if you want to add Channel/Bouquet click add else cancel.");
                       Button dialog_add = (Button)dialog.findViewById(R.id.dialog_add);
                       dialog_add.setOnClickListener(new View.OnClickListener() {
                           @Override
                           public void onClick(View v) {
                               dialog.dismiss();
                               Intent intent = new Intent(context, ChannelsBouquetActivity.class);
                               intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                               intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                               context.startActivity(intent);

                           }
                       });
                       Button dialog_cancel = (Button)dialog.findViewById(R.id.dialog_cancel);
                       dialog_cancel.setOnClickListener(new View.OnClickListener() {
                           @Override
                           public void onClick(View v) {
                               viewHolder.iv_arrow_Collapse.setVisibility(View.GONE);
                               viewHolder.img_drop_down.setVisibility(View.VISIBLE);
                               dialog.dismiss();
                           }
                       });
                       dialog.show();

                   }

                }


                //viewHolder.rec_subscriber.setVisibility(View.VISIBLE);


            }
        });

        viewHolder.iv_arrow_Collapse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                viewHolder.iv_arrow_Collapse.setVisibility(View.GONE);
                viewHolder.img_drop_down.setVisibility(View.VISIBLE);
                 if(parent_data.get(position).equalsIgnoreCase("Dropped Current Subscription")){
                     viewHolder.rec_paid_Data_recycler.setVisibility(View.GONE);

                } if(parent_data.get(position).equalsIgnoreCase("Added Channel / Bouquet")){
                    viewHolder.rec_paid_Data_recycler.setVisibility(View.GONE);
                }
            }
        });


    }



    @Override
    public int getItemCount() {
        return parent_data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView iv_channels;
        private TextView tv_choce_name;
        private ImageButton img_drop_down,iv_arrow_Collapse;
        private RecyclerView rec_subscriber,rec_bouquet_Data_recycler,rec_paid_Data_recycler,rec_free_Data_recycler;

        public MyViewHolder(View view) {
            super(view);
            int TAG = position;
            // get the reference of item view's

            tv_choce_name     = (TextView) view.findViewById(R.id.tv_choce_name);
            img_drop_down    = (ImageButton)view.findViewById(R.id.img_drop_down);
            iv_arrow_Collapse    = (ImageButton)view.findViewById(R.id.iv_arrow_Collapse);
            rec_free_Data_recycler  = (RecyclerView)view.findViewById(R.id.rec_free_Data_recycler);
            rec_bouquet_Data_recycler = (RecyclerView)view.findViewById(R.id.rec_bouquet_Data_recycler);
            rec_paid_Data_recycler = (RecyclerView)view.findViewById(R.id.rec_paid_Data_recycler);

            /*rec_bouquet_Data_recycler.setLayoutManager(layoutManager);
            rec_paid_Data_recycler.setLayoutManager(layoutManager);*/

        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }




}


