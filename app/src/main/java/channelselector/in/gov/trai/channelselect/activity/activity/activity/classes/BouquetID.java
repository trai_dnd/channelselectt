package channelselector.in.gov.trai.channelselect.activity.activity.activity.classes;

public class BouquetID {

    public long bouquet_id;

    public long getBouquet_id() {
        return bouquet_id;
    }

    public void setBouquet_id(long bouquet_id) {
        this.bouquet_id = bouquet_id;
    }

    public BouquetID(long bouquet_id )
    {

        this.bouquet_id = bouquet_id;
    }


}
