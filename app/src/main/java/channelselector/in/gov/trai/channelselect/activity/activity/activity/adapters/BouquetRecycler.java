package channelselector.in.gov.trai.channelselect.activity.activity.activity.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import channelselector.in.gov.trai.channelselect.R;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.DTO.BouquetDto;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.DTO.BouquetSubDto;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.Extras.Const;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.Extras.Global;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.classes.ChannelsBouquetActivity;


public class BouquetRecycler extends RecyclerView.Adapter<BouquetRecycler.MyViewHolder> {

    private Context context;
    private List<BouquetDto> userData;
    private List<BouquetDto> arraylist;
    private MyViewHolder viewHolder;
    private int position;
    int pos;
    //private BouquetSubRecycler userRecyclerView;
    private ArrayList<HashMap<String,String>> confirmData= new ArrayList<HashMap<String,String>>();
    private ArrayList<HashMap<String,String>> confirmDatahash= new ArrayList<HashMap<String,String>>();
    List<BouquetSubDto> subBouquetDto;
    String Flagsearch;
    int n =0;
    double bouquetTotal;
    int flag=0;
    double check =0;
    String operator;
    String url,url1;
    private BouquetSubRecycler userRecyclerView;

    public BouquetRecycler(Context context, List<BouquetDto> senderList,List<BouquetSubDto> bouquetSubDtos) {
        //operator = Global.operatorname;
        this.context=context;
        this.userData=senderList;
        this.arraylist   = new ArrayList<BouquetDto>();
        this.arraylist.addAll(userData);
        this.subBouquetDto = bouquetSubDtos;
        if(Global.confirmAddedBouquetData!=null && Global.confirmAddedBouquetData.size()>0){
            confirmData= Global.confirmAddedBouquetData;
        }if(Global.confirmAddedBouquetDataHash!=null && Global.confirmAddedBouquetDataHash.size()>0 ){
            confirmDatahash = Global.confirmAddedBouquetDataHash;
        }if(Global.TotalBouquet>0){
            n = Global.TotalBouquet;
        }if(Global.TotalSubscriptionPrice>0){
            bouquetTotal = Global.TotalSubscriptionPrice;
            ChannelsBouquetActivity.getlayout_main().setVisibility(View.VISIBLE);
        }if(ChannelsBouquetActivity.getBouquetText()!=null){
            String total_bouquet = String.valueOf(Global.TotalBouquet);
            ChannelsBouquetActivity.getBouquetText().setText(total_bouquet);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
// infalte the item Layout
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_bouquet_data, parent, false);
// set the view's size, margins, paddings and layout parameters
        MyViewHolder viewHolder = new MyViewHolder(v); // pass the view to View Holder


        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder viewHolder, final int position) {
        Log.e("DATATEST=>",userData.get(position).getBroadcaster());
        this.viewHolder = viewHolder;
        this.position = position;
        viewHolder.tv_broadcaster.setText(userData.get(position).getBroadcaster());
        viewHolder.tv_channel.setText(userData.get(position).getBouquet());
        viewHolder.tv_price.setText("Channels:"+" " +userData.get(position).getTotalchannel());
        viewHolder.tv_category.setText(context.getResources().getString(R.string.Rs)+userData.get(position).getBouquetprice());
        if(Global.selectedBouquet!=null){
            if(Global.selectedBouquet.size()>0){
                if(Global.selectedBouquet.get(position).getSelected().equals(true)){
                    viewHolder.mainLay.setBackgroundColor(Color.parseColor("#EAEAEA"));

                }else{

                    viewHolder.mainLay.setBackground(ContextCompat.getDrawable(context, R.drawable.shadow));

                }
            }if(userData.get(position).getSelected().equals(true)){
                viewHolder.mainLay.setBackgroundColor(Color.parseColor("#EAEAEA"));

            }else{

                viewHolder.mainLay.setBackground(ContextCompat.getDrawable(context, R.drawable.shadow));

            }
        }else{
            if(userData.get(position).getSelected().equals(true)){
                viewHolder.mainLay.setBackgroundColor(Color.parseColor("#EAEAEA"));

            }else{

                viewHolder.mainLay.setBackground(ContextCompat.getDrawable(context, R.drawable.shadow));

            }
        }

        viewHolder.img_select_bouquet.setChecked(userData.get(position).getSelected());
        viewHolder.img_select_bouquet.setTag(position);

        viewHolder.img_drop_down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String bouquet_id = userData.get(position).getBouquet_id();
                List<BouquetSubDto> bouquetSubDtos = new ArrayList<BouquetSubDto>();
                if(subBouquetDto!=null && subBouquetDto.size()>0){
                    for(int i =0; i<subBouquetDto.size();i++){
                        String subbouquetid = subBouquetDto.get(i).getBouquet_id();
                        if(subbouquetid.equalsIgnoreCase(bouquet_id)){
                            BouquetSubDto prodto = new BouquetSubDto();
                            prodto = subBouquetDto.get(i);
                            bouquetSubDtos.add(prodto);
                            viewHolder.img_drop_down.setVisibility(View.GONE);
                            viewHolder.iv_arrow_Collapse.setVisibility(View.VISIBLE);
                            viewHolder.rec_bottom.setVisibility(View.VISIBLE);
                            userRecyclerView = new BouquetSubRecycler(context,bouquetSubDtos);
                            viewHolder.rec_bottom.setAdapter(userRecyclerView);
                        }
                    }
                }

            }
        });

        viewHolder.img_select_bouquet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer pos = (Integer) viewHolder.img_select_bouquet.getTag();
                if (userData.get(pos).getSelected()) {
                    int ChannelBouquetCount = Integer.parseInt(userData.get(pos).getTotalchannel());
                    userData.get(pos).setSelected(false);
                    if (n >= 0) {
                        if (n == 0) {
                            //do nothing...
                        } else {
                            n = n - 1;

                            Log.d("channel_count", String.valueOf(Global.channelCount));
                            String payChannellist = String.valueOf(n);
                            int payBouquet = Global.TotalBouquet-1;
                            String set_paid = String.valueOf(payBouquet);
                            Global.addBouquetChannel = Integer.parseInt(payChannellist);
                            if(ChannelsBouquetActivity.getBouquetText()!=null){
                                ChannelsBouquetActivity.getBouquetText().setText(String.valueOf(set_paid));
                                Global.TotalBouquet = payBouquet;
                            }
                        }
                    } else {
                        // do nothing...
                    }
                    if(Global.TotalSubscriptionPrice>0){
                        bouquetTotal = Global.TotalSubscriptionPrice;
                    }
                    double s = Double.parseDouble(userData.get(position).getBouquetprice());
                    int count = Global.channelCount-ChannelBouquetCount;
                    Global.channelCount = count;
                    if(Global.channelCount<100){
                        Global.bouquetprice = Global.bouquetprice-s;
                        double Totalchannelbouquet = Global.paidchannelPrice+ Global.bouquetprice;
                        double Final = 130+Totalchannelbouquet;
                        double gt = Final*18/100;
                        double pricetotal = Final+gt;
                        Global.TotalSubscriptionPrice = Double.parseDouble(new DecimalFormat("##.##").format(pricetotal));
                        if(Global.TotalSubscriptionPrice>0){
                            bouquetTotal = Global.TotalSubscriptionPrice;
                        }else{
                            ChannelsBouquetActivity.getlayout_main().setVisibility(View.GONE);
                        }
                        if(ChannelsBouquetActivity.getTotalPrice()!=null){
                            ChannelsBouquetActivity.getTotalPrice().setText(String.valueOf(Double.parseDouble(new DecimalFormat("##.##").format(pricetotal))));
                        }
                    }else{
                        Global.bouquetprice = Global.bouquetprice-s;
                        double n = Global.channelCount;
                        double pricetotal  =Const.getuniquechannelcount(Global.channelCount);
                        /*double k = 20.0;
                        double gt = k*18/100;
                        double lgt = k+gt;
                        double gst = s*18/100;
                        double sn = s+gst;
                        if(Global.countcheckDecrement==p){
                            bouquetTotal = Global.TotalSubscriptionPrice;
                            Global.countcheckDecrement=0;
                        }else{
                            bouquetTotal = Global.TotalSubscriptionPrice-lgt;
                            Global.countcheckDecrement = p;
                        }
                        double pricetotal =bouquetTotal-sn;*/
                        bouquetTotal = pricetotal;
                        Global.TotalSubscriptionPrice = Double.parseDouble(new DecimalFormat("##.##").format(pricetotal));
                        if(Global.TotalSubscriptionPrice>0){
                            bouquetTotal = Global.TotalSubscriptionPrice;
                        }else{
                            ChannelsBouquetActivity.getlayout_main().setVisibility(View.GONE);
                        }
                        if(ChannelsBouquetActivity.getTotalPrice()!=null){
                            ChannelsBouquetActivity.getTotalPrice().setText(String.valueOf(Double.parseDouble(new DecimalFormat("##.##").format(pricetotal))));
                        }
                    }

                    String name = userData.get(position).getBouquet();
                    if(confirmDatahash!=null&& confirmDatahash.size()>0){
                        for(int i =confirmDatahash.size()-1; i>0; i--){
                            if(confirmDatahash.get(i).get("BouquetName").equalsIgnoreCase(name)){
                                confirmDatahash.remove(i);
                                Global.confirmAddedBouquetDataHash = confirmDatahash;
                            }
                        }if(confirmDatahash!=null&& confirmDatahash.size()>0){
                            for(int i =0 ;i<confirmDatahash.size();i++){
                                if(confirmDatahash.get(i).get("BouquetName").equalsIgnoreCase(name)){
                                    confirmDatahash.remove(i);
                                    Global.confirmAddedBouquetDataHash = confirmDatahash;
                                }
                            }
                        }
                    }
                    if(confirmData!=null && confirmData.size()>0){
                        for(int i =0; i<confirmData.size();i++){
                            if(confirmData.get(i).get("Channel").equalsIgnoreCase(name)){
                                confirmData.remove(i);
                                Global.confirmAddedBouquetData = confirmData;
                            }
                        }
                    }
                    viewHolder.mainLay.setBackground(ContextCompat.getDrawable(context, R.drawable.shadow));

                    if(Global.confirmAddedData!=null && Global.confirmAddedData.size()>0){
                        for(int i =0; i<Global.confirmAddedData.size();i++){
                            if(Global.confirmAddedData.get(i).get("Channel").equalsIgnoreCase(name)) {
                                Global.confirmAddedData.remove(i);
                            }
                        }
                    }

                }else{
                    userData.get(pos).setSelected(true);
                    int ChannelBouquetCount = Integer.parseInt(userData.get(pos).getTotalchannel());
                    int count = Global.channelCount+ChannelBouquetCount;
                    Global.channelCount = count;
                    Log.d("channel_count", String.valueOf(Global.channelCount));
                    n = n + 1;
                    String payChannellist = String.valueOf(n);
                    int payBouquet = Global.TotalBouquet+1;
                    String set_paid = String.valueOf(payBouquet);
                    Global.addBouquetChannel = Integer.parseInt(payChannellist);
                    if(ChannelsBouquetActivity.getBouquetText()!=null){
                        ChannelsBouquetActivity.getBouquetText().setText(String.valueOf(set_paid));
                        Global.TotalBouquet = payBouquet;
                    }
                    Global.selectedBouquet = userData;
                    viewHolder.mainLay.setBackgroundColor(Color.parseColor("#efefef"));
                    final String name = userData.get(position).getBouquet();
                    String broadcaster = userData.get(position).getBroadcaster();
                    String channel_count = userData.get(position).getTotalchannel();
                    String Bouquet_Id = userData.get(position).getBouquet_id();
                    String isBouqet = "Yes";
                    HashMap<String,String> addedData = new HashMap<String,String>();
                    addedData.put("Broadcaster",broadcaster);
                    addedData.put("Bouquet_Id",Bouquet_Id);
                    addedData.put("Channel",name);
                    addedData.put("isBouqet",isBouqet);
                    addedData.put("price",userData.get(position).getBouquetprice());
                    addedData.put("channel_count",channel_count);
                    confirmData.add(addedData);
                    Global.confirmAddedBouquetData = confirmData;
                    final String bouquetuniquename = name;
                    // added bouquet price in .......//
                    if(Global.TotalSubscriptionPrice>0){
                        bouquetTotal = Global.TotalSubscriptionPrice;
                    }
                    double s = Double.parseDouble(userData.get(position).getBouquetprice());
                    if(Global.channelCount<100){
                        Global.bouquetprice = Global.bouquetprice+s;
                        double Totalchannelbouquet = Global.paidchannelPrice+ Global.bouquetprice;
                        double Final = 130+Totalchannelbouquet;
                        double gt = Final*18/100;
                        double pricetotal = Final+gt;
                        Global.TotalSubscriptionPrice = Double.parseDouble(new DecimalFormat("##.##").format(pricetotal));
                        if(ChannelsBouquetActivity.getTotalPrice()!=null){
                            ChannelsBouquetActivity.getlayout_main().setVisibility(View.VISIBLE);
                            ChannelsBouquetActivity.getTotalPrice().setText(String.valueOf(Double.parseDouble(new DecimalFormat("##.##").format(pricetotal))));
                        }
                    }else{
                        Global.bouquetprice = Global.bouquetprice+s;
                        double n = Global.channelCount;
                        //double p  = Math.ceil((n - 100)/ 25 ) * 20;
                        /*Log.d("pqr", String.valueOf(p));
                        double k = 20.0;
                        double gt = k*18/100;
                        double lgt = k+gt;
                        double gst = s*18/100;
                        double sn = s+gst;
                            if(Global.countcheckIncrement==p){
                                bouquetTotal = Global.TotalSubscriptionPrice;
                                Global.countcheckIncrement=0;
                            }else{
                                bouquetTotal = Global.TotalSubscriptionPrice+lgt;
                                Global.countcheckIncrement = p;
                            }*/


                        double pricetotal = Const.getuniquechannelcount(Global.channelCount);
                        bouquetTotal = pricetotal;
                        Global.TotalSubscriptionPrice = Double.parseDouble(new DecimalFormat("##.##").format(pricetotal));
                        if(ChannelsBouquetActivity.getTotalPrice()!=null){
                            ChannelsBouquetActivity.getlayout_main().setVisibility(View.VISIBLE);
                            ChannelsBouquetActivity.getTotalPrice().setText(String.valueOf(Double.parseDouble(new DecimalFormat("##.##").format(pricetotal))));
                        }
                    }

                    final String bouquetprice = userData.get(position).getBouquetprice();
                    final String bouquetpriceperchannel = "1.25";
                    String bouquet_id = userData.get(position).getBouquet_id();
                    if(subBouquetDto!=null && subBouquetDto.size()>0){
                        for(int i =0; i<subBouquetDto.size();i++){
                            String subbouquetid = subBouquetDto.get(i).getBouquet_id();
                            if(subbouquetid.equalsIgnoreCase(bouquet_id)){
                                BouquetSubDto prodto = new BouquetSubDto();
                                prodto = subBouquetDto.get(i);
                                String BouquetName = name;
                                String  Channel = prodto.getChannel();
                                String category        =  prodto.getCategory();
                                String language  =  prodto.getLanguage();
                                String HD        =  prodto.getHd();
                                String price   = prodto.getPrice();
                                String ImageUrl   = prodto.getImageurl();
                                String Bouquet_Ids = prodto.getBouquet_id();
                                String selection = "false";
                                HashMap<String,String> naddedData = new HashMap<String,String>();
                                naddedData.put("Channel",Channel);
                                naddedData.put("category",category);
                                if(language!=null){
                                    naddedData.put("language",language);
                                }
                                if(HD!=null){
                                    naddedData.put("HD",HD);
                                }

                                naddedData.put("price",price);
                                naddedData.put("ImageUrl",ImageUrl);
                                naddedData.put("selection",selection);
                                naddedData.put("Bouquet",isBouqet);
                                naddedData.put("BouquetName",BouquetName);
                                naddedData.put("bouquetuniquename",bouquetuniquename);
                                naddedData.put("bouquetprice",bouquetprice);
                                naddedData.put("bouquetpriceperchannel",bouquetpriceperchannel);
                                naddedData.put("Bouquet_Id",Bouquet_Ids);
                                confirmDatahash.add(naddedData);
                                Global.confirmAddedBouquetDataHash = confirmDatahash;

                            }
                        }
                    }

                }
                notifyDataSetChanged();

            }
        });

        viewHolder.iv_arrow_Collapse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                viewHolder.iv_arrow_Collapse.setVisibility(View.GONE);
                viewHolder.img_drop_down.setVisibility(View.VISIBLE);
                viewHolder.rec_bottom.setVisibility(View.GONE);
            }
        });


    }



    @Override
    public int getItemCount() {
        return userData.size();
    }

    public void filterData(String newText) {

        newText = newText.toLowerCase(Locale.getDefault());
        userData.clear();
        if (newText.length() == 0) {
            userData.addAll(arraylist);
            Flagsearch = "false";
        } else {
            Flagsearch = "true";
            for (BouquetDto wp : arraylist) {
                if (wp.getBouquet().toLowerCase(Locale.getDefault())
                        .contains(newText)) {
                    userData.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView iv_channels,img_drop_down;
        private TextView tv_broadcaster,tv_channel,tv_price,tv_hd,bouqet_name,bouqet_price,tv_language,tv_category,bouqet_channel_name;
        private CheckBox img_select_bouquet;
        private RecyclerView rec_bottom;
        private ImageView iv_arrow_Collapse;
        RelativeLayout mainLay;

        public MyViewHolder(View view) {
            super(view);
            final int TAG = position;
            // get the reference of item view's
            tv_broadcaster     = (TextView) view.findViewById(R.id.tv_broadcaster);
            tv_channel     = (TextView) view.findViewById(R.id.tv_channel);
            tv_price     = (TextView) view.findViewById(R.id.tv_price);
            tv_category = (TextView)view.findViewById(R.id.tv_category);
            img_drop_down = (ImageView) view.findViewById(R.id.img_drop_down);
            rec_bottom    = (RecyclerView)view.findViewById(R.id.rec_bottom);
            iv_arrow_Collapse = (ImageView)view.findViewById(R.id.iv_arrow_Collapse);
            img_select_bouquet = (CheckBox) view.findViewById(R.id.img_select_bouquet);
            mainLay = (RelativeLayout)view.findViewById(R.id.mainLay);
            LinearLayoutManager layoutManager = new LinearLayoutManager(context.getApplicationContext());
            rec_bottom.setLayoutManager(layoutManager);



        }
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }


}

