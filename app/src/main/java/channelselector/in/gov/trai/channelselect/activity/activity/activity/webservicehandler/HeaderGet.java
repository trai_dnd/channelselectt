package channelselector.in.gov.trai.channelselect.activity.activity.activity.webservicehandler;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

public class HeaderGet {


    public String getData(String url, String accessToken) throws IOException, JSONException {

        String jResponse = null;

        DefaultHttpClient httpClient = new DefaultHttpClient();

        HttpGet httpGet = new HttpGet(url);
        httpGet.setHeader("Authorization", "Bearer "+accessToken);


        //httpGet.setHeader("Accept", "*/*");
        try {
            String result = URLDecoder.decode(url, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        try {

            HttpResponse httpResponse = httpClient.execute(httpGet);

            HttpEntity httpEntity = httpResponse.getEntity();

            jResponse = EntityUtils.toString(httpEntity);


        }/*catch (NullPointerException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }*/  catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return jResponse;

    }

}

