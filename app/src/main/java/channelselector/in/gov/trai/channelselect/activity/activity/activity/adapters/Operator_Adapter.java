package channelselector.in.gov.trai.channelselect.activity.activity.activity.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import channelselector.in.gov.trai.channelselect.R;

public class Operator_Adapter extends BaseAdapter {

    Context context;
    ArrayList<String> list;

    public Operator_Adapter(Context context, ArrayList<String> list)
    {
        this.context = context;
        this.list    = list;
    }
    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.textspinneradapter, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }


        viewHolder.text.setText(list.get(position));


        return convertView;
    }

    private class ViewHolder {
        TextView text;

        public ViewHolder(View view) {
            text    = (TextView) view.findViewById(R.id.text);

        }
    }
}

