package channelselector.in.gov.trai.channelselect.activity.activity.activity.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import channelselector.in.gov.trai.channelselect.R;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.DTO.MySubsFree;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.DTO.MySubsPiad;


public class RecyclerSubChoice extends RecyclerView.Adapter<RecyclerSubChoice.MyViewHolder> {

    private Context context;
    //private List<PaidChannelDTO> userData;
    private MyViewHolder viewHolder;
    private int position;
    int pos;
    List<String> parent_data;
    ArrayList<HashMap<String,String>> data;
    String check;
    private ArrayList<String>selectedNamefree =  new ArrayList<String>();
    private ArrayList<String>selectedNamepaid  = new ArrayList<String>();
    ArrayList<MySubsFree> freeDto;
    ArrayList<MySubsPiad> paidDto;
    String bouquet;
    private ArrayList<HashMap<String,String>>confirmDatapaid= new ArrayList<HashMap<String,String>>();
    private ArrayList<HashMap<String,String>>confirmDatafree= new ArrayList<HashMap<String,String>>();
    int free;
    int paid ;
    int bouquetcount;
    int freeFlag =0;
    int paidFlag =0;
    ArrayList<String> channelsData;
    ArrayList<String> arrayUrls;


    public RecyclerSubChoice(Context context, ArrayList<String> channelsData, ArrayList<String> arrayUrls, String bouquet) {
        this.context = context;
        this.channelsData = channelsData;
        this.arrayUrls = arrayUrls;
        this.check = bouquet;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
// infalte the item Layout
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_sub_choice, parent, false);
// set the view's size, margins, paddings and layout parameters
        MyViewHolder viewHolder = new MyViewHolder(v); // pass the view to View Holder



        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder viewHolder, final int position) {
        //Log.e("DATATEST=>",userData.get(position).getBroadcaster());
        this.viewHolder = viewHolder;
        this.position = position;
        final int row_index=position;
        //viewHolder.tv_broadcaster.setText(data.get(position).get("Broadcaster"));

        if(check.equalsIgnoreCase("Mandatory Channels")){

            viewHolder.tv_channel.setFilters(new InputFilter[]{ new InputFilter.LengthFilter(25) });
            viewHolder.tv_channel.setText(channelsData.get(position));
            String path =arrayUrls.get(position);
            Glide.with(context)
                    .asBitmap()
                    .load(path)
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                            viewHolder.iv_channels.setImageBitmap(resource);
                        }
                    });
        }


    }



    @Override
    public int getItemCount() {

        if(check.equalsIgnoreCase("Mandatory Channels")){

            return channelsData.size();
        }else{

            return paidDto.size();
        }

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView iv_channels,remove_channel,add_channel,restore_channel;
        private TextView tv_broadcaster,tv_channel,tv_price,tv_hd,bouqet_name,bouqet_price,tv_language,tv_category,bouqet_channel_name,tv_restore,tv_delete,tv_line;
        private RelativeLayout mainLay;

        public MyViewHolder(View view) {
            super(view);
            int TAG = position;
            // get the reference of item view's
            iv_channels       = (ImageView)view.findViewById(R.id.iv_channels);
            tv_broadcaster     = (TextView) view.findViewById(R.id.tv_broadcaster);
            tv_channel     = (TextView) view.findViewById(R.id.tv_channel);
            tv_price     = (TextView) view.findViewById(R.id.tv_price);
            tv_hd     = (TextView) view.findViewById(R.id.tv_hd);
            tv_category = (TextView)view.findViewById(R.id.tv_category);
            bouqet_name     = (TextView) view.findViewById(R.id.bouqet_name);
            bouqet_price     = (TextView) view.findViewById(R.id.bouqet_price);
            tv_language     = (TextView) view.findViewById(R.id.tv_language);
            bouqet_channel_name = (TextView)view.findViewById(R.id.bouqet_channel_name);
            remove_channel = (ImageView)view.findViewById(R.id.remove_channel);
            mainLay = (RelativeLayout)view.findViewById(R.id.mainLay);
            add_channel = (ImageView)view.findViewById(R.id.add_channel);
            tv_restore = (TextView)view.findViewById(R.id.tv_restore);
            tv_delete = (TextView)view.findViewById(R.id.tv_delete);
            restore_channel = (ImageView)view.findViewById(R.id.restore_channel);
            tv_line = (TextView)view.findViewById(R.id.tv_line);

        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }




}



