package channelselector.in.gov.trai.channelselect.activity.activity.activity.classes;

import com.android.volley.toolbox.JsonObjectRequest;

interface ApiSingleton {
    void addToRequestQueue(JsonObjectRequest jsonObjectRequest);
}
