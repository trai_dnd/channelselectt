package channelselector.in.gov.trai.channelselect.activity.activity.activity.classes;

public class SetSubscriptionRequest {


    private Long subscription_id;
    private BouquetRequest bouquet;
    private ChannelRequest channels;

    private Double amount;
    private String type;



	public BouquetRequest getBouquet() {
		return bouquet;
	}
	public void setBouquet (BouquetRequest bouquet) {
		this.bouquet = bouquet;
	}
	public ChannelRequest getChannels() {
		return channels;
	}
	public void setChannels(ChannelRequest channels) {
		this.channels = channels;
	}
	public Long getSubscription_id() {
		return subscription_id;
	}
	public void setSubscription_id(long subscription_id) {
		this.subscription_id = subscription_id;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

}