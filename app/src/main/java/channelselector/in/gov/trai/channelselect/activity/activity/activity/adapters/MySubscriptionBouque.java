package channelselector.in.gov.trai.channelselect.activity.activity.activity.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.JsonArray;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import channelselector.in.gov.trai.channelselect.R;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.DTO.MySubBouquet;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.DTO.MySubsChild;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.Extras.Const;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.Extras.Global;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.classes.MySubscription;

public class MySubscriptionBouque extends RecyclerView.Adapter<MySubscriptionBouque.MyViewHolder> {

    private Context context;
    private MyViewHolder viewHolder;
    private int position;
    List<MySubBouquet> bouquetData;
    List<MySubsChild> mdata = new ArrayList<MySubsChild>();
    ArrayList<HashMap<String,String>> newData = new ArrayList<HashMap<String,String>>();
    private RecyclerViewSubSubription subAdapter;
    private ArrayList<String>selectedbouquetName =  new ArrayList<String>();
    JsonArray myCustomArray;
    private ProgressDialog prog_dialog;
    //HashMap<String, ArrayList<UserDataDto>> listDataChild;
    private ArrayList<HashMap<String,String>>arr_Data = new ArrayList<HashMap<String,String>>();
    private ArrayList<HashMap<String,String>>arr_freeData = new ArrayList<HashMap<String,String>>();
    private ArrayList<HashMap<String,String>>mbouquetData = new ArrayList<HashMap<String,String>>();
    private ArrayList<String> subscriberBouquetselection;
    List<String> listDataHeader = new ArrayList<String>();
    private ArrayList<HashMap<String,String>> confirmData= new ArrayList<HashMap<String,String>>();
    private BougquetSubscriptionAdapter adapter;
    int Flag;
    int bouquetcount =0;
    private ArrayList<MySubsChild> muserData = new ArrayList<MySubsChild>();
    /*private ArrayList<MySubscriptionDto> userData = new ArrayList<MySubscriptionDto>();

    private ArrayList<MySubscriptionDto> mainDataFree = new ArrayList<MySubscriptionDto>();*/
    public MySubscriptionBouque(Context context, List<MySubBouquet> bouquetName) {
        this.context = context;
        this.bouquetData = bouquetName;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
// infalte the item Layout
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_sub_bouquet, parent, false);
// set the view's size, margins, paddings and layout parameters
        MyViewHolder viewHolder = new MyViewHolder(v); // pass the view to View Holder


        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder viewHolder, final int position) {
        //Log.e("DATATEST=>",userData.get(position).getBroadcaster());
        this.viewHolder = viewHolder;
        this.position = position;
        if(Global.deletedbouquesubscription!=null){
            if(Global.deletedbouquesubscription.size()>0){
                for(int i =0;i<Global.deletedbouquesubscription.size();i++){
                    if(Global.deletedbouquesubscription.get(i).get("Bouquet_Id").equalsIgnoreCase(bouquetData.get(position).getBouquet_id())){
                        viewHolder.tv_choce_name.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                        viewHolder.iv_restore_image.setVisibility(View.VISIBLE);
                        viewHolder.iv_delete_bouquet.setVisibility(View.GONE);
                        viewHolder.tv_restore.setVisibility(View.VISIBLE);
                        viewHolder.tv_delete.setVisibility(View.GONE);
                    }else{
                        //do nothing.....
                    }
                }
            }
        }
        viewHolder.tv_choce_name.setFilters(new InputFilter[]{ new InputFilter.LengthFilter(25) });
        viewHolder.tv_choce_name.setText(bouquetData.get(position).getBouquet());
        viewHolder.tv_bouquet_price.setText(context.getResources().getString(R.string.Rs)+" "+bouquetData.get(position).getBouquetPrice());
        final String name = bouquetData.get(position).getBouquet_id();

        viewHolder.img_drop_down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(newData!=null && newData.size()>0){
                    newData.clear();
                }else{
                    //do nothing...
                }
                mdata = Global.BouquetsubTotal;
                if(mdata!=null){
                    for(int i =0;i<mdata.size();i++){
                        String id = bouquetData.get(position).getBouquet_id();
                        if(mdata.get(i).getBouquet_id()!=null) {
                            if (mdata.get(i).getBouquet_id().equalsIgnoreCase(name)) {
                                HashMap<String, String> bouquet_list = new HashMap<String, String>();
                                bouquet_list.put("Broadcaster", (mdata.get(i).getBroadcaster()));
                                bouquet_list.put("Channel", (mdata.get(i).getChannel()));
                                bouquet_list.put("category", (mdata.get(i).getCategory()));
                                bouquet_list.put("price", (mdata.get(i).getPrice()));
                                bouquet_list.put("language", (mdata.get(i).getLanguage()));
                                bouquet_list.put("HD", (mdata.get(i).getHd()));
                                bouquet_list.put("ImageUrl", (mdata.get(i).getImageurl()));
                                newData.add(bouquet_list);
                            } else {
                                //do nothing...
                            }
                        }
                    }
                }
                viewHolder.rec_subscriber.setVisibility(View.VISIBLE);
                String Bouquet = "bouguet";
                adapter = new BougquetSubscriptionAdapter(context,newData,Bouquet);
                viewHolder.rec_subscriber.setAdapter(adapter);


                viewHolder.iv_arrow_Collapse.setVisibility(View.VISIBLE);
                viewHolder.img_drop_down.setVisibility(View.GONE);
                viewHolder.rec_subscriber.setVisibility(View.VISIBLE);


            }
        });

        viewHolder.iv_arrow_Collapse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                viewHolder.iv_arrow_Collapse.setVisibility(View.GONE);
                viewHolder.img_drop_down.setVisibility(View.VISIBLE);
                viewHolder.rec_subscriber.setVisibility(View.GONE);
            }
        });

        viewHolder.iv_delete_bouquet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(newData!=null && newData.size()>0){
                    newData.clear();
                }else{
                    //do nothing...
                }
                Flag =0;
                mdata = new ArrayList<MySubsChild>();
                mdata = Global.BouquetsubTotal;
                bouquetcount = Global.TotalBouquet;
                String set_bouquet = String.valueOf(bouquetcount-1);
                MySubscription.getBouquetText().setText(set_bouquet);
                Global.TotalBouquet = Integer.parseInt(set_bouquet);
                if(Global.TotalBouquet==0){
                    Global.setAlterBouquet = "setAlterBouquet";
                }
                if(mdata!=null){
                    viewHolder.tv_choce_name.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                    viewHolder.iv_restore_image.setVisibility(View.VISIBLE);
                    viewHolder.iv_delete_bouquet.setVisibility(View.GONE);
                    viewHolder.tv_delete.setVisibility(View.GONE);
                    viewHolder.tv_restore.setVisibility(View.VISIBLE);
                    String viewname = bouquetData.get(position).getBouquet_id();
                    if(Global.CheckBouquetPriceDto!=null && Global.CheckBouquetPriceDto.size()>0){
                        for(int i =0;i<Global.CheckBouquetPriceDto.size();i++){
                            if(Global.CheckBouquetPriceDto.get(i).getBouquet_id().equals(viewname)){
                                String price = Global.CheckBouquetPriceDto.get(i).getBouquetprice();
                                int ChannelBouquetCount = Integer.parseInt(Global.CheckBouquetPriceDto.get(i).getTotalchannel());
                                int count = Global.channelCount-ChannelBouquetCount;
                                Global.channelCount = count;
                                double s = Double.parseDouble(price);
                                if(Global.channelCount<100){
                                    Global.bouquetprice = Global.bouquetprice -s;
                                    double Totalchannelbouquet = Global.paidchannelPrice+ Global.bouquetprice;
                                    double Final = 130+Totalchannelbouquet;
                                    double gt = Final*18/100;
                                    double pricetotal = Final+gt;
                                    Global.TotalSubscriptionPrice = Double.parseDouble(new DecimalFormat("##.##").format(pricetotal));
                                    if(MySubscription.getTotalPrice()!=null){
                                        MySubscription.getTotalPrice().setText(String.valueOf(Double.parseDouble(new DecimalFormat("##.##").format(pricetotal))));
                                    }

                                }else{
                                    Global.bouquetprice = Global.bouquetprice -s;
                                    double n = Global.channelCount;
                                    double pricechk  = Const.getuniquechannelcount(Global.channelCount);
                                    /*double k = 20.0;
                                    double gt = k*18/100;
                                    double lgt = k+gt;

                                        double pricetotal = Global.TotalSubscriptionPrice-lgt;
                                        double gst = s*18/100;
                                        double sn = s+gst;
                                         pricechk = pricetotal - sn;*/
                                        Global.TotalSubscriptionPrice = Double.parseDouble(new DecimalFormat("##.##").format(pricechk));
                                        if(MySubscription.getTotalPrice()!=null){
                                            MySubscription.getTotalPrice().setText(String.valueOf(Double.parseDouble(new DecimalFormat("##.##").format(pricechk))));



                                    }

                                }


                            }else{
                                // do nothing...
                            }
                        }
                    }
                    for(int i =0;i<mdata.size();i++){
                        String name = bouquetData.get(position).getBouquet_id();

                        if(mdata.get(i).getBouquet_id()!=null){
                            if(mdata.get(i).getBouquet_id().equalsIgnoreCase(name)){
                                HashMap<String, String> bouquet_list = new HashMap<String, String>();
                                bouquet_list.put("Broadcaster", (mdata.get(i).getBroadcaster()));
                                bouquet_list.put("Channel", (mdata.get(i).getChannel()));
                                bouquet_list.put("category", (mdata.get(i).getCategory()));
                                bouquet_list.put("price", (mdata.get(i).getPrice()));
                                bouquet_list.put("language", (mdata.get(i).getLanguage()));
                                bouquet_list.put("HD", (mdata.get(i).getHd()));
                                bouquet_list.put("ImageUrl", (mdata.get(i).getImageurl()));
                                bouquet_list.put("Bouquet_Id",(mdata.get(i).getBouquet_id()));
                                newData.add(bouquet_list);
                                if(Flag ==0) {
                                    HashMap<String, String> arrbouque = new HashMap<String, String>();
                                    String isBouqet = "Yes";
                                    arrbouque.put("Channel", mdata.get(i).getBouquet_name());
                                    arrbouque.put("price", mdata.get(position).getBouquet_price());
                                    arrbouque.put("Bouquet_Id",mdata.get(i).getBouquet_id());
                                    arrbouque.put("isBouqet", isBouqet);
                                    confirmData.add(arrbouque);
                                    Global.deletedbouquesubscription = confirmData;
                                    Flag =1;
                                }else{
                                    //do nothing.....
                                }

                                MySubsChild userDataDto = new MySubsChild();
                                userDataDto.setBroadcaster((mdata.get(i).getBroadcaster()));
                                userDataDto.setChannel(mdata.get(i).getChannel());
                                userDataDto.setCategory(mdata.get(i).getCategory());
                                userDataDto.setLanguage(mdata.get(i).getLanguage());
                                userDataDto.setHd(mdata.get(i).getHd());
                                userDataDto.setPrice(mdata.get(i).getPrice());
                                userDataDto.setImageurl(mdata.get(i).getImageurl());
                                userDataDto.setBouquet_name(mdata.get(i).getBouquet_name());
                                userDataDto.setBouquet_price(mdata.get(i).getBouquet_price());
                                userDataDto.setBouquet_id(mdata.get(i).getBouquet_id());
                                muserData.add(userDataDto);
                                Global.DeletedBouquetData = muserData;
                            }
                        }

                        else{
                            //do nothing...
                        }
                        //Toast.makeText(context,"Bouquet Selected", Toast.LENGTH_SHORT).show();
                    }
                    //Toast.makeText(context,"Bouquet Selected", Toast.LENGTH_SHORT).show();


                }


            }
        });

        viewHolder.iv_restore_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewHolder.tv_choce_name.setPaintFlags(viewHolder.tv_choce_name.getPaintFlags() & ~Paint.STRIKE_THRU_TEXT_FLAG);
                viewHolder.iv_restore_image.setVisibility(View.GONE);
                viewHolder.iv_delete_bouquet.setVisibility(View.VISIBLE);
                viewHolder.tv_delete.setVisibility(View.VISIBLE);
                viewHolder.tv_restore.setVisibility(View.GONE);
                String name = bouquetData.get(position).getBouquet_id();
                if(Global.CheckBouquetPriceDto!=null && Global.CheckBouquetPriceDto.size()>0){
                    for(int i =0;i<Global.CheckBouquetPriceDto.size();i++){
                        if(Global.CheckBouquetPriceDto.get(i).getBouquet_id().equalsIgnoreCase(name)){
                            String price = Global.CheckBouquetPriceDto.get(i).getBouquetprice();
                            int ChannelBouquetCount = Integer.parseInt(Global.CheckBouquetPriceDto.get(i).getTotalchannel());
                            int count = Global.channelCount+ChannelBouquetCount;
                            Global.channelCount = count;
                            double s = Double.parseDouble(price);
                            if(Global.channelCount<100){
                                Global.bouquetprice = Global.bouquetprice+s;
                                double Totalchannelbouquet = Global.paidchannelPrice+ Global.bouquetprice;
                                double Final = 130+Totalchannelbouquet;
                                double gt = Final*18/100;
                                double pricechk = Final+gt;
                                Global.TotalSubscriptionPrice = Double.parseDouble(new DecimalFormat("##.##").format(pricechk));
                                if(MySubscription.getTotalPrice()!=null){
                                    MySubscription.getTotalPrice().setText(String.valueOf(Double.parseDouble(new DecimalFormat("##.##").format(pricechk))));
                                }

                            }else{
                                Global.bouquetprice = Global.bouquetprice+s;
                                double n = Global.channelCount;
                                double counts = Const.getuniquechannelcount(Global.channelCount);
                                double pricechk  = Const.getuniquechannelcount(Global.channelCount);
                                /*double k = 20.0;
                                double gt = k*18/100;
                                double lgt = k+gt;

                                    double pricetotal = Global.TotalSubscriptionPrice+lgt;
                                    double gst = s*18/100;
                                    double sn = s+gst;
                                    double pricechk = pricetotal+sn;*/
                                    Global.TotalSubscriptionPrice = Double.parseDouble(new DecimalFormat("##.##").format(pricechk));
                                    if(MySubscription.getTotalPrice()!=null){
                                        MySubscription.getTotalPrice().setText(String.valueOf(Double.parseDouble(new DecimalFormat("##.##").format(pricechk))));


                                }

                            }
                        }else{
                            // do nothing...
                        }
                    }
                }
                bouquetcount = Global.TotalBouquet;
                String set_bouquet = String.valueOf(bouquetcount+1);
                MySubscription.getBouquetText().setText(set_bouquet);
                Global.TotalBouquet = Integer.parseInt(set_bouquet);
                if(muserData!=null && muserData.size()>0){

                    for(int i = muserData.size()-1; i>0 ;i--){
                        if(muserData.get(i).getBouquet_id().equalsIgnoreCase(name)){
                            muserData.remove(i);
                            Global.DeletedBouquetData = muserData;
                        }
                    }if(muserData!=null && muserData.size()>0){
                        for(int i = 0; i<muserData.size();i++){
                            if(muserData.get(i).getBouquet_id().equalsIgnoreCase(name)){
                                muserData.remove(i);
                                Global.DeletedBouquetData = muserData;
                            }
                        }
                    }
                }else{
                    if(Global.DeletedBouquetData!=null){
                        if(Global.DeletedBouquetData.size()>0){
                            for( int i = Global.DeletedBouquetData.size()-1; i>0 ; i--){
                                if(Global.DeletedBouquetData.get(i).getBouquet_id().equalsIgnoreCase(name)){
                                    Global.DeletedBouquetData.remove(i);
                                }else{
                                    // do nothing...
                                }
                            }
                        }
                    }if(Global.DeletedBouquetData!=null && Global.DeletedBouquetData.size()>0){
                        for( int i = Global.DeletedBouquetData.size()-1; i>0 ; i--){
                            if(Global.DeletedBouquetData.get(i).getBouquet_id().equalsIgnoreCase(name)){
                                Global.DeletedBouquetData.remove(i);
                            }else{
                                 //do nothing...
                            }
                        }
                    }
                }if(confirmData!=null && confirmData.size()>0){
                    for(int i =0; i<confirmData.size();i++){
                        if(confirmData.get(i).get("Bouquet_Id").equalsIgnoreCase(name)){
                            confirmData.remove(i);
                            Global.deletedbouquesubscription = confirmData;

                        }else{
                            // do nothing...
                        }
                    }
                }else{
                    if( Global.deletedbouquesubscription!=null){
                        if( Global.deletedbouquesubscription.size()>0){
                            for(int i =0; i< Global.deletedbouquesubscription.size();i++){
                                if( Global.deletedbouquesubscription.get(i).get("Bouquet_Id").equalsIgnoreCase(name)){
                                    Global.deletedbouquesubscription.remove(i);
                                }else{
                                    // do nothing...
                                }
                            }
                        }
                    }
                }
            }
        });

    }



    @Override
    public int getItemCount() {
        return bouquetData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView iv_channels,iv_delete_bouquet,img_drop_down,iv_restore_image;
        private TextView tv_choce_name,tv_restore,tv_delete,tv_bouquet_price;
        private ImageButton iv_arrow_Collapse;
        private RecyclerView rec_subscriber;
       /* private ImageButton iv_delete_bouquet;*/

        public MyViewHolder(View view) {
            super(view);
            int TAG = position;
            // get the reference of item view's
            tv_choce_name     = (TextView) view.findViewById(R.id.tv_choce_name);
            img_drop_down    = (ImageView) view.findViewById(R.id.img_drop_down);
            iv_arrow_Collapse    = (ImageButton)view.findViewById(R.id.iv_arrow_Collapse);
            rec_subscriber  = (RecyclerView)view.findViewById(R.id.rec_subscriber);
            iv_delete_bouquet = (ImageView) view.findViewById(R.id.iv_delete_bouquet);
            iv_restore_image = (ImageView)view.findViewById(R.id.iv_restore_image);
            tv_delete  = (TextView)view.findViewById(R.id.tv_delete);
            tv_restore  = (TextView)view.findViewById(R.id.tv_restore);
            tv_bouquet_price = (TextView)view.findViewById(R.id.tv_bouquet_price);
            LinearLayoutManager layoutManager = new LinearLayoutManager(context.getApplicationContext());
            rec_subscriber.setLayoutManager(layoutManager);

        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }



   /* public class UpdateTask extends AsyncTask<JsonArray, Integer, String> {

        String response;
        String customerId = Global.CustomerID;
        String mobile = Global.MobileNumber;
        String Url = Global.setsubscription+customerId+"/"+mobile;
        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();

            prog_dialog = new ProgressDialog(context);
            prog_dialog.setMessage("Please wait....");
            prog_dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            prog_dialog.setCancelable(true);
            prog_dialog.show();

        }

        @Override
        protected String doInBackground(JsonArray... params) {
            try {

                response = new GetResponse().postDataobject(Url, params[0]);

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return response;
        }



        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);

            //prog_dialog.dismiss();

            if (result != null) {

                //Global.selectedFree.clear();
                // Global.selectedPiad.clear();
                Toast.makeText(context, "Updated Successfully...", Toast.LENGTH_LONG).show();
                Global.refreshToken = result;
                //getdata();
                ViewDialog();
                prog_dialog.dismiss();


            }

            else {

                prog_dialog.dismiss();
            }

        }


    }*/

    /*private void getdata() {
        String response;
        String Broadcaster;
        String Channel ;
        String category;
        String bouquetuniquename;
        String bouquet;
        String bouquetname ;
        String bouquetprice ;
        String bouquetpriceperchannel;
        String price;
        String language;
        String HD;
        String ImageUrl;

        //object = new JSONObject(result);
        listDataHeader = new ArrayList<String>();
        *//*listDataChild = new HashMap<String, ArrayList<UserDataDto>>();*//*
        // JSONArray jsonArray = new JSONArray(myCustomArray);
        ArrayList<UserDataDto> top250 = new ArrayList<UserDataDto>();
        ArrayList<PaidChannelDTO> paidDto = new ArrayList<PaidChannelDTO>();
        ArrayList<FreeChannelDTO> freeDto = new ArrayList<FreeChannelDTO>();
        ArrayList<String>arr_bouquet = new ArrayList<String>();
        String Flag = "First";
        for (int i = 0; i < myCustomArray.size(); i++) {
            //JSONObject obj = jsonArray.getJSONObject(i);
            JsonObject obj = myCustomArray.get(i).getAsJsonObject();
            price   = obj.get("price").getAsString();
            if(obj.has("bouquet")){
                Broadcaster = obj.get("Broadcaster").getAsString();
                Channel = obj.get("Channel").getAsString();
                category        = obj.get("category").getAsString();
                language  = obj.get("language").getAsString();
                HD        = obj.get("HD").getAsString();
                ImageUrl   = obj.get("imageurl").getAsString();
                if(ImageUrl.contains("http")){
                    //do nothing...
                }else{
                    ImageUrl = "http://59.179.24.45:8081"+"/"+obj.get("imageurl").getAsString();
                }
                bouquetuniquename = obj.get("bouquetuniquename").getAsString();
                bouquet = obj.get("bouquet").getAsString();
                bouquetname        = obj.get("bouquetname").getAsString();
                if(Flag.equalsIgnoreCase("First")){
                    arr_bouquet.add(bouquetname);
                    Flag = "Second";
                }else{
                    Flag = "Second";
                    for(int k=0;k<arr_bouquet.size();k++){
                        if(arr_bouquet.get(k).equalsIgnoreCase(bouquetname)){
                            //do nothing...
                        }else{
                            arr_bouquet.add(bouquetname);

                        }
                    }
                }


                bouquetprice  = obj.get("bouquetprice").getAsString();
                bouquetpriceperchannel        = obj.get("bouquetpriceperchannel").getAsString();

                HashMap<String,String>bouquet_list = new HashMap<String,String>();
                bouquet_list.put("Broadcaster",Broadcaster);
                bouquet_list.put("Channel",Channel);
                bouquet_list.put("category",category);
                bouquet_list.put("price",price);
                bouquet_list.put("language",language);
                bouquet_list.put("HD",HD);
                bouquet_list.put("ImageUrl",ImageUrl);
                bouquet_list.put("bouquetuniquename",bouquetuniquename);
                bouquet_list.put("bouquet",bouquet);
                bouquet_list.put("bouquetname",bouquetname);
                bouquet_list.put("bouquetprice",bouquetprice);
                bouquet_list.put("bouquetpriceperchannel",bouquetpriceperchannel);
                bouquetData.add(bouquet_list);

                UserDataDto userDataDto = new UserDataDto();
                userDataDto.setBroadcaster(Broadcaster);
                userDataDto.setChannel(Channel);
                userDataDto.setCategory(category);
                userDataDto.setLanguage(language);
                userDataDto.setHD(HD);
                userDataDto.setPrice(price);
                userDataDto.setImageurl(ImageUrl);
                userDataDto.setBouquetuniquename(bouquetuniquename);
                userDataDto.setBouquet(bouquet);
                userDataDto.setBouquetname(bouquetname);
                userDataDto.setBouquetprice(bouquetprice);
                userDataDto.setBouquetpriceperchannel(bouquetpriceperchannel);
                userData.add(userDataDto);

            }
            else{
                if(price.equalsIgnoreCase("0")){
                    Broadcaster = obj.get("Broadcaster").getAsString();
                    Channel = obj.get("Channel").getAsString();
                    category        = obj.get("category").getAsString();
                    price   = obj.get("price").getAsString();
                    ImageUrl   = obj.get("imageurl").getAsString();
                    if(ImageUrl.contains("http")){
                        //do nothing...
                    }else{
                        ImageUrl = "http://59.179.24.45:8081"+"/"+obj.get("imageurl").getAsString();
                    }

                    HashMap<String,String>freedata = new HashMap<String,String>();
                    freedata.put("Broadcaster",Broadcaster);
                    freedata.put("Channel",Channel);
                    freedata.put("category",category);
                    freedata.put("price",price);
                    freedata.put("ImageUrl",ImageUrl);
                    arr_freeData.add(freedata);

                    UserDataDto userDataDto = new UserDataDto();
                    userDataDto.setBroadcaster(Broadcaster);
                    userDataDto.setChannel(Channel);
                    userDataDto.setCategory(category);
                    userDataDto.setPrice(price);
                    userDataDto.setImageurl(ImageUrl);
                    userData.add(userDataDto);

                }else{
                    Broadcaster = obj.get("Broadcaster").getAsString();
                    Channel = obj.get("Channel").getAsString();
                    category        = obj.get("category").getAsString();
                    language  = obj.get("language").getAsString();
                    HD        = obj.get("HD").getAsString();
                    price   = obj.get("price").getAsString();
                    ImageUrl   = obj.get("imageurl").getAsString();
                    if(ImageUrl.contains("http")){
                        //do nothing...
                    }else{
                        ImageUrl = "http://59.179.24.45:8081"+"/"+obj.get("imageurl").getAsString();
                    }

                    HashMap<String,String>piadData = new HashMap<String,String>();
                    piadData.put("Broadcaster",Broadcaster);
                    piadData.put("Channel",Channel);
                    piadData.put("category",category);
                    piadData.put("language",language);
                    piadData.put("HD",HD);
                    piadData.put("price",price);
                    piadData.put("ImageUrl",ImageUrl);
                    arr_Data.add(piadData);

                    UserDataDto userDataDto = new UserDataDto();
                    userDataDto.setBroadcaster(Broadcaster);
                    userDataDto.setChannel(Channel);
                    userDataDto.setCategory(category);
                    userDataDto.setLanguage(language);
                    userDataDto.setHD(HD);
                    userDataDto.setPrice(price);
                    userDataDto.setImageurl(ImageUrl);
                    userData.add(userDataDto);


                }
            }


        }
        HashSet<String> listToSet = new HashSet<String>(arr_bouquet);
        ArrayList<String> listWithoutDuplicates = new ArrayList<String>(listToSet);
        arr_bouquet = listWithoutDuplicates;
        listDataHeader.add("Free Channel");
        listDataHeader.add("Paid Channel");
        listDataHeader.add("Bouquet");
        tDataChild = new HashMap<String, ArrayList<HashMap<String,String>>>();
        tDataChild.put("Paid Channel", arr_Data);
        tDataChild.put("Free Channel", arr_freeData);
        tDataChild.put("Bouquet", bouquetData);
        if(Global.freedata!=null){
            Global.freedata.clear();
        }
        if(Global.paidData!=null){
            Global.paidData.clear();
        }
        if(Global.arrbougqet!=null){
            Global.arrbougqet.clear();
        }
        if(Global.bouquetData!=null){
            Global.bouquetData.clear();
        }
        Global.freedata = arr_freeData;
        Global.paidData = arr_Data;
        Global.arrbougqet = arr_bouquet;
        Global.userDataValue = userData;
        Global.childData = tDataChild;
        Global.parentData = listDataHeader;
        Global.bouquetData = bouquetData;
        prog_dialog.dismiss();
        ViewDialog();


    }*/

    /*private void ViewDialog() {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.customerdilog);

        TextView text = (TextView) dialog.findViewById(R.id.txt_show);
        text.setText(Global.refreshToken);

        Button dialogButton = (Button) dialog.findViewById(R.id.btn_save);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //setAdapter();
                dialog.dismiss();
            }
        });

        dialog.show();

        //setAdapter();
    }
*/


}

