package channelselector.in.gov.trai.channelselect.activity.activity.activity.DTO;

public class ChannelID {

    public long getChannel_id() {
        return channel_id;
    }

    public void setChannel_id(long channel_id) {
        this.channel_id = channel_id;
    }

    public long channel_id;

    public ChannelID(long channel_id )
    {
        this.channel_id = channel_id;
    }


}
