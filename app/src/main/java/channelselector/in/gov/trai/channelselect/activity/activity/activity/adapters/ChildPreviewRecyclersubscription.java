package channelselector.in.gov.trai.channelselect.activity.activity.activity.adapters;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import channelselector.in.gov.trai.channelselect.R;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.DTO.BouquetDto;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.DTO.ChannelDto;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.DTO.MySubsChild;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.DTO.MySubsFree;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.DTO.MySubsPiad;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.Extras.Const;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.Extras.Global;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.classes.MySubscription;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.classes.Review;


public class ChildPreviewRecyclersubscription extends RecyclerView.Adapter<ChildPreviewRecyclersubscription.MyViewHolder> {

    private Context context;
    private List<BouquetDto> userData;
    private MyViewHolder viewHolder;
    private int position;
    int pos;
    private BouquetSubRecycler userRecyclerView;
    ArrayList<HashMap<String, String>> Data;
    HashMap<String,ArrayList<ChannelDto>> GlobalChannelData;
    ArrayList<String> datam;
    String Flag;
    ArrayList<HashMap<String, String>> senderList;
    ArrayList<HashMap<String, String>> nData = new ArrayList<HashMap<String, String>>();
    ArrayList<MySubsChild>nDatamain = new ArrayList<MySubsChild>();
    private PreRecycler adapter;
    int free,paid,bouquet;

    public ChildPreviewRecyclersubscription(Context context, ArrayList<HashMap<String,String>> mySubsFreePaid) {
        this.context=context;
        this.Data=mySubsFreePaid;
        this.GlobalChannelData = Global.channelData;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
// infalte the item Layout
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.child_row_prev_subs, parent, false);
// set the view's size, margins, paddings and layout parameters
        MyViewHolder viewHolder = new MyViewHolder(v); // pass the view to View Holder


        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder viewHolder, final int position) {
        //Log.e("DATATEST=>",userData.get(position).getBroadcaster());
        this.viewHolder = viewHolder;
        this.position = position;
        final String Bouquestatus = Data.get(position).get("isBouqet");
        viewHolder.tv_channel_name.setFilters(new InputFilter[]{ new InputFilter.LengthFilter(25) });
        viewHolder.tv_channel_name.setText(Data.get(position).get("Channel"));
        viewHolder.tv_channel_name.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        String checkprice = Data.get(position).get("price");
        if(checkprice!=null){

            if(Data.get(position).get("price").equalsIgnoreCase("0")){
                viewHolder.tv_price.setText("Free");

                viewHolder.iv_info.setVisibility(View.INVISIBLE);

            }else{
                if( Bouquestatus!=null&& Bouquestatus.equalsIgnoreCase("Yes")){
                    viewHolder.tv_channel_name.setFilters(new InputFilter[]{ new InputFilter.LengthFilter(25) });
                    viewHolder.tv_channel_name.setText(Data.get(position).get("Channel"));
                    viewHolder.iv_info.setVisibility(View.VISIBLE);
                    viewHolder.iv_delete.setVisibility(View.INVISIBLE);
                    if(checkprice!=null){
                        viewHolder.tv_price.setText(context.getResources().getString(R.string.Rs)+" "+Data.get(position).get("price"));
                    }else{

                        //viewHolder.tv_price.setText(context.getResources().getString(R.string.Rs)+" " + "35");
                        if(Global.CheckBouquetPriceDto!=null && Global.CheckBouquetPriceDto.size()>0){
                            for(int i =0;i<Global.CheckBouquetPriceDto.size();i++){
                                if(Global.CheckBouquetPriceDto.get(i).getBouquet().equalsIgnoreCase(Data.get(position).get("Channel"))){
                                    String price = Global.CheckBouquetPriceDto.get(i).getBouquetprice();
                                    viewHolder.tv_price.setText(context.getResources().getString(R.string.Rs)+" " +price);

                                }else{
                                    // do nothing...
                                }
                            }
                        }
                    }

                }else{
                    viewHolder.tv_channel_name.setFilters(new InputFilter[]{ new InputFilter.LengthFilter(25) });
                    viewHolder.tv_channel_name.setText(Data.get(position).get("Channel"));
                    viewHolder.tv_price.setText(context.getResources().getString(R.string.Rs)+" "+Data.get(position).get("price"));
                    viewHolder.iv_info.setVisibility(View.GONE);
                    //viewHolder.iv_delete.setVisibility(View.VISIBLE);
                }
            /*viewHolder.tv_channel_name.setText(Data.get(position).get("Channel"));
            viewHolder.tv_price.setText(context.getResources().getString(R.string.Rs)+" "+Data.get(position).get("price"));
            viewHolder.iv_info.setVisibility(View.INVISIBLE);*/
            }
        }else{
            if( Bouquestatus!=null&& Bouquestatus.equalsIgnoreCase("Yes")){
                viewHolder.tv_channel_name.setFilters(new InputFilter[]{ new InputFilter.LengthFilter(25) });
                viewHolder.tv_channel_name.setText(Data.get(position).get("Channel"));
                viewHolder.iv_info.setVisibility(View.VISIBLE);
                viewHolder.iv_delete.setVisibility(View.INVISIBLE);
                if(checkprice!=null){
                    viewHolder.tv_price.setText(context.getResources().getString(R.string.Rs)+" "+Data.get(position).get("price"));
                }else{

                    if(Global.CheckBouquetPriceDto!=null && Global.CheckBouquetPriceDto.size()>0){
                        for(int i =0;i<Global.CheckBouquetPriceDto.size();i++){
                            if(Global.CheckBouquetPriceDto.get(i).getBouquet().equalsIgnoreCase(Data.get(position).get("Channel"))){
                                String price = Global.CheckBouquetPriceDto.get(i).getBouquetprice();
                                viewHolder.tv_price.setText(context.getResources().getString(R.string.Rs)+" " +price);

                            }else{
                                // do nothing...
                            }
                        }
                    }
                }
            }else{
                viewHolder.tv_channel_name.setFilters(new InputFilter[]{ new InputFilter.LengthFilter(25) });
                viewHolder.tv_channel_name.setText(Data.get(position).get("Channel"));
                viewHolder.tv_price.setText(context.getResources().getString(R.string.Rs)+" "+Data.get(position).get("price"));
                viewHolder.iv_info.setVisibility(View.GONE);
                //viewHolder.iv_delete.setVisibility(View.VISIBLE);
            }
        }
        /*if(Data.get(position).get("price")!=null){
            viewHolder.iv_info.setVisibility(View.INVISIBLE);
        }else{


        }*/


        viewHolder.iv_restore.setVisibility(View.VISIBLE);
        viewHolder.iv_delete.setVisibility(View.INVISIBLE);
        viewHolder.tv_restore_text.setVisibility(View.VISIBLE);

        viewHolder.iv_restore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String name = Data.get(position).get("Channel");
                //for(int i =0;i<Data.size();i++){
                    for(int i =0; i<Data.size();i++){
                    String arraychannelname = Data.get(i).get("Channel");
                    String ch_Id = Data.get(i).get("Bouquet_Id");
                    String price = Data.get(i).get("price");
                    String HD = Data.get(i).get("HD");
                    String Bouquestatus = Data.get(i).get("isBouqet");
                    if(arraychannelname.equalsIgnoreCase(name)){
                        Data.remove(i);
                        notifyItemRemoved(position);
                        notifyItemRangeChanged(position, Data.size());
                        notifyItemRemoved(position);
                        notifyItemRangeChanged(position, Data.size());
                        Flag = "Remove Channel";
                        Global.addtionPartFlag = Flag;
                        Global.setfinal = Data;
                        if( Bouquestatus!=null){
                            if(Bouquestatus.equalsIgnoreCase("Yes")){
                                bouquet = Global.TotalBouquet;
                                String set_bouquet = String.valueOf(bouquet+1);
                                MySubscription.getBouquetText().setText(set_bouquet);
                                Review.getBouquetText().setText(set_bouquet);
                                Global.TotalBouquet = Integer.parseInt(set_bouquet);
                                if(Global.CheckBouquetPriceDto!=null && Global.CheckBouquetPriceDto.size()>0){
                                    for(int z =0;z<Global.CheckBouquetPriceDto.size();z++){
                                        if(Global.CheckBouquetPriceDto.get(z).getBouquet_id().equalsIgnoreCase(ch_Id)){
                                            String pricem = Global.CheckBouquetPriceDto.get(z).getBouquetprice();
                                            int ChannelBouquetCount = Integer.parseInt(Global.CheckBouquetPriceDto.get(z).getTotalchannel());
                                            int count = Global.channelCount+ChannelBouquetCount;
                                            Global.channelCount = count;
                                            double s = Double.parseDouble(pricem);
                                            if(Global.channelCount<100){
                                                Global.bouquetprice = Global.bouquetprice+s;
                                                double Totalchannelbouquet = Global.paidchannelPrice+ Global.bouquetprice;
                                                double Final = 130+Totalchannelbouquet;
                                                double gt = Final*18/100;
                                                double pricetotal = Final+gt;
                                                Global.TotalSubscriptionPrice = Double.parseDouble(new DecimalFormat("##.##").format(pricetotal));
                                                if(MySubscription.getTotalPrice()!=null){
                                                    MySubscription.getTotalPrice().setText(String.valueOf(Double.parseDouble(new DecimalFormat("##.##").format(pricetotal))));
                                                }
                                                if(Review.getTv_price()!=null){
                                                    Review.getTv_price().setText(String.valueOf(Double.parseDouble(new DecimalFormat("##.##").format(pricetotal))));
                                                }
                                            }else{
                                                Global.bouquetprice = Global.bouquetprice+s;
                                                double n = Global.channelCount;
                                                double pricechk  = Const.getuniquechannelcount(Global.channelCount);
                                                /*double k = 20.0;
                                                double gt = k*18/100;
                                                double lgt = k+gt;
                                                    double pricetotal = Global.TotalSubscriptionPrice+lgt;
                                                    double gst = s*18/100;
                                                    double pricechk = pricetotal+s+gst;*/
                                                    Global.TotalSubscriptionPrice = Double.parseDouble(new DecimalFormat("##.##").format(pricechk));
                                                    if(MySubscription.getTotalPrice()!=null){
                                                        MySubscription.getTotalPrice().setText(String.valueOf(Double.parseDouble(new DecimalFormat("##.##").format(pricechk))));
                                                    }
                                                    if(Review.getTv_price()!=null){
                                                        Review.getTv_price().setText(String.valueOf(Double.parseDouble(new DecimalFormat("##.##").format(pricechk))));

                                                }

                                            }

                                        }else{
                                            // do nothing...
                                        }
                                    }
                                }

                                if(Global.DeletedBouquetData!=null){
                                        for(int p =Global.DeletedBouquetData.size()-1; p>0; p--){
                                        if(Global.DeletedBouquetData.get(p).getBouquet_name().equalsIgnoreCase(arraychannelname)){
                                            Global.DeletedBouquetData.remove(p);
                                        }
                                    }
                                    if(Global.DeletedBouquetData!=null && Global.DeletedBouquetData.size()>0){
                                        for(int m = 0; m<Global.DeletedBouquetData.size();m++){
                                            if(Global.DeletedBouquetData.get(m).getBouquet_name().equalsIgnoreCase(arraychannelname)){
                                                Global.DeletedBouquetData.remove(m);
                                            }
                                        }
                                    }
                                }
                            }if(Global.deletedbouquesubscription!=null){
                                for(int j =0 ; j<Global.deletedbouquesubscription.size();j++){
                                    if(Global.deletedbouquesubscription.get(j).get("Channel").equalsIgnoreCase(arraychannelname)){
                                        Global.deletedbouquesubscription.remove(j);
                                    }
                                }
                            }
                            //do nothing.....
                        }else{
                            if(price!=null){
                                if(price.equalsIgnoreCase("0.0")){
                                    ArrayList<MySubsFree> freeDto = new ArrayList<MySubsFree>();
                                    if(Global.mySubscriptionFreeData!=null ){

                                        for(int k =0; k<Global.mySubscriptionFreeData.size();k++){
                                            if(Global.mySubscriptionFreeData.get(k).getChannel().equalsIgnoreCase(arraychannelname)){
                                                freeDto = Global.mySubscriptionFreeData;
                                                freeDto.get(k).setSelected(false);
                                                Global.mySubscriptionFreeData = freeDto;
                                            }
                                        }
                                    }else{
                                        //do nothing...
                                    }if(Global.mysubscfree!=null){
                                        for(int l =0;l<Global.mysubscfree.size();l++){
                                            if(Global.mysubscfree.get(l).get("Channel").equalsIgnoreCase(arraychannelname)){
                                                Global.mysubscfree.remove(l);
                                            }
                                        }
                                    }else{
                                        // do nothing...
                                    }

                                        free = Global.Freechannels;
                                        String set_free = String.valueOf(free+1);
                                        MySubscription.getFreeText().setText(set_free+"+"+25);
                                        Review.getFreeText().setText(set_free+"+"+25);
                                        int count = Global.channelCount+1;
                                        Global.channelCount = count;
                                        Global.Freechannels = Integer.parseInt(set_free);


                                }else{
                                    ArrayList<MySubsPiad> paidDto = new ArrayList<MySubsPiad>();
                                    if(Global.SubscriptionPaidData!=null ){

                                        for(int k =0; k<Global.SubscriptionPaidData.size();k++){
                                            if(Global.SubscriptionPaidData.get(k).getChannel().equalsIgnoreCase(arraychannelname)){
                                                paidDto = Global.SubscriptionPaidData;
                                                paidDto.get(k).setSelected(false);
                                                Global.SubscriptionPaidData = paidDto;
                                            }
                                        }
                                    }else{
                                        //do nothing...
                                    }if(Global.mysubscpaid!=null){
                                        for(int l =0;l<Global.mysubscpaid.size();l++){
                                            if(Global.mysubscpaid.get(l).get("Channel").equalsIgnoreCase(arraychannelname)){
                                                Global.mysubscpaid.remove(l);
                                            }
                                        }
                                    }

                                    paid = Global.PaidChannels;
                                    String set_paid = String.valueOf(paid+1);
                                    MySubscription.getPaidText().setText(set_paid);
                                    Review.getPaidText().setText(set_paid);
                                    Global.PaidChannels = Integer.parseInt(set_paid);
                                    if(HD.equalsIgnoreCase("HD")){
                                        int count = Global.channelCount+2;
                                        Global.channelCount = count;
                                        Log.d("channel_count", String.valueOf(Global.channelCount));
                                    }else{
                                        int count = Global.channelCount+1;
                                        Global.channelCount = count;
                                    }

                                    double s = Double.parseDouble(price);
                                    //Global.paidchannelPrice = Global.paidchannelPrice+s;
                                    if(Global.channelCount<100){
                                        Global.paidchannelPrice = Global.paidchannelPrice+s;
                                        double Totalchannelbouquet = Global.paidchannelPrice+ Global.bouquetprice;
                                        double Final = 130+Totalchannelbouquet;
                                        double gt = Final*18/100;
                                        double pricetotal = Final+gt;
                                        Global.TotalSubscriptionPrice = Double.parseDouble(new DecimalFormat("##.##").format(pricetotal));
                                        if(MySubscription.getTotalPrice()!=null){
                                            MySubscription.getTotalPrice().setText(String.valueOf(Double.parseDouble(new DecimalFormat("##.##").format(pricetotal))));
                                        }
                                        if(Review.getTv_price()!=null){
                                            Review.getTv_price().setText(String.valueOf(Double.parseDouble(new DecimalFormat("##.##").format(pricetotal))));
                                        }
                                    }else{
                                        Global.paidchannelPrice = Global.paidchannelPrice+s;
                                        double n = Global.channelCount;
                                        double pricechk  = Const.getuniquechannelcount(Global.channelCount);
                                        /*double k = 20.0;
                                        double gt = k*18/100;
                                        double lgt = k+gt;
                                        double gst = s*18/100;
                                        double sn = s+gst;

                                            double pricetotal = Global.TotalSubscriptionPrice+lgt;
                                            double pricechk = pricetotal+sn;*/
                                            Global.TotalSubscriptionPrice = Double.parseDouble(new DecimalFormat("##.##").format(pricechk));
                                            if(MySubscription.getTotalPrice()!=null){
                                                MySubscription.getTotalPrice().setText(String.valueOf(Double.parseDouble(new DecimalFormat("##.##").format(pricechk))));
                                            }
                                            if(Review.getTv_price()!=null){
                                                Review.getTv_price().setText(String.valueOf(Double.parseDouble(new DecimalFormat("##.##").format(pricechk))));


                                        }

                                    }


                                }


                                //changes today
                               // Global.paidmysubs = paidDto;
                               // Global.freemysubs = freeDto;
                               // Global.channelData = GlobalChannelData;
                            }else{
                                //do nothing...
                            }

                        }


                    }else{
                        //do nothing....
                    }
                }

            }
        });

        viewHolder.iv_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<HashMap<String, String>> arrData = new ArrayList<HashMap<String, String>>();
                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.popups_row);

                RecyclerView lv = (RecyclerView) dialog.findViewById(R.id.pre_recycler);
                LinearLayoutManager layoutManager = new LinearLayoutManager(context);
                lv.setLayoutManager(layoutManager);
                TextView tv_header = (TextView)dialog.findViewById(R.id.tv_header);
                tv_header.setText(Data.get(position).get("Channel"));
                ImageView iv_close = (ImageView)dialog.findViewById(R.id.iv_close);
                iv_close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                nDatamain = Global.DeletedBouquetData;
                for(int K =0;K<nDatamain.size();K++){
                    if(nDatamain.get(K).getBouquet_name().equalsIgnoreCase(Data.get(position).get("Channel"))) {
                        HashMap<String, String> bouquet_list = new HashMap<String, String>();
                        bouquet_list.put("Channel", (nDatamain.get(K).getChannel()));
                        bouquet_list.put("category", (nDatamain.get(K).getCategory()));
                        bouquet_list.put("language", (nDatamain.get(K).getLanguage()));
                        bouquet_list.put("price", (nDatamain.get(K).getPrice()));
                        bouquet_list.put("ImageUrl", (nDatamain.get(K).getImageurl()));
                        arrData.add(bouquet_list);
                    }}
                adapter = new PreRecycler(context, arrData);
                lv.setAdapter(adapter);
                dialog.setCancelable(true);
                dialog.show();
            }
        });

        /*viewHolder.img_drop_down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = userData.get(position).getBouquet();
                String broadcaster = userData.get(position).getBroadcaster();
                final ProgressDialog progressDialog = new ProgressDialog(context);
                progressDialog.setCancelable(false); // set cancelable to false
                progressDialog.setMessage("Please Wait"); // set message
                progressDialog.show();
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl("http://59.179.24.45:8085/api/bouquet/"+name+"/"+broadcaster+"/")
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                BouquetSubInterface service = retrofit.create(BouquetSubInterface.class);

                Call<List<BouquetSubDto>> call = service.getsubBouquetDetails("");

                call.enqueue(new Callback<List<BouquetSubDto>>() {
                    @Override
                    public void onResponse(Call<List<BouquetSubDto>> call, retrofit2.Response<List<BouquetSubDto>> response) {

                        try {

                            List<BouquetSubDto> bouquetSubDtos = response.body();

                            //Log.d("Data", bouquetSubDtos.get(20).getBroadcaster());
                            viewHolder.img_drop_down.setVisibility(View.GONE);
                            viewHolder.iv_arrow_Collapse.setVisibility(View.VISIBLE);
                            viewHolder.rec_bottom.setVisibility(View.VISIBLE);
                            progressDialog.dismiss();
                            userRecyclerView = new BouquetSubRecycler(context,bouquetSubDtos);
                            viewHolder.rec_bottom.setAdapter(userRecyclerView);


                        } catch (Exception e) {
                            Log.d("onResponse", "There is an error");
                            progressDialog.dismiss();
                            e.printStackTrace();
                        }



                    }

                    @Override
                    public void onFailure(Call<List<BouquetSubDto>> call, Throwable t) {
                        progressDialog.dismiss();

                        Log.d("onFailure", t.toString());

                    }


                });
            }
        });
*/
        /*viewHolder.iv_arrow_Collapse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                viewHolder.iv_arrow_Collapse.setVisibility(View.GONE);
                viewHolder.img_drop_down.setVisibility(View.VISIBLE);
                viewHolder.rec_bottom.setVisibility(View.GONE);
            }
        });*/


    }



    @Override
    public int getItemCount() {
        return Data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView iv_delete,iv_restore,iv_info;
        private TextView tv_channel_name,tv_channel,tv_price,tv_hd,bouqet_name,bouqet_price,tv_language,tv_category,bouqet_channel_name,tv_restore_text;
        private ImageButton img_drop_down;
        private RecyclerView rec_bottom;
        private ImageView iv_arrow_Collapse;

        public MyViewHolder(View view) {
            super(view);
            final int TAG = position;
            // get the reference of item view's
            tv_channel_name     = (TextView) view.findViewById(R.id.tv_channel_name);
            tv_price     = (TextView) view.findViewById(R.id.tv_price);
            rec_bottom    = (RecyclerView)view.findViewById(R.id.rec_bottom);
            iv_arrow_Collapse = (ImageView)view.findViewById(R.id.iv_arrow_Collapse);
            iv_delete = (ImageView)view.findViewById(R.id.iv_delete);
            iv_restore = (ImageView)view.findViewById(R.id.iv_restore);
            iv_info = (ImageView)view.findViewById(R.id.iv_info);
            tv_restore_text = (TextView)view.findViewById(R.id.tv_restore_text);
            LinearLayoutManager layoutManager = new LinearLayoutManager(context.getApplicationContext());
            rec_bottom.setLayoutManager(layoutManager);



        }
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }


}



