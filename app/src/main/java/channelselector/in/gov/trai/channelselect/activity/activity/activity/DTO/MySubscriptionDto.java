package channelselector.in.gov.trai.channelselect.activity.activity.activity.DTO;

public class MySubscriptionDto {
    public String Bouquet_Id;
    public String Broadcaster;
    public String Channel;
    public String category;
    public String language;
    public String HD;
    public String price;
    public String imageurl;
    public String bouquetuniquename;
    public String bouquet;
    public String bouquetname;
    public String bouquetprice;
    public String bouquetpriceperchannel;
    public String totalFreeChannels;
    public String totalPaidChannels;
    public String totalBouquet;
    public String totalsubsPrice;

    public String getBouquet_Id() {
        return Bouquet_Id;
    }

    public void setBouquet_Id(String bouquet_Id) {
        Bouquet_Id = bouquet_Id;
    }

    public String getDropped() {
        return dropped;
    }

    public void setDropped(String dropped) {
        this.dropped = dropped;
    }

    public String getAdded() {
        return added;
    }

    public void setAdded(String added) {
        this.added = added;
    }

    public String dropped;
    public String added;

    public String getChannelChanges() {
        return ChannelChanges;
    }

    public void setChannelChanges(String channelChanges) {
        ChannelChanges = channelChanges;
    }

    public String ChannelChanges;

    public String getTotalFreeChannels() {
        return totalFreeChannels;
    }

    public void setTotalFreeChannels(String totalFreeChannels) {
        this.totalFreeChannels = totalFreeChannels;
    }

    public String getTotalPaidChannels() {
        return totalPaidChannels;
    }

    public void setTotalPaidChannels(String totalPaidChannels) {
        this.totalPaidChannels = totalPaidChannels;
    }

    public String getTotalBouquet() {
        return totalBouquet;
    }

    public void setTotalBouquet(String totalBouquet) {
        this.totalBouquet = totalBouquet;
    }

    public String getTotalsubsPrice() {
        return totalsubsPrice;
    }

    public void setTotalsubsPrice(String totalsubsPrice) {
        this.totalsubsPrice = totalsubsPrice;
    }


    public String getBroadcaster() {
        return Broadcaster;
    }

    public void setBroadcaster(String broadcaster) {
        Broadcaster = broadcaster;
    }

    public String getChannel() {
        return Channel;
    }

    public void setChannel(String channel) {
        Channel = channel;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getHD() {
        return HD;
    }

    public void setHD(String HD) {
        this.HD = HD;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

    public String getBouquetuniquename() {
        return bouquetuniquename;
    }

    public void setBouquetuniquename(String bouquetuniquename) {
        this.bouquetuniquename = bouquetuniquename;
    }

    public String getBouquet() {
        return bouquet;
    }

    public void setBouquet(String bouquet) {
        this.bouquet = bouquet;
    }

    public String getBouquetname() {
        return bouquetname;
    }

    public void setBouquetname(String bouquetname) {
        this.bouquetname = bouquetname;
    }

    public String getBouquetprice() {
        return bouquetprice;
    }

    public void setBouquetprice(String bouquetprice) {
        this.bouquetprice = bouquetprice;
    }

    public String getBouquetpriceperchannel() {
        return bouquetpriceperchannel;
    }

    public void setBouquetpriceperchannel(String bouquetpriceperchannel) {
        this.bouquetpriceperchannel = bouquetpriceperchannel;
    }


}

