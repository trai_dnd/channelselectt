package channelselector.in.gov.trai.channelselect.activity.activity.activity.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import channelselector.in.gov.trai.channelselect.R;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.DTO.FreeChannelDto;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.DTO.PaidChannelDto;


public class PreRecycler extends RecyclerView.Adapter<PreRecycler.MyViewHolder> {

    private Context context;

    private MyViewHolder viewHolder;
    private int position;
    int pos;
    List<String> parent_data;
    ArrayList<HashMap<String,String>> freeData;
    ArrayList<HashMap<String,String>> paidChannel;
    ArrayList<String> bouquetName;
    ArrayList<FreeChannelDto> freeDto;
    ArrayList<PaidChannelDto> paidDto;
    private RecyclerViewSubSubription subAdapter;
    ArrayList<HashMap<String,String>> mData;
    private BouquetChildAdapter adapter;



    public PreRecycler(Context context, ArrayList<HashMap<String,String>> arrData) {
        this.context = context;
        this.mData = arrData;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
// infalte the item Layout
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_user, parent, false);
// set the view's size, margins, paddings and layout parameters
        MyViewHolder viewHolder = new MyViewHolder(v); // pass the view to View Holder


        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder viewHolder, final int position) {
        //Log.e("DATATEST=>",userData.get(position).getBroadcaster());
        this.viewHolder = viewHolder;
        this.position = position;
        viewHolder.tv_delete.setVisibility(View.GONE);
        viewHolder.tv_channel.setFilters(new InputFilter[]{ new InputFilter.LengthFilter(15) });
        viewHolder.tv_channel.setText(mData.get(position).get("Channel"));
        viewHolder.tv_language.setText(mData.get(position).get("language"));
        viewHolder.tv_price.setText(mData.get(position).get("price"));
        String path = mData.get(position).get("ImageUrl");
        viewHolder.remove_channel.setVisibility(View.GONE);
        Glide.with(context)
                .asBitmap()
                .load(path)
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                        viewHolder.iv_channels.setImageBitmap(resource);
                    }
                });





    }



    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView iv_channels,remove_channel;
        private TextView tv_channel,tv_price,tv_language,tv_delete;
        private ImageButton img_drop_down,iv_arrow_Collapse;
        private RecyclerView rec_subscriber,rec_bouquet_Data_recycler,rec_paid_Data_recycler,rec_free_Data_recycler;

        public MyViewHolder(View view) {
            super(view);
            int TAG = position;
            // get the reference of item view's
            iv_channels = (ImageView)view.findViewById(R.id.iv_channels);
            tv_channel     = (TextView) view.findViewById(R.id.tv_channel);
            tv_price    = (TextView) view.findViewById(R.id.tv_price);
            tv_language    = (TextView) view.findViewById(R.id.tv_language);
            remove_channel = (ImageView)view.findViewById(R.id.remove_channel);
            tv_delete = (TextView)view.findViewById(R.id.tv_delete);

            /*rec_bouquet_Data_recycler.setLayoutManager(layoutManager);
            rec_paid_Data_recycler.setLayoutManager(layoutManager);*/

        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }




}



