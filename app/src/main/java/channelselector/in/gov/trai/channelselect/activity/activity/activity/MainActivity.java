package channelselector.in.gov.trai.channelselect.activity.activity.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import channelselector.in.gov.trai.channelselect.R;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.DTO.BouquetDto;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.DTO.BouquetSubDto;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.DTO.ChannelDto;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.DTO.MySubBouquet;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.DTO.MySubsChild;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.DTO.MySubsFree;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.DTO.MySubsPiad;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.Extras.Const;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.Extras.Global;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.adapters.BouquetRecycler;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.classes.ChannelsBouquetActivity;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.classes.MySubscription;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.classes.PastRequest;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.classes.Review;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.webservicehandler.GetWebService;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.webservicehandler.HeaderGet;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private DrawerLayout drawer;
    private RelativeLayout mysubsc;
    private RelativeLayout channels;
    private RelativeLayout rel_exit;
    private RelativeLayout see_subscription;
    private TextView tv_subs_value,tv_mobile,tv_mobile_head,tv_subscriber_id_head;
    String subsid,mobile;
    private TextView tv_free_channels,tv_paid_channels,tv_bouquet,tv_price;
    private LinearLayout linear_sub_text,linear_sub_value;
    String customerId,Url;
    List<String> listDataHeader;
    int n =0;
    int strnum = 0;
    double d =0;
    double Total_price,MainPrice;
    private RelativeLayout rel_mainlayout;
    double piad;
    double bouquets;
    HashMap<String,String> arr_changes = new HashMap<String,String>();
    int Flag =0;
    String operator;
    private ImageView img_amage,img_user;
    private List<BouquetDto> bouquetDtos = new ArrayList<BouquetDto>();
    String subscriptionid, accessToken;
    private List<MySubBouquet> bouquetDtoss = new ArrayList<MySubBouquet>();
    private List<MySubsChild> bouquetSubDtos = new ArrayList<MySubsChild>();
    private List<BouquetSubDto> bouquetMainDto = new ArrayList<BouquetSubDto>();
    ArrayList<MySubsPiad> paidDto = new ArrayList<MySubsPiad>();
    ArrayList<MySubsFree> freeDto = new ArrayList<MySubsFree>();
    private List<BouquetSubDto> subbouquet = new ArrayList<BouquetSubDto>();
    private List<BouquetDto> gbouquetDtos = new ArrayList<BouquetDto>();
    HashMap<String, ArrayList<ChannelDto>> listDataChild;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        subscriptionid = Prefs.getString(Const.SUBSCRIPTION_ID, "id");
        accessToken = Prefs.getString(Const.ACCESSTOKEN, "token");
        operator    = Prefs.getString(Const.OPERATORNAME, "operator");
        subbouquet = Global.bouquetSubDtos;
        gbouquetDtos = Global.selectedBouquet;
        view();
        events();
        getbouquet();
        getsubscription();
        setvalues();
    }

    private void getsubscription() {
        String url;
        if(operator.equalsIgnoreCase("Tata Sky")){

            url = Const.getSubscriptionTata + subscriptionid;
        }else{
            url = Const.getSubscription + subscriptionid;
        }

        if(Global.FlagMainActivity==1){
            Flag = Global.FlagMainActivity;
        }else{
            //rel_mainlayout.setVisibility(View.VISIBLE);
        }
        if(Flag==0) {
            if (Const.isNetworkAvailable(MainActivity.this)) {

                GetSubscription o = new GetSubscription(url, accessToken);
                o.execute();
            } else {
               // Const.messageViewRe(mainLay, getString(R.string.network));
            }
            Flag =1;
            Global.FlagMainActivity = Flag;
        }else{
            rel_mainlayout.setVisibility(View.VISIBLE);
        }
    }

    private class GetSubscription extends AsyncTask<String, Void, String> {
        String accessToken;
        String url;
        String authToken;
        String Flag;
        String Total_Channel;
        String Total_Bouquet;
        String Amount;
        String AvailableBalance;
        String channel_id;
        String Broadcaster;
        String Channel ;
        String category;
        String language;
        String HD;
        String ImageUrl;

        GetSubscription(String url, String accessToken) {
            this.url = url;
            this.accessToken = accessToken;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.e("URL=>", "" + url);
        }

        @Override
        protected String doInBackground(String... strings) {

            HeaderGet o = new HeaderGet();
            String res = null;
            try {
                res = o.getData(url,accessToken);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return res;
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.e("RES=>", "" + s);
            try {
                String result = s;
                JSONObject obj = new JSONObject(result);
                String status = obj.getString("status");
                if (status.equalsIgnoreCase("200")) {
                    listDataHeader = new ArrayList<String>();
                    Total_Channel = obj.getString("total_channel");
                    Total_Bouquet = obj.getString("total_bouquet");
                    Amount = obj.getString("amount");
                    Prefs.putString(Const.Total_Channel, Total_Channel);
                    Prefs.putString(Const.Total_Bouquet, Total_Bouquet);
                    Prefs.putString(Const.Amount, Amount);
                    Global.TotalSubscriptionPrice = Double.parseDouble(Amount);
                    JSONArray jsonArray = obj.getJSONArray("bouquet");
                    JSONArray jsonArrayChannel = obj.getJSONArray("channels");
                    for (int i = 0; i < jsonArray.length(); i++) {

                        JSONObject object = jsonArray.getJSONObject(i);
                        String bouquet_id = object.getString("bouquet_id");
                        String bouquet = object.getString("bouquet");
                        String broadcaster = object.getString("broadcaster");
                        String bouquetPrice = object.getString("bouquet_price");
                        String totalchannel = object.getString("total_channel");

                        MySubBouquet bk = new MySubBouquet();
                        bk.setBouquet_id(bouquet_id);
                        bk.setBouquet(bouquet);
                        bk.setBroadcaster(broadcaster);
                        bk.setBouquetprice(bouquetPrice);
                        bk.setTotalchannel(totalchannel);
                        bk.setSelected(false);
                        bouquetDtoss.add(bk);

                        if(object.has("bouquetchannel")){

                            JSONArray jsonArraymember = object.getJSONArray("bouquetchannel");
                            if(jsonArraymember.length()>0) {

                                for (int j = 0; j < jsonArraymember.length(); j++) {

                                    JSONObject products = jsonArraymember.getJSONObject(j);
                                    String channel_id = products.getString("channel_id");
                                    String broadcasterch = products.getString("broadcaster");
                                    String channel = products.getString("channel");
                                    String category = products.getString("category");
                                    String language = products.getString("language");
                                    String price = products.getString("price");
                                    String imageurl = products.getString("imageurl");
                                    String hd = products.getString("hd");
                                    if(imageurl.contains("http")){
                                        //do nothing...
                                    }else{
                                        imageurl = "http://59.179.24.45:8081"+"/"+products.getString("imageurl");
                                    }

                                    MySubsChild prodto = new MySubsChild();
                                    prodto.setChannel_id(channel_id);
                                    prodto.setBroadcaster(broadcasterch);
                                    prodto.setChannel(channel);
                                    prodto.setCategory(category);
                                    prodto.setLanguage(language);
                                    prodto.setPrice(price);
                                    prodto.setImageurl(imageurl);
                                    prodto.setHd(hd);
                                    prodto.setBouquet_id(bouquet_id);
                                    prodto.setBouquet_name(bouquet);
                                    prodto.setBouquet_price(bouquetPrice);
                                    bouquetSubDtos.add(prodto);

                                }
                            }else{

                                //do nothing...
                            }

                        }
                    }// end bouquet task start channel part.......................//

                    for(int i =0 ; i<jsonArrayChannel.length();i++){
                        JSONObject objchannel = jsonArrayChannel.getJSONObject(i);
                        String price   = objchannel.getString("price");

                        if(price.equalsIgnoreCase("0.0")){
                            channel_id  = objchannel.getString("channel_id");
                            Broadcaster = objchannel.getString("broadcaster");
                            Channel = objchannel.getString("channel");
                            category        = objchannel.getString("category");
                            language  = objchannel.getString("language");
                            price   = objchannel.getString("price");
                            ImageUrl   = objchannel.getString("imageurl");
                            HD        = objchannel.getString("hd");
                            if(ImageUrl.contains("http")){
                                //do nothing...
                            }else{
                                ImageUrl = "http://59.179.24.45:8081"+"/"+objchannel.getString("imageurl");
                            }

                            MySubsFree freeData = new MySubsFree();
                            freeData.setChannel_id(channel_id);
                            freeData.setBroadcaster(Broadcaster);
                            freeData.setChannel(Channel);
                            freeData.setCategory(category);
                            freeData.setLanguage(language);
                            freeData.setPrice(price);
                            freeData.setImageurl(ImageUrl);
                            freeData.setHd(HD);
                            freeData.setSelected(false);
                            freeDto.add(freeData);


                        }else{
                            channel_id  = objchannel.getString("channel_id");
                            Broadcaster = objchannel.getString("broadcaster");
                            Channel = objchannel.getString("channel");
                            category        = objchannel.getString("category");
                            language  = objchannel.getString("language");
                            price   = objchannel.getString("price");
                            ImageUrl   = objchannel.getString("imageurl");
                            HD        = objchannel.getString("hd");
                            if(ImageUrl.contains("http")){
                                //do nothing...
                            }else{
                                ImageUrl = "http://59.179.24.45:8081"+"/"+objchannel.getString("imageurl");
                            }


                            MySubsPiad paidDtos = new MySubsPiad();
                            paidDtos.setChannel_id(channel_id);
                            paidDtos.setBroadcaster(Broadcaster);
                            paidDtos.setChannel(Channel);
                            paidDtos.setCategory(category);
                            paidDtos.setLanguage(language);
                            paidDtos.setHd(HD);
                            paidDtos.setPrice(price);
                            paidDtos.setImageurl(ImageUrl);
                            paidDtos.setSelected(false);
                            paidDto.add(paidDtos);

                        }

                    }

                    listDataHeader.add("Free Channel");
                    listDataHeader.add("Paid Channel");
                    listDataHeader.add("Bouquet");
                    Global.freemysubs = freeDto;
                    Global.Freechannels = freeDto.size();
                    Global.mySubscriptionFreeData=freeDto;
                    Global.paidmysubs = paidDto;
                    Global.SubscriptionPaidData=paidDto;
                    Global.PaidChannels = paidDto.size();
                    Global.BouquetsubTotal= bouquetSubDtos;
                    // Global.arrboquet = bouquetDtos.size();
                    Global.bouquetData = bouquetDtoss;
                    Global.TotalBouquet= bouquetDtoss.size();
                    Global.listheader = listDataHeader;
                    int channel_count = paidDto.size()+25;
                    channel_count = channel_count+freeDto.size();
                    channel_count = channel_count+bouquetSubDtos.size();
                    Global.channelCount = channel_count;
                    Log.d("channelcount", String.valueOf(channel_count));


                    getprice();



                }else{
                    String message = obj.getString("message");

                    //Const.messageViewRe(mainLay, message);
                }
            }catch (Exception e) {

            }
        }
    }

    private void getprice() {

        if(paidDto!=null && paidDto.size()>0) {
            for (int i = 0; i < paidDto.size(); i++) {
                String price_paid = paidDto.get(i).getPrice();
                double s = Double.parseDouble(price_paid);
                piad = piad + s;

            }
            Global.paidchannelPrice = piad;
        }
        if(bouquetDtoss!=null && bouquetDtoss.size()>0){
            for(int i =0;i<bouquetDtoss.size();i++){
                String bouquet_price = bouquetDtoss.get(i).getBouquetPrice();
                double s = Double.parseDouble(bouquet_price);
                bouquets = bouquets+s;
            }
            Global.bouquetprice = bouquets;
        }

        Double Total_price = Global.paidchannelPrice + Global.bouquetprice + 130;
        Double interest = Total_price*18/100;
        Double sub_price = Total_price+ interest;


        Global.TotalSubscriptionPrice = Double.parseDouble(new DecimalFormat("##.##").format(sub_price));
    }

    private void setvalues() {

        subsid = Global.CustomerID;
        if(subsid!=null){
            tv_subs_value.setText(subsid);
            tv_subscriber_id_head.setText("SubscriberId"+":"+" "+subsid );
        }if(mobile!=null){
            tv_mobile.setText("+91-xxxxxxxxxx");
            tv_mobile_head.setText("+91-xxxxxxxxxx");
        }
        if(operator!=null && operator.equalsIgnoreCase("Tata Sky")){
            //  img_amage.setImageResource(R.drawable.tataskyn);
            Glide.with(MainActivity.this)
                    .load(R.drawable.tata_sky)
                    .apply(RequestOptions.circleCropTransform())
                    .into(img_amage);
            Glide.with(MainActivity.this)
                    .load(R.drawable.tata_sky)
                    .apply(RequestOptions.circleCropTransform())
                    .into(img_user);
        }else if(operator!=null && operator.equalsIgnoreCase("Airtel Digital TV")){
            Glide.with(MainActivity.this)
                    .load(R.drawable.airtel_tv)
                    .apply(RequestOptions.circleCropTransform())
                    .into(img_amage);
            Glide.with(MainActivity.this)
                    .load(R.drawable.airtel_tv)
                    .apply(RequestOptions.circleCropTransform())
                    .into(img_user);
            // img_amage.setImageResource(R.drawable.airteltv);
        }
    }

    private void getbouquet() {
        subbouquet = Global.bouquetSubDtos;
        gbouquetDtos = Global.FreshBouquet;
        if(subbouquet!=null && gbouquetDtos!=null){
            if(subbouquet.size()>0 && gbouquetDtos.size()>0){
                // do nothing...
            }else{
                new GetBouquetTask().execute();
            }
        }else{
            new GetBouquetTask().execute();
        }

    }
    public class GetBouquetTask extends AsyncTask<String, Void, String> {

        String response;
        String url;


        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();

            //dialogWait = new DialogWait(getActivity());
            // dialogWait.show();

        }

        @Override
        protected String doInBackground(String... params) {

            if(operator.equalsIgnoreCase("Tata Sky")){

                url = Const.getBouquetTaata;
            }else{

                url = Const.getBouquet;
            }



            HeaderGet o = new HeaderGet();
            String res = null;
            try {
                res = o.getData(url,accessToken);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return res;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);

            //dialogWait.dismiss();


            if (null != result) {


                try {
                    JSONObject obj = new JSONObject(result);
                    String status = obj.getString("status");
                    if (status.equalsIgnoreCase("200")) {
                        JSONArray jsonArray = obj.getJSONArray("bouquet");
                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject object = jsonArray.getJSONObject(i);
                            String bouquet_id = object.getString("bouquet_id");
                            String bouquet = object.getString("bouquet");
                            String broadcaster = object.getString("broadcaster");
                            String bouquetPrice = object.getString("bouquet_price");
                            String totalchannel = object.getString("total_channel");

                            BouquetDto bk = new BouquetDto();
                            bk.setBouquet_id(bouquet_id);
                            bk.setBouquet(bouquet);
                            bk.setBroadcaster(broadcaster);
                            bk.setBouquetprice(bouquetPrice);
                            bk.setTotalchannel(totalchannel);
                            bk.setSelected(false);
                            bouquetDtos.add(bk);

                            if(object.has("bouquetchannel")){

                                JSONArray jsonArraymember = object.getJSONArray("bouquetchannel");
                                if(jsonArraymember.length()>0) {

                                    for (int j = 0; j < jsonArraymember.length(); j++) {

                                        JSONObject products = jsonArraymember.getJSONObject(j);
                                        String channel_id = products.getString("channel_id");
                                        String broadcasterch = products.getString("broadcaster");
                                        String channel = products.getString("channel");
                                        String category = products.getString("category");
                                        String language = products.getString("language");
                                        String price = products.getString("price");
                                        String imageurl = products.getString("imageurl");
                                        String hd = products.getString("hd");
                                        if(imageurl.contains("http")){
                                            //do nothing...
                                        }else{
                                            imageurl = "http://59.179.24.45:8081"+"/"+products.getString("imageurl");
                                        }

                                        BouquetSubDto prodto = new BouquetSubDto();
                                        prodto.setChannel_id(channel_id);
                                        prodto.setBroadcaster(broadcasterch);
                                        prodto.setChannel(channel);
                                        prodto.setCategory(category);
                                        prodto.setLanguage(language);
                                        prodto.setPrice(price);
                                        prodto.setImageurl(imageurl);
                                        prodto.setHd(hd);
                                        prodto.setBouquet_id(bouquet_id);
                                        bouquetMainDto.add(prodto);

                                    }
                                }else{

                                    //do nothing...
                                }

                            }

                        }

                        Global.CheckBouquetPriceDto = bouquetDtos;


                    }else{
                        String message = obj.getString("message");

                    }
                }catch (Exception e) {

                }

            }

            //dialogWait.dismiss();
        }
    }

    private void events() {

        mysubsc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*startActivity(new Intent(MainActivity.this, MySubscription.class));*/
                Intent i = new Intent(MainActivity.this,MySubscription.class);
                i.putExtra("MainActivityFlagValidate","MainActivityFlagValidate");
                startActivity(i);
            }
        });
        channels.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,ChannelsBouquetActivity.class));
            }
        });
        rel_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                moveTaskToBack(true);
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(1);
            }
        });
        see_subscription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(MainActivity.this,Review.class));
            }
        });
    }

    private void view() {

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.openDrawer, R.string.closeDrawer);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View headerView = navigationView.getHeaderView(0);
        mysubsc = (RelativeLayout)findViewById(R.id.mysubsc);
        channels = (RelativeLayout)findViewById(R.id.channels);
        rel_exit = (RelativeLayout)findViewById(R.id.rel_exit);
        see_subscription = (RelativeLayout)findViewById(R.id.see_subscription);
        tv_mobile = (TextView)findViewById(R.id.tv_mobile);
        tv_subs_value = (TextView)findViewById(R.id.tv_subs_value);
        tv_price    = (TextView)findViewById(R.id.tv_price);
        tv_free_channels = (TextView)findViewById(R.id.tv_free_channels);
        tv_paid_channels = (TextView)findViewById(R.id.tv_paid_channels);
        tv_bouquet = (TextView)findViewById(R.id.tv_bouquet);
        linear_sub_value = (LinearLayout)findViewById(R.id.linear_sub_value);
        linear_sub_text  = (LinearLayout)findViewById(R.id.linear_sub_text);
        rel_mainlayout = (RelativeLayout)findViewById(R.id.rel_mainlayout);
        tv_subscriber_id_head = (TextView)headerView.findViewById(R.id.tv_subscriber_id_head);
        tv_mobile_head = (TextView)headerView.findViewById(R.id.tv_mobile_head);
        img_amage = (ImageView)findViewById(R.id.img_amage);
        img_user = (ImageView)headerView.findViewById(R.id.img_user);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        drawer.closeDrawer(GravityCompat.START);
        //return true; // Use back to return to previous
        int id = menuItem.getItemId();
        if (id == R.id.nav_changes) {
            //startActivity(new Intent(MainActivity.this,ChangesActivity.class));
        }if(id == R.id.nav_mysubscription){

            Intent i = new Intent(MainActivity.this,MySubscription.class);
            i.putExtra("MainActivityFlagValidate","MainActivityFlagValidate");
            startActivity(i);

        }if(id == R.id.nav_Channel){
            startActivity(new Intent(MainActivity.this,ChannelsBouquetActivity.class));
        }if(id == R.id.nav_review){
            startActivity(new Intent(MainActivity.this,Review.class));
        }if(id == R.id.nav_changes){
            startActivity(new Intent(MainActivity.this,PastRequest.class));
        }if(id == R.id.nav_exit){
            moveTaskToBack(true);
            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(1);
        }
        return true;
    }
}
