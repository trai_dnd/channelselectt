package channelselector.in.gov.trai.channelselect.activity.activity.activity.adapters;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import channelselector.in.gov.trai.channelselect.R;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.DTO.BouquetDto;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.DTO.ChannelDto;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.DTO.MySubscriptionDto;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.Extras.Const;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.Extras.Global;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.classes.Review;


public class ChildPreviewRecycler extends RecyclerView.Adapter<ChildPreviewRecycler.MyViewHolder> {

    private Context context;
    private List<BouquetDto> userData;
    private MyViewHolder viewHolder;
    private int position;
    int pos;
    private BouquetSubRecycler userRecyclerView;
    ArrayList<HashMap<String, String>> Data;
    HashMap<String,ArrayList<ChannelDto>> GlobalChannelData;
    String Flag,BouquetFlag;
    ArrayList<HashMap<String, String>> nData = new ArrayList<HashMap<String, String>>();
    ArrayList<MySubscriptionDto>nDatamain = new ArrayList<MySubscriptionDto>();
    private PreRecycler adapter;
    int free,paid,bouquet;
    private ArrayList<HashMap<String,String>> confirmAddedData;


    public ChildPreviewRecycler(Context context, ArrayList<HashMap<String, String>> senderList) {
        this.context=context;
        this.Data=senderList;
        this.GlobalChannelData = Global.channelData;
        this.confirmAddedData = Global.confirmAddedData;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
// infalte the item Layout
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_child_channel, parent, false);
// set the view's size, margins, paddings and layout parameters
        MyViewHolder viewHolder = new MyViewHolder(v); // pass the view to View Holder


        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder viewHolder, final int position) {
        //Log.e("DATATEST=>",userData.get(position).getBroadcaster());
        this.viewHolder = viewHolder;
        this.position = position;
        String Bouquestatus = Data.get(position).get("isBouqet");
        if( Bouquestatus!=null&& Bouquestatus.equalsIgnoreCase("Yes")){
            viewHolder.iv_info.setVisibility(View.VISIBLE);
            //viewHolder.iv_delete.setVisibility(View.INVISIBLE);
        }else{
            viewHolder.tv_channel_name.setFilters(new InputFilter[]{ new InputFilter.LengthFilter(25) });
            viewHolder.tv_channel_name.setText(Data.get(position).get("Channel"));
            viewHolder.iv_info.setVisibility(View.GONE);
            viewHolder.iv_delete.setVisibility(View.VISIBLE);
        }
        viewHolder.tv_channel_name.setFilters(new InputFilter[]{ new InputFilter.LengthFilter(25) });
        viewHolder.tv_channel_name.setText(Data.get(position).get("Channel"));
        viewHolder.tv_price.setText(context.getResources().getString(R.string.Rs)+" "+Data.get(position).get("price"));

        viewHolder.iv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String name = (Data.get(position).get("Channel"));
                for(int i =0;i<Data.size();i++){
                    String arraychannelname = Data.get(i).get("Channel");
                    String channel_id  = Data.get(i).get("Channel_Id");
                    String price = Data.get(i).get("price");
                    String Bouquestatus = Data.get(i).get("isBouqet");
                    String Bouquet_id = Data.get(i).get("Bouquet_Id");
                    if(arraychannelname.equalsIgnoreCase(name)){
                        Data.remove(i);
                        //Global.confirmAddedData = Data;
                        notifyItemRemoved(position);
                        notifyItemRangeChanged(position, Data.size());
                        notifyItemRemoved(position);
                        notifyItemRangeChanged(position, Data.size());
                        Global.channelData = GlobalChannelData;
                        if( Bouquestatus!=null){

                            if(Bouquestatus.equalsIgnoreCase("Yes")){
                                Global.confirmAddedData = Data;
                                double s = Double.parseDouble(price);
                                if(Global.confirmAddedBouquetDataHash!=null){
                                    for(int p =Global.confirmAddedBouquetDataHash.size()-1; p>0; p--){
                                        if(Global.confirmAddedBouquetDataHash.get(p).get("BouquetName").equalsIgnoreCase(arraychannelname)){
                                            Global.confirmAddedBouquetDataHash.remove(p);
                                        }
                                    }
                                    if(Global.confirmAddedBouquetDataHash!=null && Global.confirmAddedBouquetDataHash.size()>0){
                                        for(int m = 0; m<Global.confirmAddedBouquetDataHash.size();m++){
                                            if(Global.confirmAddedBouquetDataHash.get(m).get("BouquetName").equalsIgnoreCase(arraychannelname)){
                                                Global.confirmAddedBouquetDataHash.remove(m);
                                            }
                                        }
                                    }
                                }
                            if(Global.confirmAddedBouquetData!=null){
                                for(int j =0 ; j<Global.confirmAddedBouquetData.size();j++){
                                    if(Global.confirmAddedBouquetData.get(j).get("Channel").equalsIgnoreCase(arraychannelname)){
                                        int ChannelBouquetCount = Integer.parseInt(Global.confirmAddedBouquetData.get(j).get("channel_count"));
                                        Global.confirmAddedBouquetData.remove(j);
                                        int count = Global.channelCount-ChannelBouquetCount;
                                        Global.channelCount = count;
                                        Log.d("channel_count", String.valueOf(Global.channelCount));
                                    }
                                }
                            }if(Global.selectedBouquet!=null){
                                for(int m =0;m<Global.selectedBouquet.size();m++){
                                    if(Global.selectedBouquet.get(m).getBouquet().equalsIgnoreCase(arraychannelname)){
                                        Global.selectedBouquet.get(m).setSelected(false);
                                    }else{
                                        //do nothing...
                                    }
                                }
                            }
                            if(Global.channelCount<100){
                                Global.bouquetprice = Global.bouquetprice-s;
                                double Totalchannelbouquet = Global.paidchannelPrice+ Global.bouquetprice;
                                double Final = 130+Totalchannelbouquet;
                                double gt = Final*18/100;
                                double pricetotal = Final+gt;
                                Global.TotalSubscriptionPrice = Double.parseDouble(new DecimalFormat("##.##").format(pricetotal));
                                if(Review.getTv_price()!=null){
                                    Review.getTv_price().setText(String.valueOf(Double.parseDouble(new DecimalFormat("##.##").format(pricetotal))));
                                }
                                bouquet = Global.TotalBouquet;
                                String set_Bouquet = String.valueOf(bouquet-1);
                                Review.getBouquetText().setText(String.valueOf(set_Bouquet));
                                Global.TotalBouquet = Integer.parseInt(set_Bouquet);
                            }else {
                                Global.bouquetprice = Global.bouquetprice - s;
                                double gst = s * 18 / 100;
                                double sn = s + gst;
                                double n = Global.channelCount;
                                double setPricetotal = Const.getuniquechannelcount(Global.channelCount);
                                   /* double k = 20.0;
                                    double gt = k*18/100;
                                    double lgt = k+gt;
                                    double l = lgt+sn;

                                        double setPricetotal = Global.TotalSubscriptionPrice-l;*/
                                Global.TotalSubscriptionPrice = Double.parseDouble(new DecimalFormat("##.##").format(setPricetotal));
                                if (Review.getTv_price() != null) {
                                    Review.getTv_price().setText(String.valueOf(Double.parseDouble(new DecimalFormat("##.##").format(setPricetotal))));
                                }
                                bouquet = Global.TotalBouquet;
                                String set_Bouquet = String.valueOf(bouquet - 1);
                                Review.getBouquetText().setText(String.valueOf(set_Bouquet));
                                Global.TotalBouquet = Integer.parseInt(set_Bouquet);

                            }


                            }

                            BouquetFlag = "Remove Added Bouquet";
                            Global.addtionalBouquetFlag = BouquetFlag;
                            //do nothing.....
                        }else {
                            ArrayList<ChannelDto> datak = new ArrayList<ChannelDto>();
                            ArrayList<ChannelDto> dataD = new ArrayList<ChannelDto>();
                            datak = GlobalChannelData.get("Paid Channel");
                            dataD = GlobalChannelData.get("Free Channel");
                            if (price.equalsIgnoreCase("0.0")) {
                                for (int n = 0; n < dataD.size(); n++) {
                                    if (dataD.get(n).getChannel().equalsIgnoreCase(name)) {
                                        dataD.get(n).setSelected(false);
                                    }
                                }
                                free = Global.Freechannels;
                                String set_free = String.valueOf(free - 1);
                                Review.getFreeText().setText(set_free + "+" + 25);
                                Global.Freechannels = Integer.parseInt(set_free);
                                int count = Global.channelCount - 1;
                                Global.channelCount = count;
                                Log.d("channel_count", String.valueOf(Global.channelCount));
                            } else {
                                for (int n = 0; n < datak.size(); n++) {
                                    if (datak.get(n).getChannel().equalsIgnoreCase(name)) {
                                        datak.get(n).setSelected(false);
                                        if (datak.get(n).getHd().equalsIgnoreCase("HD")) {
                                            int count = Global.channelCount - 2;
                                            Global.channelCount = count;
                                            Log.d("channel_count", String.valueOf(Global.channelCount));
                                        } else {
                                            int count = Global.channelCount - 1;
                                            Global.channelCount = count;
                                            Log.d("channel_count", String.valueOf(Global.channelCount));
                                        }
                                    }
                                }
                                paid = Global.PaidChannels;
                                String set_paid = String.valueOf(paid - 1);
                                Review.getPaidText().setText(set_paid);
                                Global.PaidChannels = Integer.parseInt(set_paid);

                                double s = Double.parseDouble(price);
                                Global.confirmAddedData = Data;
                                if (Global.channelCount < 100) {
                                    Global.paidchannelPrice = Global.paidchannelPrice - s;
                                    double Totalchannelbouquet = Global.paidchannelPrice + Global.bouquetprice;
                                    double Final = 130 + Totalchannelbouquet;
                                    double gt = Final * 18 / 100;
                                    double pricetotal = Final + gt;
                                    Global.TotalSubscriptionPrice = Double.parseDouble(new DecimalFormat("##.##").format(pricetotal));
                                    if (Review.getTv_price() != null) {
                                        Review.getTv_price().setText(String.valueOf(Double.parseDouble(new DecimalFormat("##.##").format(pricetotal))));
                                    }
                                    //Data.remove(i);
                                    /*Global.confirmAddedData = Data;*/
                                } else {
                                    Global.paidchannelPrice = Global.paidchannelPrice - s;
                                    double z = Global.channelCount;
                                    double setPricetotal = Const.getuniquechannelcount(Global.channelCount);
                                                /*double k = 20.0;
                                                double gt = k*18/100;
                                                double lgt = k+gt;
                                                double gst = s*18/100;
                                                double sn = s+gst;
                                                double l =lgt+sn;
                                                    double setPricetotal = Global.TotalSubscriptionPrice-l;*/
                                    Global.TotalSubscriptionPrice = Double.parseDouble(new DecimalFormat("##.##").format(setPricetotal));
                                    if (Review.getTv_price() != null) {
                                        Review.getTv_price().setText(String.valueOf(Double.parseDouble(new DecimalFormat("##.##").format(setPricetotal))));
                                    }
                                    //Data.remove(i);
                                   // Global.confirmAddedData = Data;
                                    //Global.confirmAddedData.remove(n);
                                }


                                /*if (Global.confirmAddedData != null) {
                                    if (Global.confirmAddedData.size() > 0) {
                                        for (int n = 0; n < Global.confirmAddedData.size(); n++) {
                                            if (Global.confirmAddedData.get(n).get("Channel_Id").equalsIgnoreCase(channel_id)) {
                                                String pricepaid = Global.confirmAddedData.get(n).get("price");
                                                double s = Double.parseDouble(pricepaid);
                                                if (Global.channelCount < 100) {
                                                    Global.paidchannelPrice = Global.paidchannelPrice - s;
                                                    double Totalchannelbouquet = Global.paidchannelPrice + Global.bouquetprice;
                                                    double Final = 130 + Totalchannelbouquet;
                                                    double gt = Final * 18 / 100;
                                                    double pricetotal = Final + gt;
                                                    Global.TotalSubscriptionPrice = Double.parseDouble(new DecimalFormat("##.##").format(pricetotal));
                                                    if (Review.getTv_price() != null) {
                                                        Review.getTv_price().setText(String.valueOf(Double.parseDouble(new DecimalFormat("##.##").format(pricetotal))));
                                                    }
                                                    Global.confirmAddedData.remove(n);
                                                } else {
                                                    Global.paidchannelPrice = Global.paidchannelPrice - s;
                                                    double z = Global.channelCount;
                                                    double setPricetotal = Const.getuniquechannelcount(Global.channelCount);
                                                *//*double k = 20.0;
                                                double gt = k*18/100;
                                                double lgt = k+gt;
                                                double gst = s*18/100;
                                                double sn = s+gst;
                                                double l =lgt+sn;
                                                    double setPricetotal = Global.TotalSubscriptionPrice-l;*//*
                                                    Global.TotalSubscriptionPrice = Double.parseDouble(new DecimalFormat("##.##").format(setPricetotal));
                                                    if (Review.getTv_price() != null) {
                                                        Review.getTv_price().setText(String.valueOf(Double.parseDouble(new DecimalFormat("##.##").format(setPricetotal))));
                                                    }
                                                    Global.confirmAddedData.remove(n);


                                                }
                                            }
                                        }
                                    } else {

                                    }
                                }*/

                                Flag = "Remove Added Channel";
                                Global.addtionPartFlag = Flag;
                                GlobalChannelData.put("Paid Channel", datak);
                                GlobalChannelData.put("Free Channel", dataD);
                                Global.channelData = GlobalChannelData;
                            }
                        }
                    }else{
                        //do nothing....
                    }
                }

            }
        });

        viewHolder.iv_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ArrayList<HashMap<String, String>> arrData = new ArrayList<HashMap<String, String>>();
                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.popups_row);

                RecyclerView lv = (RecyclerView) dialog.findViewById(R.id.pre_recycler);
                LinearLayoutManager layoutManager = new LinearLayoutManager(context);
                lv.setLayoutManager(layoutManager);
                TextView tv_header = (TextView)dialog.findViewById(R.id.tv_header);
                tv_header.setText(Data.get(position).get("Channel"));
                ImageView iv_close = (ImageView)dialog.findViewById(R.id.iv_close);
                iv_close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                nData = Global.confirmAddedBouquetDataHash;
                if (nData != null && nData.size() > 0) {
                    for (int i = 0; i < nData.size(); i++) {
                        if (nData.get(i).get("BouquetName").equalsIgnoreCase(Data.get(position).get("Channel"))) {
                            HashMap<String, String> pData = new HashMap<String, String>();
                            String channelName = nData.get(i).get("Channel");
                            String category = nData.get(i).get("category");
                            String language = nData.get(i).get("language");
                            String price = nData.get(i).get("price");
                            String ImageUrl = nData.get(i).get("ImageUrl");
                            pData.put("Channel", channelName);
                            pData.put("category", category);
                            if (language != null) {
                                pData.put("language", language);
                            }

                            pData.put("price", price);
                            pData.put("ImageUrl", ImageUrl);
                            arrData.add(pData);
                        } else {
                            //do nothing...
                        }

                    }
                    adapter = new PreRecycler(context, arrData);
                    lv.setAdapter(adapter);
                    dialog.setCancelable(true);
                    dialog.show();
                }

            }
        });

        /*viewHolder.img_drop_down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = userData.get(position).getBouquet();
                String broadcaster = userData.get(position).getBroadcaster();
                final ProgressDialog progressDialog = new ProgressDialog(context);
                progressDialog.setCancelable(false); // set cancelable to false
                progressDialog.setMessage("Please Wait"); // set message
                progressDialog.show();
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl("http://59.179.24.45:8085/api/bouquet/"+name+"/"+broadcaster+"/")
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                BouquetSubInterface service = retrofit.create(BouquetSubInterface.class);

                Call<List<BouquetSubDto>> call = service.getsubBouquetDetails("");

                call.enqueue(new Callback<List<BouquetSubDto>>() {
                    @Override
                    public void onResponse(Call<List<BouquetSubDto>> call, retrofit2.Response<List<BouquetSubDto>> response) {

                        try {

                            List<BouquetSubDto> bouquetSubDtos = response.body();

                            //Log.d("Data", bouquetSubDtos.get(20).getBroadcaster());
                            viewHolder.img_drop_down.setVisibility(View.GONE);
                            viewHolder.iv_arrow_Collapse.setVisibility(View.VISIBLE);
                            viewHolder.rec_bottom.setVisibility(View.VISIBLE);
                            progressDialog.dismiss();
                            userRecyclerView = new BouquetSubRecycler(context,bouquetSubDtos);
                            viewHolder.rec_bottom.setAdapter(userRecyclerView);


                        } catch (Exception e) {
                            Log.d("onResponse", "There is an error");
                            progressDialog.dismiss();
                            e.printStackTrace();
                        }



                    }

                    @Override
                    public void onFailure(Call<List<BouquetSubDto>> call, Throwable t) {
                        progressDialog.dismiss();

                        Log.d("onFailure", t.toString());

                    }


                });
            }
        });
*/
        /*viewHolder.iv_arrow_Collapse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                viewHolder.iv_arrow_Collapse.setVisibility(View.GONE);
                viewHolder.img_drop_down.setVisibility(View.VISIBLE);
                viewHolder.rec_bottom.setVisibility(View.GONE);
            }
        });*/


    }



    @Override
    public int getItemCount() {
        return Data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView iv_delete,iv_info;
        private TextView tv_channel_name,tv_channel,tv_price,tv_hd,bouqet_name,bouqet_price,tv_language,tv_category,bouqet_channel_name;
        private ImageButton img_drop_down;
        private RecyclerView rec_bottom;
        private ImageView iv_arrow_Collapse;

        public MyViewHolder(View view) {
            super(view);
            final int TAG = position;
            // get the reference of item view's
            tv_channel_name     = (TextView) view.findViewById(R.id.tv_channel_name);
            tv_price     = (TextView) view.findViewById(R.id.tv_price);
            rec_bottom    = (RecyclerView)view.findViewById(R.id.rec_bottom);
            iv_arrow_Collapse = (ImageView)view.findViewById(R.id.iv_arrow_Collapse);
            iv_delete = (ImageView)view.findViewById(R.id.iv_delete);
            iv_info = (ImageView)view.findViewById(R.id.iv_info);
            LinearLayoutManager layoutManager = new LinearLayoutManager(context.getApplicationContext());
            rec_bottom.setLayoutManager(layoutManager);



        }
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }


}


