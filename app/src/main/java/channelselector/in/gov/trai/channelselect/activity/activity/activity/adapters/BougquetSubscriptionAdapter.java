package channelselector.in.gov.trai.channelselect.activity.activity.activity.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import channelselector.in.gov.trai.channelselect.R;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.Extras.Global;


public class BougquetSubscriptionAdapter extends RecyclerView.Adapter<BougquetSubscriptionAdapter.MyViewHolder> {

    private Context context;
    //private List<PaidChannelDTO> userData;
    private MyViewHolder viewHolder;
    private int position;
    int pos;
    List<String> parent_data;
    ArrayList<HashMap<String,String>> data;
    String check;
    private ArrayList<String>selectedNamefree =  new ArrayList<String>();
    private ArrayList<String>selectedNamepaid  = new ArrayList<String>();

    public BougquetSubscriptionAdapter(Context context, ArrayList<HashMap<String, String>> mdata, String bouquet) {
        this.context = context;
        this.data = mdata;
        this.check = bouquet;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
// infalte the item Layout
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_user, parent, false);
// set the view's size, margins, paddings and layout parameters
        MyViewHolder viewHolder = new MyViewHolder(v); // pass the view to View Holder



        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder viewHolder, final int position) {
        //Log.e("DATATEST=>",userData.get(position).getBroadcaster());
        this.viewHolder = viewHolder;
        this.position = position;
        final int row_index=position;
        viewHolder.tv_broadcaster.setText(data.get(position).get("Broadcaster"));
        viewHolder.tv_channel.setText(data.get(position).get("Channel"));
        viewHolder.tv_price.setText(context.getResources().getString(R.string.Rs)+" "+data.get(position).get("price"));
        viewHolder.tv_hd.setText(data.get(position).get("HD"));
        viewHolder.tv_language.setText(data.get(position).get("language"));
        viewHolder.tv_category.setText(data.get(position).get("category"));
        String path =(data.get(position).get("ImageUrl"));
        Glide.with(context)
                .asBitmap()
                .load(path)
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                        viewHolder.iv_channels.setImageBitmap(resource);
                    }
                });

        if(check.equalsIgnoreCase("bouguet")){

            viewHolder.remove_channel.setVisibility(View.GONE);
            viewHolder.tv_delete.setVisibility(View.GONE);
        }else{

            viewHolder.remove_channel.setVisibility(View.VISIBLE);
        }

        viewHolder.remove_channel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (row_index == position) {
                    viewHolder.mainLay.setBackgroundColor(Color.parseColor("#EAEAEA"));
                    viewHolder.add_channel.setVisibility(View.VISIBLE);
                    viewHolder.remove_channel.setVisibility(View.GONE);
                    String name = data.get(row_index).get("Channel");
                    if(check=="Free"){
                        selectedNamefree.add(name);
                        Global.selectedFree = selectedNamefree;
                    }else{

                        selectedNamepaid.add(name);
                        Global.selectedPiad = selectedNamepaid;
                    }


                } else {
                    viewHolder.mainLay.setBackground(ContextCompat.getDrawable(context, R.drawable.shadow));
                    /*if(selectedName!=null){
                        selectedName.remove(row_index);
                    }
*/
                }

            }
        });
        viewHolder.add_channel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (row_index == position) {
                    viewHolder.mainLay.setBackground(ContextCompat.getDrawable(context, R.drawable.shadow));
                    viewHolder.add_channel.setVisibility(View.GONE);
                    viewHolder.remove_channel.setVisibility(View.VISIBLE);
                    String name = data.get(row_index).get("Channel");
                    if(check=="Free"){
                        for(int i=0;i<selectedNamefree.size();i++){
                            if(selectedNamefree.get(i).equalsIgnoreCase(name)){
                                selectedNamefree.remove(name);
                                Global.selectedFree = selectedNamefree;
                            }
                        }
                    }else{

                        for(int i=0;i<selectedNamepaid.size();i++){
                            if(selectedNamepaid.get(i).equalsIgnoreCase(name)){
                                selectedNamepaid.remove(name);
                                Global.selectedPiad = selectedNamepaid;
                            }
                        }
                    }


                } else {
                    //viewHolder.mainLay.setBackground(ContextCompat.getDrawable(context, R.drawable.shadow));

                }

            }
        });

    }



    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView iv_channels,remove_channel,add_channel;
        private TextView tv_broadcaster,tv_channel,tv_price,tv_hd,bouqet_name,bouqet_price,tv_language,tv_category,bouqet_channel_name,tv_delete;
        private RelativeLayout mainLay;

        public MyViewHolder(View view) {
            super(view);
            int TAG = position;
            // get the reference of item view's
            iv_channels       = (ImageView)view.findViewById(R.id.iv_channels);
            tv_broadcaster     = (TextView) view.findViewById(R.id.tv_broadcaster);
            tv_channel     = (TextView) view.findViewById(R.id.tv_channel);
            tv_price     = (TextView) view.findViewById(R.id.tv_price);
            tv_hd     = (TextView) view.findViewById(R.id.tv_hd);
            tv_category = (TextView)view.findViewById(R.id.tv_category);
            bouqet_name     = (TextView) view.findViewById(R.id.bouqet_name);
            bouqet_price     = (TextView) view.findViewById(R.id.bouqet_price);
            tv_language     = (TextView) view.findViewById(R.id.tv_language);
            bouqet_channel_name = (TextView)view.findViewById(R.id.bouqet_channel_name);
            remove_channel = (ImageView)view.findViewById(R.id.remove_channel);
            mainLay = (RelativeLayout)view.findViewById(R.id.mainLay);
            add_channel = (ImageView)view.findViewById(R.id.add_channel);
            tv_delete  = (TextView)view.findViewById(R.id.tv_delete);

        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }




}



