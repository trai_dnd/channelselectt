package channelselector.in.gov.trai.channelselect.activity.activity.activity.classes;

import java.util.List;

import channelselector.in.gov.trai.channelselect.activity.activity.activity.DTO.ChannelID;

public class  ChannelRequest {
	
	 private List<ChannelID> added;
	 private List<ChannelID> deleted;
	 
	 
	public List<ChannelID> getAdded() {
		return added;
	}
	public void setAdded(List<ChannelID> added) {
		this.added = added;
	}
	public List<ChannelID> getDeleted() {
		return deleted;
	}
	public void setDeleted(List<ChannelID> deleted) {
		this.deleted = deleted;
	}
	 
	 
}
