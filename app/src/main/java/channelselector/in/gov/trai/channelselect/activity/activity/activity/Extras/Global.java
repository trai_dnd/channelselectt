package channelselector.in.gov.trai.channelselect.activity.activity.activity.Extras;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import channelselector.in.gov.trai.channelselect.activity.activity.activity.DTO.BouquetDto;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.DTO.BouquetSubDto;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.DTO.ChannelDto;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.DTO.MySubBouquet;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.DTO.MySubsChild;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.DTO.MySubsFree;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.DTO.MySubsPiad;

public class Global {
   


    //..................Constant are used in app....................................//

    public static ArrayList<ChannelDto> newDataFree;
    public static ArrayList<ChannelDto> newDataPaid;
    public static ArrayList<ChannelDto> setChannels;
    public static HashMap<String,ArrayList<ChannelDto>> channelData;
    public static HashMap<String,ArrayList<ChannelDto>> searchMainChannelData;
    //...Declare MySubscrption Data for further uses......//
    //public static ArrayList<MySubscriptionDto> userDataValue;
    public static ArrayList<HashMap<String,String>> confirmAddedData;

    public static String refreshToken;
    public static ArrayList<String> selectedFree;
    public static ArrayList<String> selectedPiad;


    public static String addtionPartFlag;
    public static ArrayList<HashMap<String,String>> mysubscfree;
    public static ArrayList<HashMap<String,String>> mysubscpaid;
    public static ArrayList<MySubsFree> freemysubs;
    public static ArrayList<MySubsPiad> paidmysubs;
    public static ArrayList<String> arrboquet;
    public static List<String> listheader;
    public static ArrayList<HashMap<String,String>> setfinal;
    public static ArrayList<HashMap<String,String>> confirmAddedBouquetData;
    public static ArrayList<HashMap<String, String>> confirmAddedBouquetDataHash;
    public static String ValidatedOtp;
    public static ArrayList<HashMap<String,String>> deletedbouquesubscription;
    public static ArrayList<MySubsFree> mySubscriptionFreeData;
    public static ArrayList<MySubsChild> DeletedBouquetData;
    //public static ArrayList<MySubsFree> mySubscriptionFreeData;
    //public static ArrayList<MySubsPaidDto> SubscriptionPaidData;
    public static List<BouquetDto> selectedBouquet;
    public static String addtionalBouquetFlag;
    public static int Freechannels;
    public static int PaidChannels;
    public static int TotalBouquet;
    public static String setAlterFree;
    public static String setAlterPaid;
    public static String setAlterBouquet;
    public static int addPaidChannel;
    public static int addFreeChannel;
    public static int addBouquetChannel;
    //public static List<BouquetDto> CheckBouquetPriceDto;
    public static double TotalSubscriptionPrice;
    public static double TotalAddedchannelprice;
    public static String CalculatedPrice;
    public static int channelCount;
    public static double calculatedChannelPrice;
    public static ArrayList<HashMap<String, String>> ChangesData;
    public static double paidchannelPrice;
    public static double bouquetprice;
    public static ArrayList<String> checkbouqueDups;
    public static int FlagMainActivity;
    public static double countcheckIncrement;
    public static double countcheckDecrement;
    public static ArrayList<HashMap<String,String>> addedchanges;
    public static String operatorname;
    public static ArrayList<MySubsPiad> SubscriptionPaidData;
    public static List<MySubsChild> BouquetsubTotal;
    public static List<BouquetDto> CheckBouquetPriceDto;
    public static List<BouquetSubDto> bouquetSubDtos;
    public static ArrayList<MySubsChild> userDataValue;
    public static List<MySubBouquet> bouquetData;
    public static String CustomerID;
    public static ArrayList<ChannelDto> freedtochannel;
    public static ArrayList<ChannelDto> paidDtochannel;
    public static List<BouquetDto> FreshBouquet;
    public static String success;
    public static HashMap<String,ArrayList<ChannelDto>> ChannelsMysubsData;
}
