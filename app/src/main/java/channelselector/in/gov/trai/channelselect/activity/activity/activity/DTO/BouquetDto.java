package channelselector.in.gov.trai.channelselect.activity.activity.activity.DTO;

public class BouquetDto {
    private String bouquet_id;
    private String bouquet;
    private String broadcaster;
    private String bouquetPrice;
    private String totalchannel;
    public Boolean isSelected;

    public String getBouquet_id() {
        return bouquet_id;
    }

    public void setBouquet_id(String bouquet_id) {
        this.bouquet_id = bouquet_id;
    }

    public String getBouquetPrice() {
        return bouquetPrice;
    }

    public void setBouquetPrice(String bouquetPrice) {
        this.bouquetPrice = bouquetPrice;
    }


    public Boolean getSelected() {
        return isSelected;
    }

    public void setSelected(Boolean selected) {
        isSelected = selected;
    }

    public String getTotalchannel() {
        return totalchannel;
    }

    public void setTotalchannel(String totalchannel) {
        this.totalchannel = totalchannel;
    }


    public String getBroadcaster() {
        return broadcaster;
    }

    public void setBroadcaster(String broadcaster) {
        this.broadcaster = broadcaster;
    }

    public String getBouquet() {
        return bouquet;
    }

    public void setBouquet(String bouquet) {
        this.bouquet = bouquet;
    }

    public String getBouquetprice() {
        return bouquetPrice;
    }

    public void setBouquetprice(String bouquetprice) {
        this.bouquetPrice = bouquetprice;
    }
}

