package channelselector.in.gov.trai.channelselect.activity.activity.activity.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import channelselector.in.gov.trai.channelselect.activity.activity.activity.fragments.Bouquet;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.fragments.Channels;

/**
 * Created by sarfraz on 10-07-2019.
 */
public class ViewPagerAdapter extends FragmentPagerAdapter {

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        if (position == 0)
        {
            fragment = new Channels();
        }
        else if (position == 1)
        {
            fragment = new Bouquet();
        }
        /*else if (position == 4)
        {
            fragment = new Checklist();
        }*/
        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = null;
        if (position == 0)
        {
            title = "Channels";
        }
        else if (position == 1)
        {
            title = "Bouquet";
        }
        return title;
    }
}
