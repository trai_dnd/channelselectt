package channelselector.in.gov.trai.channelselect.activity.activity.activity.adapters;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import channelselector.in.gov.trai.channelselect.R;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.Extras.Const;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.webservicehandler.HeaderGet;


public class PastRequestRecycler extends RecyclerView.Adapter<PastRequestRecycler.MyViewHolder> {

    private Context context;
    ArrayList<String>mData;
    private MyViewHolder viewHolder;
    private int position;
    String accessToken;
    String operator,url;

    public PastRequestRecycler(Context context, ArrayList<String> array_data) {

        this.context=context;
        this.mData = array_data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
// infalte the item Layout
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.past_request_row, parent, false);
// set the view's size, margins, paddings and layout parameters
        MyViewHolder viewHolder = new MyViewHolder(v); // pass the view to View Holder


        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder viewHolder, final int position) {

        this.viewHolder = viewHolder;
        this.position = position;
        final int row_index=position;
        viewHolder.tv_acknumber.setText("Acknoledgement No "+ " " +mData.get(position));
        accessToken = Prefs.getString(Const.ACCESSTOKEN, "token");
        operator    = Prefs.getString(Const.OPERATORNAME, "operator");
        viewHolder.tv_acknumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String acknumber = mData.get(row_index);
                if(operator.equalsIgnoreCase("Tata Sky")){

                    url = Const.getsubscriptionStatusTata + acknumber;
                }else{

                    url = Const.getsubscriptionStatus + acknumber;
                }
                getsubscriptionStatus(url,accessToken);

            }
        });


    }

    private void getsubscriptionStatus(String url, String accessToken) {

        if (Const.isNetworkAvailable(context)) {

            GetSubscriptionStatus o = new GetSubscriptionStatus(url, accessToken);
            o.execute();
        } else {
           Toast.makeText(context,"Please check internet connectivity", Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    public int getItemCount() {
        return mData.size();
    }



    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_acknumber;

        public MyViewHolder(View view) {
            super(view);

            tv_acknumber     = (TextView) view.findViewById(R.id.tv_acknumber);

        }
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }


    ProgressDialog progressDialog;

    private class GetSubscriptionStatus extends AsyncTask<String, Void, String> {
        String accessToken;
        String url;

        GetSubscriptionStatus(String url, String accessToken) {
            this.url = url;
            this.accessToken = accessToken;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.e("URL=>", "" + url);
            progressDialog = Const.loader(progressDialog, context);
        }

        @Override
        protected String doInBackground(String... strings) {

            HeaderGet o = new HeaderGet();
            String res = null;
            try {
                res = o.getData(url,accessToken);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return res;
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();

            Log.e("RES=>", "" + s);
            try {
                String result = s;
                JSONObject obj = new JSONObject(result);
                String status = obj.getString("status");
                if (status.equalsIgnoreCase("200")) {
                    String subscriptionStatus = obj.getString("subscriptionStatus");
                    String subscription_id    = obj.getString("subscription_id");
                    //String Date               = obj.getString("ActRejDate");
                    final Dialog dialog = new Dialog(context);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setCancelable(false);
                    dialog.setContentView(R.layout.row_dialogstatus);
                    TextView tv_subscrption = (TextView)dialog.findViewById(R.id.dialog_info);
                    tv_subscrption.setText("Subscription Id  " + "  " +subscription_id);
                    TextView text = (TextView) dialog.findViewById(R.id.txt_show);
                    text.setText("Subscription Status  " + "  " +subscriptionStatus);

                    Button dialogButton = (Button) dialog.findViewById(R.id.dialog_add);
                    dialogButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });


                    dialog.show();




                }else{
                    String message = obj.getString("message");

                   Toast.makeText(context,message,Toast.LENGTH_LONG).show();
                }
            }catch (Exception e) {

            }
        }
    }


}


