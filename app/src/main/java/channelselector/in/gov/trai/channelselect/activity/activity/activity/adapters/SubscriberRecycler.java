package channelselector.in.gov.trai.channelselect.activity.activity.activity.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import channelselector.in.gov.trai.channelselect.R;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.DTO.MySubBouquet;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.DTO.MySubsFree;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.DTO.MySubsPiad;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.Extras.Global;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.classes.MySubscription;

public class SubscriberRecycler extends RecyclerView.Adapter<SubscriberRecycler.MyViewHolder> {

    private Context context;

    private MyViewHolder viewHolder;
    private int position;
    int pos;
    List<String> parent_data;
    ArrayList<HashMap<String,String>>freeData;
    ArrayList<HashMap<String,String>> paidChannel;
    ArrayList<MySubsFree> freeDto;
    ArrayList<MySubsPiad> paidDto;
    List<MySubBouquet> bouquetName;
    private RecyclerViewSubSubription subAdapter;
     private MySubscriptionBouque buquetAdapter;

    public SubscriberRecycler(MySubscription context, ArrayList<MySubsFree> freeDto, ArrayList<MySubsPiad> paidDto, List<MySubBouquet> bouquetDtos, List<String> listDataHeader) {
        this.context = context;
        this.bouquetName = bouquetDtos;
        this.freeDto = freeDto;
        this.paidDto = paidDto;
        this.parent_data =listDataHeader;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
// infalte the item Layout
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_subscriber, parent, false);
// set the view's size, margins, paddings and layout parameters
        MyViewHolder viewHolder = new MyViewHolder(v); // pass the view to View Holder


        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder viewHolder, final int position) {
        //Log.e("DATATEST=>",userData.get(position).getBroadcaster());
        this.viewHolder = viewHolder;
        this.position = position;
        viewHolder.tv_choce_name.setText(parent_data.get(position));

        viewHolder.img_drop_down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(parent_data.get(position).equalsIgnoreCase("Free Channel")){
                    viewHolder.rec_free_Data_recycler.setVisibility(View.VISIBLE);
                    String Bouquet = "Free";
                    LinearLayoutManager layoutManager = new LinearLayoutManager(context.getApplicationContext());
                    viewHolder.rec_free_Data_recycler.setLayoutManager(layoutManager);
                    if(Global.SubscriptionPaidData!=null){
                        if(Global.SubscriptionPaidData.size()>0){
                            paidDto = Global.SubscriptionPaidData;
                        }else{
                            paidDto = paidDto;
                        }

                    }else{
                        //do nothing....
                        paidDto = paidDto;
                    }if(Global.mySubscriptionFreeData!=null){
                        if(Global.mySubscriptionFreeData.size()>0){
                            freeDto = Global.mySubscriptionFreeData;
                        }else{
                            freeDto = freeDto;
                        }

                    }else{
                        freeDto = freeDto;
                    }
                    subAdapter = new RecyclerViewSubSubription(context,freeDto,paidDto,Bouquet);
                    viewHolder.rec_free_Data_recycler.setAdapter(subAdapter);

                }if(parent_data.get(position).equalsIgnoreCase("Paid Channel")){
                    if(Global.SubscriptionPaidData!=null){
                        if(Global.SubscriptionPaidData.size()>0){
                            paidDto = Global.SubscriptionPaidData;
                        }else{
                            paidDto = paidDto;
                        }

                    }else{
                        //do nothing....
                        paidDto = paidDto;
                    }if(Global.mySubscriptionFreeData!=null){
                        if(Global.mySubscriptionFreeData.size()>0){
                            freeDto = Global.mySubscriptionFreeData;
                        }else{
                            freeDto = freeDto;
                        }

                    }else{
                        freeDto = freeDto;
                    }

                    viewHolder.rec_paid_Data_recycler.setVisibility(View.VISIBLE);
                    LinearLayoutManager layoutManager = new LinearLayoutManager(context.getApplicationContext());
                    viewHolder.rec_paid_Data_recycler.setLayoutManager(layoutManager);
                    String Bouquet = "Paid";
                        subAdapter = new RecyclerViewSubSubription(context,freeDto,paidDto,Bouquet);
                        viewHolder.rec_paid_Data_recycler.setAdapter(subAdapter);
                }
                    if(parent_data.get(position).equalsIgnoreCase("Bouquet")){
                        viewHolder.rec_bouquet_Data_recycler.setVisibility(View.VISIBLE);
                       // viewHolder.rec_paid_Data_recycler.setVisibility(View.GONE);
                        LinearLayoutManager layoutManager = new LinearLayoutManager(context.getApplicationContext());
                        viewHolder.rec_bouquet_Data_recycler.setLayoutManager(layoutManager);
                         buquetAdapter = new MySubscriptionBouque(context,bouquetName);
                        viewHolder.rec_bouquet_Data_recycler.setAdapter(buquetAdapter);


                }
                viewHolder.iv_arrow_Collapse.setVisibility(View.VISIBLE);
                viewHolder.img_drop_down.setVisibility(View.GONE);

                //viewHolder.rec_subscriber.setVisibility(View.VISIBLE);


            }
        });

        viewHolder.iv_arrow_Collapse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                viewHolder.iv_arrow_Collapse.setVisibility(View.GONE);
                viewHolder.img_drop_down.setVisibility(View.VISIBLE);
                if(parent_data.get(position).equalsIgnoreCase("Bouquet")){
                    viewHolder.rec_bouquet_Data_recycler.setVisibility(View.GONE);

                } if(parent_data.get(position).equalsIgnoreCase("Paid Channel")){
                    viewHolder.rec_paid_Data_recycler.setVisibility(View.GONE);

                } if(parent_data.get(position).equalsIgnoreCase("Free Channel")){
                    viewHolder.rec_free_Data_recycler.setVisibility(View.GONE);
                }
            }
        });


    }



    @Override
    public int getItemCount() {
        return parent_data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView iv_channels;
        private TextView tv_choce_name;
        private ImageButton img_drop_down,iv_arrow_Collapse;
        private RecyclerView rec_subscriber,rec_bouquet_Data_recycler,rec_paid_Data_recycler,rec_free_Data_recycler;

        public MyViewHolder(View view) {
            super(view);
            int TAG = position;
            // get the reference of item view's

            tv_choce_name     = (TextView) view.findViewById(R.id.tv_choce_name);
            img_drop_down    = (ImageButton)view.findViewById(R.id.img_drop_down);
            iv_arrow_Collapse    = (ImageButton)view.findViewById(R.id.iv_arrow_Collapse);
            rec_free_Data_recycler  = (RecyclerView)view.findViewById(R.id.rec_free_Data_recycler);
            rec_bouquet_Data_recycler = (RecyclerView)view.findViewById(R.id.rec_bouquet_Data_recycler);
            rec_paid_Data_recycler = (RecyclerView)view.findViewById(R.id.rec_paid_Data_recycler);

            /*rec_bouquet_Data_recycler.setLayoutManager(layoutManager);
            rec_paid_Data_recycler.setLayoutManager(layoutManager);*/

        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }




}

