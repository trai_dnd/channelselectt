package channelselector.in.gov.trai.channelselect.activity.activity.activity.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;

import java.util.List;

import channelselector.in.gov.trai.channelselect.R;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.DTO.BouquetSubDto;


public class BouquetSubRecycler extends RecyclerView.Adapter<BouquetSubRecycler.MyViewHolder> {

    private Context context;
    private List<BouquetSubDto> userData;
    private MyViewHolder viewHolder;
    private int position;
    int pos;

    public BouquetSubRecycler(Context context, List<BouquetSubDto> senderList) {
        this.context=context;
        this.userData=senderList;
    }



    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
// infalte the item Layout
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_user, parent, false);
// set the view's size, margins, paddings and layout parameters
        MyViewHolder viewHolder = new MyViewHolder(v); // pass the view to View Holder


        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder viewHolder, final int position) {
        //Log.e("DATATEST=>",userData.get(position).getBroadcaster());
        this.viewHolder = viewHolder;
        this.position = position;
        viewHolder.remove_channel.setVisibility(View.GONE);
        viewHolder.tv_delete.setVisibility(View.GONE);
        //viewHolder.checkbox.setVisibility(View.VISIBLE);
        viewHolder.tv_broadcaster.setText(userData.get(position).getBroadcaster());
        viewHolder.tv_channel.setText(userData.get(position).getChannel());
        viewHolder.tv_price.setText(context.getResources().getString(R.string.Rs)+" " +userData.get(position).getPrice());
        viewHolder.tv_hd.setText(userData.get(position).getHd());
        viewHolder.tv_language.setText(userData.get(position).getLanguage());
        viewHolder.tv_category.setText(userData.get(position).getCategory());
        String path = userData.get(position).getImageurl();
        Glide.with(context)
                .asBitmap()
                .load(path)
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                        viewHolder.iv_channels.setImageBitmap(resource);
                    }
                });

    }



    @Override
    public int getItemCount() {
        return userData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView iv_channels,remove_channel;
        private TextView tv_broadcaster,tv_channel,tv_price,tv_hd,bouqet_name,bouqet_price,tv_language,tv_category,bouqet_channel_name,tv_delete;
        private ImageButton img_btn_down,img_btn_up;
        private CheckBox checkbox;

        public MyViewHolder(View view) {
            super(view);
            int TAG = position;
            // get the reference of item view's
            iv_channels       = (ImageView)view.findViewById(R.id.iv_channels);
            tv_broadcaster     = (TextView) view.findViewById(R.id.tv_broadcaster);
            tv_channel     = (TextView) view.findViewById(R.id.tv_channel);
            tv_price     = (TextView) view.findViewById(R.id.tv_price);
            tv_hd     = (TextView) view.findViewById(R.id.tv_hd);
            tv_category = (TextView)view.findViewById(R.id.tv_category);
            bouqet_name     = (TextView) view.findViewById(R.id.bouqet_name);
            bouqet_price     = (TextView) view.findViewById(R.id.bouqet_price);
            tv_language     = (TextView) view.findViewById(R.id.tv_language);
            img_btn_down    = (ImageButton)view.findViewById(R.id.img_btn_down);
            img_btn_down    = (ImageButton)view.findViewById(R.id.img_btn_up);
            bouqet_channel_name = (TextView)view.findViewById(R.id.bouqet_channel_name);
            checkbox = (CheckBox)view.findViewById(R.id.checkbox);
            remove_channel = (ImageView)view.findViewById(R.id.remove_channel);
            tv_delete = (TextView)view.findViewById(R.id.tv_delete);


        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }





}

