package channelselector.in.gov.trai.channelselect.activity.activity.activity.classes;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import channelselector.in.gov.trai.channelselect.R;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.DTO.MySubBouquet;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.DTO.MySubsChild;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.DTO.MySubsFree;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.DTO.MySubsPiad;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.DTO.MySubscriptionDto;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.Extras.Const;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.Extras.Global;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.adapters.SubscriberRecycler;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.webservicehandler.HeaderGet;

public class MySubscription extends AppCompatActivity {
    private RecyclerView recycler_mysubscription;
    String customerId, mobile, Url;
    private RelativeLayout rel_bottom;
    List<String> listDataHeader;
    private Button btn_add, btn_skip;
    String FlagSucess, FlagRemove;
    String FlagOtp;
    private static TextView tv_free_channels;
    private static TextView tv_paid_channels;
    private static TextView tv_bouquet;
    private static TextView tv_price;
    /*private TextView tv_price;*/
    private LinearLayout linear_sub_text, linear_sub_value;
    int n = 0;
    int strnum = 0;
    double d = 0;
    double piad;
    double bouquets;
    double Total_price, MainPrice;
    private RelativeLayout mainLay;
    private RelativeLayout rel_top;
    HashMap<String, String> arr_changes = new HashMap<String, String>();
    String operator,url;
    String subscriptionid, accessToken;
    private List<MySubBouquet> bouquetDtos = new ArrayList<MySubBouquet>();
    private List<MySubsChild> bouquetSubDtos = new ArrayList<MySubsChild>();
    ArrayList<MySubsPiad> paidDto = new ArrayList<MySubsPiad>();
    ArrayList<MySubsFree> freeDto = new ArrayList<MySubsFree>();
    private SubscriberRecycler adapter;
    private ArrayList<MySubscriptionDto> userData = new ArrayList<MySubscriptionDto>();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mysubscription);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        subscriptionid = Prefs.getString(Const.SUBSCRIPTION_ID, "id");
        accessToken = Prefs.getString(Const.ACCESSTOKEN, "token");
        operator    = Prefs.getString(Const.OPERATORNAME, "operator");
        view();
        events();
        if(operator.equalsIgnoreCase("Tata Sky")){

             url = Const.getSubscriptionTata + subscriptionid;
        }else{

            url = Const.getSubscription + subscriptionid;
        }

        getSubscription(url,accessToken);

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    private void events() {

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(MySubscription.this,ChannelsBouquetActivity.class));
            }
        });
        btn_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(MySubscription.this,Review.class));
            }
        });
        rel_top.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(MySubscription.this,MyselectionActivity.class));
            }
        });
    }

    private void getSubscription(String url, String accessToken) {

        FlagOtp = Global.ValidatedOtp;
        FlagSucess = getIntent().getStringExtra("setSuccess");
        FlagRemove = Global.addtionPartFlag;
        ArrayList<MySubsPiad> paidDto = new ArrayList<MySubsPiad>();
        ArrayList<MySubsFree> freeDto = new ArrayList<MySubsFree>();
        ArrayList<String> arrboquet = new ArrayList<String>();
        List<String> listheader = new ArrayList<String>();


        if (FlagOtp != null && FlagOtp.equalsIgnoreCase("Validate")) {

            Global.ValidatedOtp = "set";
            Global.addtionPartFlag = "";
            if (Global.mysubscfree != null) {
                Global.mysubscfree.clear();
            }
            if (Global.mysubscpaid != null) {
                Global.mysubscpaid.clear();
            }
            if (Global.freemysubs != null) {
                Global.freemysubs.clear();
            }
            if (Global.paidmysubs != null) {
                Global.paidmysubs.clear();
            }
            if (Global.confirmAddedData != null) {
                Global.confirmAddedData.clear();
            }
            if (Global.channelData != null) {
                Global.channelData.clear();
            }
            if (Global.paidmysubs != null) {
                Global.paidmysubs.clear();
                paidDto = Global.paidmysubs;
            }
            if (Global.freemysubs != null) {
                Global.freemysubs.clear();
                freeDto = Global.freemysubs;
            }
            if (Global.listheader != null) {
                Global.listheader.clear();
                listheader = Global.listheader;
            }
            if (Global.arrboquet != null) {
                Global.arrboquet.clear();
                arrboquet = Global.arrboquet;
            }
            if (Global.confirmAddedBouquetData != null) {
                Global.confirmAddedBouquetData.clear();
            }
            if (Global.confirmAddedData != null) {
                Global.confirmAddedData.clear();
            }
            if (Global.confirmAddedBouquetDataHash != null) {
                Global.confirmAddedBouquetDataHash.clear();
            }
            if (Global.confirmAddedBouquetDataHash != null) {
                Global.confirmAddedBouquetDataHash.clear();
            }
            if (Global.confirmAddedBouquetData != null) {
                Global.confirmAddedBouquetData.clear();
            }
            if (Global.deletedbouquesubscription != null) {
                Global.deletedbouquesubscription.clear();
            }
            if (Global.DeletedBouquetData != null) {
                Global.DeletedBouquetData.clear();
            }
            if (Global.mysubscfree != null) {
                Global.mysubscfree.clear();
            }
            if (Global.mysubscpaid != null) {
                Global.mysubscpaid.clear();
            }
            if (Global.deletedbouquesubscription != null) {
                Global.deletedbouquesubscription.clear();
            }
            if (Global.mySubscriptionFreeData != null) {
                Global.mySubscriptionFreeData.clear();
            }
            if (Global.SubscriptionPaidData != null) {
                Global.SubscriptionPaidData.clear();
            }
            if (Global.TotalBouquet > 0) {
                Global.TotalBouquet = 0;
            }
            if (Global.Freechannels > 0) {
                Global.Freechannels = 0;
            }
            if (Global.PaidChannels > 0) {
                Global.PaidChannels = 0;
            }
            if (Global.addPaidChannel > 0) {
                Global.addPaidChannel = 0;
            }
            if (Global.addFreeChannel > 0) {
                Global.addFreeChannel = 0;
            }
            if (Global.addBouquetChannel > 0) {
                Global.addBouquetChannel = 0;
            }
            if (Global.TotalAddedchannelprice > 0) {
                Global.TotalAddedchannelprice = 0;
            }
            if (Global.ChangesData != null) {
                Global.ChangesData.clear();
            }
            if (Global.TotalSubscriptionPrice > 0) {
                Global.TotalSubscriptionPrice = 0;
            }
            if (Global.addedchanges != null) {
                Global.addedchanges.clear();
            }

            Global.ValidatedOtp = "done";
        if (Const.isNetworkAvailable(MySubscription.this)) {

            GetSubscription o = new GetSubscription(url, accessToken);
            o.execute();
        } else {
            Const.messageViewRe(mainLay, getString(R.string.network));
        }
    }else {
            if (FlagSucess != null && FlagSucess.equalsIgnoreCase("setSuccess")) {
                Global.addtionPartFlag = "";
                if (Global.mysubscfree != null) {
                    Global.mysubscfree.clear();
                }
                if (Global.mysubscpaid != null) {
                    Global.mysubscpaid.clear();
                }
                if (Global.freemysubs != null) {
                    Global.freemysubs.clear();
                }
                if (Global.paidmysubs != null) {
                    Global.paidmysubs.clear();
                }
                if (Global.confirmAddedData != null) {
                    Global.confirmAddedData.clear();
                }
                if (Global.channelData != null) {
                    Global.channelData.clear();
                }
                if (Global.paidmysubs != null) {
                    Global.paidmysubs.clear();
                    paidDto = Global.paidmysubs;
                }
                if (Global.freemysubs != null) {
                    Global.freemysubs.clear();
                    freeDto = Global.freemysubs;
                }
                if (Global.listheader != null) {
                    Global.listheader.clear();
                    listheader = Global.listheader;
                }
                if (Global.arrboquet != null) {
                    Global.arrboquet.clear();
                    arrboquet = Global.arrboquet;
                }
                if (Global.confirmAddedBouquetData != null) {
                    Global.confirmAddedBouquetData.clear();
                }
                if (Global.confirmAddedData != null) {
                    Global.confirmAddedData.clear();
                }
                if (Global.confirmAddedBouquetDataHash != null) {
                    Global.confirmAddedBouquetDataHash.clear();
                }
                if (Global.confirmAddedBouquetDataHash != null) {
                    Global.confirmAddedBouquetDataHash.clear();
                }
                if (Global.confirmAddedBouquetData != null) {
                    Global.confirmAddedBouquetData.clear();
                }
                if (Global.deletedbouquesubscription != null) {
                    Global.deletedbouquesubscription.clear();
                }
                if (Global.DeletedBouquetData != null) {
                    Global.DeletedBouquetData.clear();
                }
                if (Global.mysubscfree != null) {
                    Global.mysubscfree.clear();
                }
                if (Global.mysubscpaid != null) {
                    Global.mysubscpaid.clear();
                }
                if (Global.deletedbouquesubscription != null) {
                    Global.deletedbouquesubscription.clear();
                }
                if (Global.mySubscriptionFreeData != null) {
                    Global.mySubscriptionFreeData.clear();
                }
                if (Global.SubscriptionPaidData != null) {
                    Global.SubscriptionPaidData.clear();
                }
                if (Global.userDataValue != null) {
                    Global.userDataValue.clear();
                }
                if (Global.TotalBouquet > 0) {
                    Global.TotalBouquet = 0;
                }
                if (Global.Freechannels > 0) {
                    Global.Freechannels = 0;
                }
                if (Global.PaidChannels > 0) {
                    Global.PaidChannels = 0;
                }
                if (Global.addPaidChannel > 0) {
                    Global.addPaidChannel = 0;
                }
                if (Global.addFreeChannel > 0) {
                    Global.addFreeChannel = 0;
                }
                if (Global.addBouquetChannel > 0) {
                    Global.addBouquetChannel = 0;
                }
                if (Global.TotalAddedchannelprice > 0) {
                    Global.TotalAddedchannelprice = 0;
                }
                if (Global.ChangesData != null) {
                    Global.ChangesData.clear();
                }
                if (Global.TotalSubscriptionPrice > 0) {
                    Global.TotalSubscriptionPrice = 0;
                }
                if (Global.addedchanges != null) {
                    Global.addedchanges.clear();
                }


            }

            if(paidDto!=null && paidDto.size()>0){

                if(FlagSucess!=null && FlagSucess.equalsIgnoreCase("setSuccess")){
                    if (Const.isNetworkAvailable(MySubscription.this)) {

                        GetSubscription o = new GetSubscription(url, accessToken);
                        o.execute();
                    } else {
                        Const.messageViewRe(mainLay, getString(R.string.network));
                    }
                }else{
                    adapter = new SubscriberRecycler(MySubscription.this,freeDto,paidDto,Global.bouquetData, Global.listheader);
                    recycler_mysubscription.setAdapter(adapter);
                }

            }else {
                freeDto = Global.mySubscriptionFreeData;
                paidDto = Global.SubscriptionPaidData;
                arrboquet = Global.arrboquet;
                listheader = Global.listheader;
                rel_top.setVisibility(View.VISIBLE);
                rel_top.setVisibility(View.VISIBLE);
                linear_sub_text.setVisibility(View.VISIBLE);
                linear_sub_value.setVisibility(View.VISIBLE);
                if(Global.Freechannels==0){
                    tv_free_channels.setText("0"+"+"+25);
                }else{
                    tv_free_channels.setText(String.valueOf(Global.Freechannels)+"+"+25);
                }if(Global.PaidChannels==0){
                    tv_paid_channels.setText("0");
                }else{
                    tv_paid_channels.setText(String.valueOf(Global.PaidChannels));
                }if(Global.TotalBouquet==0){
                    tv_bouquet.setText("0");
                }else{
                    tv_bouquet.setText(String.valueOf(Global.TotalBouquet));
                }tv_price.setText(getResources().getString(R.string.Rs)+" "+String.valueOf(Global.TotalSubscriptionPrice));
                if(FlagSucess!=null && FlagSucess.equalsIgnoreCase("setSuccess")){
                    if (Const.isNetworkAvailable(MySubscription.this)) {

                        GetSubscription o = new GetSubscription(url, accessToken);
                        o.execute();
                    } else {
                        Const.messageViewRe(mainLay, getString(R.string.network));
                    }
                } else if(FlagRemove!=null && FlagRemove.equalsIgnoreCase("Remove Channel")){
                    adapter = new SubscriberRecycler(MySubscription.this,freeDto,paidDto,Global.bouquetData, Global.listheader);
                    recycler_mysubscription.setAdapter(adapter);
                    Global.addtionPartFlag="changednow";

                }else if(FlagRemove!=null && FlagRemove.equalsIgnoreCase("Remove Added Channel")){
                    adapter = new SubscriberRecycler(MySubscription.this,freeDto,paidDto,Global.bouquetData, Global.listheader);
                    recycler_mysubscription.setAdapter(adapter);
                    Global.addtionPartFlag="changednow";

                }else{

                    if (Const.isNetworkAvailable(MySubscription.this)) {

                        GetSubscription o = new GetSubscription(url, accessToken);
                        o.execute();
                    } else {
                        Const.messageViewRe(mainLay, getString(R.string.network));
                    }
                }

            }



        }
    }

    ProgressDialog progressDialog;

    private class GetSubscription extends AsyncTask<String, Void, String> {
        String accessToken;
        String url;
        String authToken;
        String Flag;
        String Total_Channel;
        String Total_Bouquet;
        String Amount;
        String AvailableBalance;
        String channel_id;
        String Broadcaster;
        String Channel ;
        String category;
        String language;
        String HD;
        String ImageUrl;

        GetSubscription(String url, String accessToken) {
            this.url = url;
            this.accessToken = accessToken;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.e("URL=>", "" + url);
            progressDialog = Const.loader(progressDialog, MySubscription.this);
        }

        @Override
        protected String doInBackground(String... strings) {

            HeaderGet o = new HeaderGet();
            String res = null;
            try {
                res = o.getData(url,accessToken);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return res;
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            if(paidDto!=null && paidDto.size()>0){
                paidDto.clear();
            }
            Log.e("RES=>", "" + s);
            try {
                String result = s;
                JSONObject obj = new JSONObject(result);
                String status = obj.getString("status");
                if (status.equalsIgnoreCase("200")) {
                    listDataHeader = new ArrayList<String>();
                    Total_Channel = obj.getString("total_channel");
                    Total_Bouquet = obj.getString("total_bouquet");
                    Amount = obj.getString("amount");
                    Prefs.putString(Const.Total_Channel, Total_Channel);
                    Prefs.putString(Const.Total_Bouquet, Total_Bouquet);
                    Prefs.putString(Const.Amount, Amount);
                    Global.TotalSubscriptionPrice = Double.parseDouble(Amount);
                    JSONArray jsonArray = obj.getJSONArray("bouquet");
                    JSONArray jsonArrayChannel = obj.getJSONArray("channels");
                    for (int i = 0; i < jsonArray.length(); i++) {

                        JSONObject object = jsonArray.getJSONObject(i);
                        String bouquet_id = object.getString("bouquet_id");
                        String bouquet = object.getString("bouquet");
                        String broadcaster = object.getString("broadcaster");
                        String bouquetPrice = object.getString("bouquet_price");
                        String totalchannel = object.getString("total_channel");

                        MySubBouquet bk = new MySubBouquet();
                        bk.setBouquet_id(bouquet_id);
                        bk.setBouquet(bouquet);
                        bk.setBroadcaster(broadcaster);
                        bk.setBouquetprice(bouquetPrice);
                        bk.setTotalchannel(totalchannel);
                        bk.setSelected(false);
                        bouquetDtos.add(bk);

                        if(object.has("bouquetchannel")){

                            JSONArray jsonArraymember = object.getJSONArray("bouquetchannel");
                            if(jsonArraymember.length()>0) {

                                for (int j = 0; j < jsonArraymember.length(); j++) {

                                    JSONObject products = jsonArraymember.getJSONObject(j);
                                    String channel_id = products.getString("channel_id");
                                    String broadcasterch = products.getString("broadcaster");
                                    String channel = products.getString("channel");
                                    String category = products.getString("category");
                                    String language = products.getString("language");
                                    String price = products.getString("price");
                                    String imageurl = products.getString("imageurl");
                                    String hd = products.getString("hd");
                                    if(imageurl.contains("http")){
                                        //do nothing...
                                    }else{
                                        imageurl = "http://59.179.24.45:8081"+"/"+products.getString("imageurl");
                                    }

                                    MySubsChild prodto = new MySubsChild();
                                    prodto.setChannel_id(channel_id);
                                    prodto.setBroadcaster(broadcasterch);
                                    prodto.setChannel(channel);
                                    prodto.setCategory(category);
                                    prodto.setLanguage(language);
                                    prodto.setPrice(price);
                                    prodto.setImageurl(imageurl);
                                    prodto.setHd(hd);
                                    prodto.setBouquet_id(bouquet_id);
                                    prodto.setBouquet_name(bouquet);
                                    prodto.setBouquet_price(bouquetPrice);
                                    bouquetSubDtos.add(prodto);

                                }
                            }else{

                                //do nothing...
                            }

                        }
                    }// end bouquet task start channel part.......................//

                    for(int i =0 ; i<jsonArrayChannel.length();i++){
                        JSONObject objchannel = jsonArrayChannel.getJSONObject(i);
                        String price   = objchannel.getString("price");

                        if(price.equalsIgnoreCase("0.0")){
                            channel_id  = objchannel.getString("channel_id");
                            Broadcaster = objchannel.getString("broadcaster");
                            Channel = objchannel.getString("channel");
                            category        = objchannel.getString("category");
                            language  = objchannel.getString("language");
                            price   = objchannel.getString("price");
                            ImageUrl   = objchannel.getString("imageurl");
                            HD        = objchannel.getString("hd");
                            if(ImageUrl.contains("http")){
                                //do nothing...
                            }else{
                                ImageUrl = "http://59.179.24.45:8081"+"/"+objchannel.getString("imageurl");
                            }

                            MySubsFree freeData = new MySubsFree();
                            freeData.setChannel_id(channel_id);
                            freeData.setBroadcaster(Broadcaster);
                            freeData.setChannel(Channel);
                            freeData.setCategory(category);
                            freeData.setLanguage(language);
                            freeData.setPrice(price);
                            freeData.setImageurl(ImageUrl);
                            freeData.setHd(HD);
                            freeData.setSelected(false);
                            freeDto.add(freeData);


                        }else{
                            channel_id  = objchannel.getString("channel_id");
                            Broadcaster = objchannel.getString("broadcaster");
                            Channel = objchannel.getString("channel");
                            category        = objchannel.getString("category");
                            language  = objchannel.getString("language");
                            price   = objchannel.getString("price");
                            ImageUrl   = objchannel.getString("imageurl");
                            HD        = objchannel.getString("hd");
                            if(ImageUrl.contains("http")){
                                //do nothing...
                            }else{
                                ImageUrl = "http://59.179.24.45:8081"+"/"+objchannel.getString("imageurl");
                            }


                            MySubsPiad paidDtos = new MySubsPiad();
                            paidDtos.setChannel_id(channel_id);
                            paidDtos.setBroadcaster(Broadcaster);
                            paidDtos.setChannel(Channel);
                            paidDtos.setCategory(category);
                            paidDtos.setLanguage(language);
                            paidDtos.setHd(HD);
                            paidDtos.setPrice(price);
                            paidDtos.setImageurl(ImageUrl);
                            paidDtos.setSelected(false);
                            paidDto.add(paidDtos);

                        }

                    }


                    listDataHeader.add("Free Channel");
                    listDataHeader.add("Paid Channel");
                    listDataHeader.add("Bouquet");
                    Global.freemysubs = freeDto;
                    Global.Freechannels = freeDto.size();
                    Global.mySubscriptionFreeData=freeDto;
                    Global.paidmysubs = paidDto;
                    Global.SubscriptionPaidData=paidDto;
                    Global.PaidChannels = paidDto.size();
                    Global.BouquetsubTotal= bouquetSubDtos;
                   // Global.arrboquet = bouquetDtos.size();
                    Global.bouquetData = bouquetDtos;
                    Global.TotalBouquet= bouquetDtos.size();
                    Global.listheader = listDataHeader;

                    int channel_count = paidDto.size()+25;
                    channel_count = channel_count+freeDto.size();
                    channel_count = channel_count+bouquetSubDtos.size();
                    Global.channelCount = channel_count;
                    Log.d("channelcount", String.valueOf(channel_count));

                    moveToMasterActivity(freeDto,paidDto,bouquetDtos,listDataHeader);


                }else{
                    String message = obj.getString("message");

                    Const.messageViewRe(mainLay, message);
                }
            }catch (Exception e) {

            }
        }
    }

    private void moveToMasterActivity(ArrayList<MySubsFree> freeDto, ArrayList<MySubsPiad> paidDto, List<MySubBouquet> bouquetDtos, List<String> listDataHeader) {

        getprice();
        adapter = new SubscriberRecycler(MySubscription.this,freeDto,paidDto,bouquetDtos, listDataHeader);
        recycler_mysubscription.setAdapter(adapter);

        rel_top.setVisibility(View.VISIBLE);
        linear_sub_text.setVisibility(View.VISIBLE);
        linear_sub_value.setVisibility(View.VISIBLE);
        if(Global.Freechannels==0){
            tv_free_channels.setText("0"+"+"+25);
        }else{
            tv_free_channels.setText(String.valueOf(Global.Freechannels)+"+"+25);
        }if(Global.PaidChannels==0){
            tv_paid_channels.setText("0");
        }else{
            tv_paid_channels.setText(String.valueOf(Global.PaidChannels));
        }if(Global.TotalBouquet==0){
            tv_bouquet.setText("0");
        }else{
            tv_bouquet.setText(String.valueOf(Global.TotalBouquet));
        }tv_price.setText(getResources().getString(R.string.Rs)+" "+String.valueOf(Global.TotalSubscriptionPrice));
    }

    private void getprice() {

        if(paidDto!=null && paidDto.size()>0) {
            for (int i = 0; i < paidDto.size(); i++) {
                String price_paid = paidDto.get(i).getPrice();
                double s = Double.parseDouble(price_paid);
                piad = piad + s;

            }
            Global.paidchannelPrice = piad;
        }
        if(bouquetDtos!=null && bouquetDtos.size()>0){
            for(int i =0;i<bouquetDtos.size();i++){
                String bouquet_price = bouquetDtos.get(i).getBouquetPrice();
                double s = Double.parseDouble(bouquet_price);
                bouquets = bouquets+s;
            }
            Global.bouquetprice = bouquets;
        }

        Double Total_price = Global.paidchannelPrice + Global.bouquetprice + 130;
        Double interest = Total_price*18/100;
        Double sub_price = Total_price+ interest;


        Global.TotalSubscriptionPrice = Double.parseDouble(new DecimalFormat("##.##").format(sub_price));
    }

    private void view() {

        recycler_mysubscription = (RecyclerView)findViewById(R.id.recycler_mysubscription);
        btn_add  = (Button)findViewById(R.id.btn_add);
        btn_skip  = (Button)findViewById(R.id.btn_skip);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recycler_mysubscription.setLayoutManager(layoutManager);
        tv_price    = (TextView)findViewById(R.id.tv_price);
        tv_free_channels = (TextView)findViewById(R.id.tv_free_channels);
        tv_paid_channels = (TextView)findViewById(R.id.tv_paid_channels);
        tv_bouquet = (TextView)findViewById(R.id.tv_bouquet);
        linear_sub_value = (LinearLayout)findViewById(R.id.linear_sub_value);
        linear_sub_text  = (LinearLayout)findViewById(R.id.linear_sub_text);
        linear_sub_text.setVisibility(View.INVISIBLE);
        linear_sub_value.setVisibility(View.INVISIBLE);
        mainLay = (RelativeLayout)findViewById(R.id.mainLay);
        rel_top = (RelativeLayout)findViewById(R.id.rel_top);


    }

    // call widget from adapter.......//

    public static TextView getFreeText() {

        return tv_free_channels;
    }

    public static TextView getPaidText() {

        return tv_paid_channels;
    }
    public static TextView getBouquetText() {

        return tv_bouquet;
    }

    public static TextView getTotalPrice() {

        return tv_price;
    }

}