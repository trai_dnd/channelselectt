package channelselector.in.gov.trai.channelselect.activity.activity.activity.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import channelselector.in.gov.trai.channelselect.R;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.DTO.MySubsFree;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.DTO.MySubsPiad;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.Extras.Const;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.Extras.Global;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.classes.MySubscription;


public class RecyclerViewSubSubription extends RecyclerView.Adapter<RecyclerViewSubSubription.MyViewHolder> {

    private Context context;
    //private List<PaidChannelDTO> userData;
    private MyViewHolder viewHolder;
    private int position;
    int pos;
    List<String> parent_data;
    ArrayList<HashMap<String,String>> data;
    String check;
    private ArrayList<String>selectedNamefree =  new ArrayList<String>();
    private ArrayList<String>selectedNamepaid  = new ArrayList<String>();
    ArrayList<MySubsFree> freeDto;
    ArrayList<MySubsPiad> paidDto;
    String bouquet;
    private ArrayList<HashMap<String,String>>confirmDatapaid= new ArrayList<HashMap<String,String>>();
    private ArrayList<HashMap<String,String>>confirmDatafree= new ArrayList<HashMap<String,String>>();
    int free;
    int paid ;
    int bouquetcount;
    int freeFlag =0;
    int paidFlag =0;

    public RecyclerViewSubSubription(Context context, ArrayList<MySubsFree> freeDto, ArrayList<MySubsPiad> paidDto, String bouquet) {
            this.context = context;
            this.freeDto = freeDto;
            this.paidDto = paidDto;
            this.check = bouquet;
            if(Global.mysubscfree!=null && Global.mysubscfree.size()>0){
                confirmDatafree= Global.mysubscfree;
            }if(Global.selectedFree!=null && Global.selectedFree.size()>0){
                selectedNamefree = Global.selectedFree;
            }if( Global.mysubscpaid!=null &&  Global.mysubscpaid.size()>0){
                confirmDatapaid =  Global.mysubscpaid;
            }if(Global.selectedPiad!=null && Global.selectedPiad.size()>0){
                selectedNamepaid = Global.selectedPiad;
             }if(Global.SubscriptionPaidData!=null && Global.SubscriptionPaidData.size()>0){
                this.paidDto = Global.SubscriptionPaidData;
        }if(Global.mySubscriptionFreeData!=null && Global.mySubscriptionFreeData.size()>0){
                this.freeDto = Global.mySubscriptionFreeData;
        }
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
// infalte the item Layout
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_sub_mysubcription, parent, false);
// set the view's size, margins, paddings and layout parameters
        MyViewHolder viewHolder = new MyViewHolder(v); // pass the view to View Holder



        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder viewHolder, final int position) {
        //Log.e("DATATEST=>",userData.get(position).getBroadcaster());
        this.viewHolder = viewHolder;
        this.position = position;
        final int row_index=position;
        //viewHolder.tv_broadcaster.setText(data.get(position).get("Broadcaster"));

        if(check.equalsIgnoreCase("Free")){

            if(freeDto.get(position).getSelected().equals(true)){
                viewHolder.mainLay.setBackgroundColor(Color.parseColor("#EAEAEA"));
                viewHolder.restore_channel.setVisibility(View.VISIBLE);
                viewHolder.tv_restore.setVisibility(View.VISIBLE);
                viewHolder.remove_channel.setVisibility(View.GONE);
                viewHolder.tv_delete.setVisibility(View.GONE);
                viewHolder.tv_channel.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                viewHolder.tv_price.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
            }else{

               // viewHolder.mainLay.setBackgroundColor(Color.parseColor("#EAEAEA"));
                viewHolder.restore_channel.setVisibility(View.GONE);
                viewHolder.tv_restore.setVisibility(View.GONE);
                viewHolder.remove_channel.setVisibility(View.VISIBLE);
                viewHolder.remove_channel.setVisibility(View.VISIBLE);
                viewHolder.tv_delete.setVisibility(View.VISIBLE);
                viewHolder.tv_channel.setPaintFlags(viewHolder.tv_channel.getPaintFlags() & ~Paint.STRIKE_THRU_TEXT_FLAG);
                viewHolder.tv_price.setPaintFlags(viewHolder.tv_price.getPaintFlags() & ~Paint.STRIKE_THRU_TEXT_FLAG);
            }
            viewHolder.tv_channel.setFilters(new InputFilter[]{ new InputFilter.LengthFilter(25) });
            viewHolder.tv_channel.setText(freeDto.get(position).getChannel());
            viewHolder.tv_language.setText(freeDto.get(position).getLanguage());
            String price = freeDto.get(position).getPrice();
            if(price.equalsIgnoreCase("0")){
                viewHolder.tv_price.setText("");
                viewHolder.tv_line.setVisibility(View.GONE);
            }else{
                viewHolder.tv_price.setText(context.getResources().getString(R.string.Rs)+" "+freeDto.get(position).getPrice());
               // viewHolder.tv_language.setText(freeDto.get(position).getLanguage());
            }

            /*viewHolder.tv_channel.setText(freeDto.get(position).getChannel());
            viewHolder.tv_price.setText(context.getResources().getString(R.string.Rs)+" "+freeDto.get(position).getPrice());*/
            //viewHolder.tv_language.setText(freeDto.get(position).get);
            String path =(freeDto.get(position).getImageurl());
            Glide.with(context)
                    .asBitmap()
                    .load(path)
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                            viewHolder.iv_channels.setImageBitmap(resource);
                        }
                    });

        }else{

            if(paidDto.get(position).getSelected().equals(true)){
                viewHolder.mainLay.setBackgroundColor(Color.parseColor("#EAEAEA"));
                viewHolder.restore_channel.setVisibility(View.VISIBLE);
                viewHolder.tv_restore.setVisibility(View.VISIBLE);
                viewHolder.remove_channel.setVisibility(View.GONE);
                viewHolder.tv_delete.setVisibility(View.GONE);
                viewHolder.tv_channel.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                viewHolder.tv_price.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
            }else{

                viewHolder.restore_channel.setVisibility(View.GONE);
                viewHolder.tv_restore.setVisibility(View.GONE);
                viewHolder.remove_channel.setVisibility(View.VISIBLE);
                viewHolder.remove_channel.setVisibility(View.VISIBLE);
                viewHolder.tv_delete.setVisibility(View.VISIBLE);
                viewHolder.tv_channel.setPaintFlags(viewHolder.tv_channel.getPaintFlags() & ~Paint.STRIKE_THRU_TEXT_FLAG);
                viewHolder.tv_price.setPaintFlags(viewHolder.tv_price.getPaintFlags() & ~Paint.STRIKE_THRU_TEXT_FLAG);
            }
            viewHolder.tv_channel.setFilters(new InputFilter[]{ new InputFilter.LengthFilter(25) });
            viewHolder.tv_channel.setText(paidDto.get(position).getChannel());
            String price = paidDto.get(position).getPrice();
            if(price.equalsIgnoreCase("0.0")){
                viewHolder.tv_price.setText("");
                viewHolder.tv_language.setText("");
            }else{
                viewHolder.tv_price.setText(context.getResources().getString(R.string.Rs)+" "+paidDto.get(position).getPrice());
                viewHolder.tv_language.setText(paidDto.get(position).getLanguage());
                viewHolder.tv_line.setVisibility(View.VISIBLE);
            }


            String path =(paidDto.get(position).getImageurl());
            Glide.with(context)
                    .asBitmap()
                    .load(path)
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                            viewHolder.iv_channels.setImageBitmap(resource);
                        }
                    });

        }

        /*if(check.equalsIgnoreCase("bouguet")){

            viewHolder.remove_channel.setVisibility(View.GONE);
        }else{

            viewHolder.remove_channel.setVisibility(View.VISIBLE);
        }*/

        viewHolder.remove_channel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String name = null;

                if (row_index == position) {
                    viewHolder.mainLay.setBackgroundColor(Color.parseColor("#EAEAEA"));
                    viewHolder.restore_channel.setVisibility(View.VISIBLE);
                    viewHolder.tv_restore.setVisibility(View.VISIBLE);
                    viewHolder.remove_channel.setVisibility(View.GONE);
                    viewHolder.tv_delete.setVisibility(View.GONE);
                    if(check=="Free"){
                         name = freeDto.get(row_index).getChannel();
                        viewHolder.tv_channel.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                        viewHolder.tv_price.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                        freeDto.get(row_index).setSelected(true);
                        String Broadcaster = freeDto.get(row_index).getBroadcaster();
                        String  Channel = freeDto.get(row_index).getChannel();
                        String category        =  freeDto.get(row_index).getCategory();
                        String price   = freeDto.get(row_index).getPrice();
                        String ImageUrl   = freeDto.get(row_index).getImageurl();
                        String Channel_Id = freeDto.get(row_index).getChannel_id();
                        String selection = String.valueOf(freeDto.get(row_index).getSelected());
                        HashMap<String,String>addedData = new HashMap<String,String>();
                        addedData.put("Broadcaster",Broadcaster);
                        addedData.put("Channel_Id",Channel_Id);
                        addedData.put("Channel",Channel);
                        addedData.put("category",category);
                        addedData.put("price",price);
                        addedData.put("ImageUrl",ImageUrl);
                        addedData.put("selection",selection);
                        confirmDatafree.add(addedData);
                        Global.mysubscfree = confirmDatafree;
                        selectedNamefree.add(name);
                        Global.selectedFree = selectedNamefree;
                        Global.mySubscriptionFreeData = freeDto;
                        if(MySubscription.getFreeText()!=null){

                            if(freeFlag==0){
                                free = Global.Freechannels;
                                String set_free = String.valueOf(free-1);
                                MySubscription.getFreeText().setText(set_free+"+"+25);
                                Global.Freechannels = Integer.parseInt(set_free);
                                    int count = Global.channelCount-1;
                                    Global.channelCount = count;
                                    Log.d("channel_count", String.valueOf(Global.channelCount));
                                freeFlag=1;
                            }else{
                                free = Global.Freechannels;
                                int count = Global.channelCount-1;
                                Global.channelCount = count;
                                Log.d("channel_count", String.valueOf(Global.channelCount));
                                String set_free = String.valueOf(free-1);
                                MySubscription.getFreeText().setText(set_free+"+"+25);
                                Global.Freechannels = Integer.parseInt(set_free);
                                if(Global.Freechannels==0){
                                    Global.setAlterFree = "setAlterFree";
                                }
                            }


                        }

                    }else{

                        name = paidDto.get(row_index).getChannel();
                        viewHolder.tv_channel.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                        viewHolder.tv_price.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                        paidDto.get(row_index).setSelected(true);
                        String Broadcaster = paidDto.get(row_index).getBroadcaster();
                        String  Channel = paidDto.get(row_index).getChannel();
                        String category        =  paidDto.get(row_index).getCategory();
                        String price   = paidDto.get(row_index).getPrice();
                        String ImageUrl   = paidDto.get(row_index).getImageurl();
                        String HD = paidDto.get(row_index).getHd();
                        String Channel_Id = paidDto.get(row_index).getChannel_id();
                        String selection = String.valueOf(paidDto.get(row_index).getSelected());
                        HashMap<String,String>addedData = new HashMap<String,String>();
                        addedData.put("Broadcaster",Broadcaster);
                        addedData.put("Channel_Id",Channel_Id);
                        addedData.put("Channel",Channel);
                        addedData.put("category",category);
                        addedData.put("price",price);
                        addedData.put("ImageUrl",ImageUrl);
                        addedData.put("selection",selection);
                        addedData.put("HD",HD);
                        confirmDatapaid.add(addedData);
                        Global.mysubscpaid = confirmDatapaid;
                        selectedNamepaid.add(name);

                        if(MySubscription.getPaidText()!=null){

                            if(paidFlag==0){
                                paid = Global.PaidChannels;
                                String set_paid = String.valueOf(paid-1);
                                MySubscription.getPaidText().setText(set_paid);
                                Global.PaidChannels = Integer.parseInt(set_paid);
                                if(HD.equalsIgnoreCase("HD")){
                                    int count = Global.channelCount-2;
                                    Global.channelCount = count;
                                    Log.d("channel_count", String.valueOf(Global.channelCount));
                                }else{
                                    int count = Global.channelCount-1;
                                    Global.channelCount = count;
                                    Log.d("channel_count", String.valueOf(Global.channelCount));
                                }
                                paidFlag=1;
                            }else{
                                paid =  Global.PaidChannels;
                                String set_paid = String.valueOf(paid-1);
                                MySubscription.getPaidText().setText(set_paid);
                                Global.PaidChannels = Integer.parseInt(set_paid);
                                if(Global.PaidChannels==0){
                                    Global.setAlterPaid = "setAlterPaid";
                                }
                            }

                        }
                       Global.selectedPiad = selectedNamepaid;
                       Global.SubscriptionPaidData = paidDto;
                        double s = Double.parseDouble(price);
                        Global.paidchannelPrice = Global.paidchannelPrice-s;
                        if(Global.channelCount<100){
                            double Totalchannelbouquet = Global.paidchannelPrice+ Global.bouquetprice;
                            double Final = 130+Totalchannelbouquet;
                            double gt = Final*18/100;
                            double pricetotal = Final+gt;
                            Global.TotalSubscriptionPrice = Double.parseDouble(new DecimalFormat("##.##").format(pricetotal));
                            if(MySubscription.getTotalPrice()!=null){
                                MySubscription.getTotalPrice().setText(String.valueOf(Double.parseDouble(new DecimalFormat("##.##").format(pricetotal))));
                            }
                        }else{
                            double n = Global.channelCount;
                            double pricechk  = Const.getuniquechannelcount(Global.channelCount);
                            /*double k = 20.0;
                            double gt = k*18/100;
                            double lgt = k+gt;

                                double pricetotal = Global.TotalSubscriptionPrice-lgt;
                                double gst = s*18/100;
                                double sn = s+gst;
                                double pricechk = pricetotal-sn;*/
                                Global.TotalSubscriptionPrice = Double.parseDouble(new DecimalFormat("##.##").format(pricechk));
                                if(MySubscription.getTotalPrice()!=null){
                                    MySubscription.getTotalPrice().setText(String.valueOf(Double.parseDouble(new DecimalFormat("##.##").format(pricechk))));


                               // Global.countcheckDecrement = p;

                            }

                        }


                    }


                } else {
                    viewHolder.mainLay.setBackground(ContextCompat.getDrawable(context, R.drawable.shadow));
                    /*if(selectedName!=null){
                        selectedName.remove(row_index);
                    }
*/
                }

            }
        });
        viewHolder.restore_channel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    String name;
                    if (row_index == position) {
                        viewHolder.mainLay.setBackground(ContextCompat.getDrawable(context, R.drawable.shadow));
                        viewHolder.restore_channel.setVisibility(View.GONE);
                        viewHolder.remove_channel.setVisibility(View.VISIBLE);
                        viewHolder.tv_restore.setVisibility(View.GONE);
                        viewHolder.tv_delete.setVisibility(View.VISIBLE);
                        viewHolder.tv_channel.setPaintFlags(viewHolder.tv_channel.getPaintFlags() & ~Paint.STRIKE_THRU_TEXT_FLAG);
                        viewHolder.tv_price.setPaintFlags(viewHolder.tv_price.getPaintFlags() & ~Paint.STRIKE_THRU_TEXT_FLAG);
                        //viewHolder.tv_price.setTypeface(null, Typeface.NORMAL);
                        if(check=="Free"){
                            name = freeDto.get(row_index).getChannel();
                            free = Global.Freechannels;
                            String set_free = String.valueOf(free+1);
                            MySubscription.getFreeText().setText(set_free+"+"+25);
                            Global.Freechannels = Integer.parseInt(set_free);
                            int count = Global.channelCount+1;
                            Global.channelCount = count;
                            Log.d("channel_count", String.valueOf(Global.channelCount));
                            if(confirmDatafree!=null && confirmDatafree.size()>0){
                                for(int i=0;i<confirmDatafree.size();i++){

                                    if(confirmDatafree.get(i).get("Channel").equalsIgnoreCase(name)){
                                        confirmDatafree.remove(i);
                                        freeDto.get(row_index).setSelected(false);
                                        Global.mysubscpaid = confirmDatapaid;
                                        Global.mySubscriptionFreeData = freeDto;

                                    }
                                }

                            }else{
                                if(Global.mySubscriptionFreeData!=null){
                                    //freeDto = Global.mySubscriptionFreeData;
                                    freeDto.get(row_index).setSelected(false);
                                }
                                 //do nothing....
                            }
                            if(selectedNamefree!=null && selectedNamefree.size()>0) {
                                for(int i =0 ; i<selectedNamefree.size(); i++) {
                                    if (selectedNamefree.get(i).equalsIgnoreCase(name)) {
                                        selectedNamefree.remove(name);
                                        freeDto.get(row_index).setSelected(false);
                                        Global.selectedFree = selectedNamefree;
                                    }
                                }
                            }else{
                                //do nothing....
                            }

                        }else{
                            name = paidDto.get(row_index).getChannel();
                            String price = paidDto.get(row_index).getPrice();
                            String HD = paidDto.get(row_index).getHd();
                            paid =  Global.PaidChannels;
                            String set_paid = String.valueOf(paid+1);
                            MySubscription.getPaidText().setText(set_paid);
                            Global.PaidChannels = Integer.parseInt(set_paid);
                            double s = Double.parseDouble(price);
                            Global.paidchannelPrice = Global.paidchannelPrice+s;
                            double gst = s*18/100;
                            if(Global.channelCount<100){
                                double Totalchannelbouquet = Global.paidchannelPrice+ Global.bouquetprice;
                                double Final = 130+Totalchannelbouquet;
                                double gt = Final*18/100;
                                double pricetotal = Final+gt;
                                Global.TotalSubscriptionPrice = Double.parseDouble(new DecimalFormat("##.##").format(pricetotal));
                                if(MySubscription.getTotalPrice()!=null){
                                    MySubscription.getTotalPrice().setText(String.valueOf(Double.parseDouble(new DecimalFormat("##.##").format(pricetotal))));
                                }

                            }else{
                                double n = Global.channelCount;
                                double pricechk  = Const.getuniquechannelcount(Global.channelCount);
                                /*double k = 20.0;
                                double gt = k*18/100;
                                double lgt = k+gt;

                                    double pricetotal = Global.TotalSubscriptionPrice+lgt;
                                    double pricechk = pricetotal+s+gst;*/
                                    Global.TotalSubscriptionPrice = Double.parseDouble(new DecimalFormat("##.##").format(pricechk));
                                    if(MySubscription.getTotalPrice()!=null){
                                        MySubscription.getTotalPrice().setText(String.valueOf(Double.parseDouble(new DecimalFormat("##.##").format(pricechk))));


                                }

                            }
                            if(HD.equalsIgnoreCase("HD")){
                                int count = Global.channelCount+2;
                                Global.channelCount = count;
                                Log.d("channel_count", String.valueOf(Global.channelCount));
                            }else{
                                int count = Global.channelCount+1;
                                Global.channelCount = count;
                                Log.d("channel_count", String.valueOf(Global.channelCount));
                            }

                            //Global.TotalSubscriptionPrice = pricechk;

                            if(confirmDatapaid!=null && confirmDatapaid.size()>0){
                                for (int i =0 ; i<confirmDatapaid.size(); i++){
                                    if(confirmDatapaid.get(i).get("Channel").equalsIgnoreCase(name)){
                                        paidDto.get(row_index).setSelected(false);
                                        confirmDatapaid.remove(i);
                                        Global.selectedPiad = selectedNamepaid;
                                        Global.SubscriptionPaidData = paidDto;

                                    }
                                }
                            }else{
                                if(Global.SubscriptionPaidData!=null){
                                    //freeDto = Global.mySubscriptionFreeData;
                                    paidDto.get(row_index).setSelected(false);
                                }
                            }
                            if(selectedNamepaid!=null && selectedNamepaid.size()>0){
                                for(int i =0 ; i<selectedNamepaid.size(); i ++){
                                    if(selectedNamepaid.get(i).equalsIgnoreCase(name)){
                                        selectedNamepaid.remove(i);
                                        Global.selectedPiad = selectedNamepaid;

                                    }
                                }
                            }else{
                                // do nothing...
                            }
                        }


                    } else {
                        //viewHolder.mainLay.setBackground(ContextCompat.getDrawable(context, R.drawable.shadow));

                }

            }
        });

    }



    @Override
    public int getItemCount() {

        if(check.equalsIgnoreCase("Free")){

            return freeDto.size();
        }else{

            return paidDto.size();
        }

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView iv_channels,remove_channel,add_channel,restore_channel;
        private TextView tv_broadcaster,tv_channel,tv_price,tv_hd,bouqet_name,bouqet_price,tv_language,tv_category,bouqet_channel_name,tv_restore,tv_delete,tv_line;
        private RelativeLayout mainLay;

        public MyViewHolder(View view) {
            super(view);
            int TAG = position;
            // get the reference of item view's
            iv_channels       = (ImageView)view.findViewById(R.id.iv_channels);
            tv_broadcaster     = (TextView) view.findViewById(R.id.tv_broadcaster);
            tv_channel     = (TextView) view.findViewById(R.id.tv_channel);
            tv_price     = (TextView) view.findViewById(R.id.tv_price);
            tv_hd     = (TextView) view.findViewById(R.id.tv_hd);
            tv_category = (TextView)view.findViewById(R.id.tv_category);
            bouqet_name     = (TextView) view.findViewById(R.id.bouqet_name);
            bouqet_price     = (TextView) view.findViewById(R.id.bouqet_price);
            tv_language     = (TextView) view.findViewById(R.id.tv_language);
            bouqet_channel_name = (TextView)view.findViewById(R.id.bouqet_channel_name);
            remove_channel = (ImageView)view.findViewById(R.id.remove_channel);
            mainLay = (RelativeLayout)view.findViewById(R.id.mainLay);
            add_channel = (ImageView)view.findViewById(R.id.add_channel);
            tv_restore = (TextView)view.findViewById(R.id.tv_restore);
            tv_delete = (TextView)view.findViewById(R.id.tv_delete);
            restore_channel = (ImageView)view.findViewById(R.id.restore_channel);
            tv_line = (TextView)view.findViewById(R.id.tv_line);

        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }




}


