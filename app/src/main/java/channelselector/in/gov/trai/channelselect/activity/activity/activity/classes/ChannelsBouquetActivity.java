package channelselector.in.gov.trai.channelselect.activity.activity.activity.classes;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import channelselector.in.gov.trai.channelselect.R;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.adapters.ViewPagerAdapter;

public class ChannelsBouquetActivity extends AppCompatActivity {
    ViewPager viewPager;
    ViewPagerAdapter viewPagerAdapter;
    TabLayout tabLayout;
    private static RelativeLayout rel_top;
    private static TextView tv_free_channels;
    private static TextView tv_paid_channels;
    private static TextView tv_bouquet;
    private static TextView tv_price;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_channel_bouquet);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        rel_top = (RelativeLayout)findViewById(R.id.rel_top);
        tv_price    = (TextView)findViewById(R.id.tv_price);
        tv_free_channels = (TextView)findViewById(R.id.tv_free_channels);
        tv_paid_channels = (TextView)findViewById(R.id.tv_paid_channels);
        tv_bouquet = (TextView)findViewById(R.id.tv_bouquet);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(viewPagerAdapter);
        rel_top.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(ChannelsBouquetActivity.this,MyselectionActivity.class));
            }
        });
        tabLayout.setupWithViewPager(viewPager);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }


    public static RelativeLayout getlayout_main() {

        return rel_top;
    }
    public static TextView getFreeText() {

        return tv_free_channels;
    }

    public static TextView getPaidText() {

        return tv_paid_channels;
    }
    public static TextView getBouquetText() {

        return tv_bouquet;
    }

    public static TextView getTotalPrice() {

        return tv_price;
    }
}
