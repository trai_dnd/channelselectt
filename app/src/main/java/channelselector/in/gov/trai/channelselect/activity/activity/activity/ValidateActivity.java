package channelselector.in.gov.trai.channelselect.activity.activity.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.SyncStateContract;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.util.ArrayList;

import channelselector.in.gov.trai.channelselect.R;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.Extras.Const;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.Extras.Global;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.adapters.Operator_Adapter;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.webservicehandler.GetWebService;

import static channelselector.in.gov.trai.channelselect.activity.activity.activity.Extras.Const.getPlatform;

public class ValidateActivity extends AppCompatActivity {

    private EditText ed_op_id, ed_op_auth_token;
    private Button submit;
    String subs_id;
    private ArrayList<String> operatorlist = new ArrayList<String>();
    private Operator_Adapter opt_adapt;
    private Spinner spinner_operator;
    String operatorName, subscriber_ID, authtoken;
    int Flag = 1;
    private RelativeLayout mainLay;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_validate);
        view();
        //getPlatform();
        events();
        setSpinnerAdapter();
    }

    private void getPlatform() {

        GetPlatforms o = new GetPlatforms();
        o.execute();

    }

    ProgressDialog progressDialog;

    private class GetPlatforms extends AsyncTask<String, Void, String> {
        String subscriber_id;
        String url;
        String authToken;
        String Flag;



        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            url = Const.getPlatform;

            Log.e("URL=>", "" + url);
            progressDialog = Const.loader(progressDialog, ValidateActivity.this);
        }

        @Override
        protected String doInBackground(String... strings) {

            GetWebService o = new GetWebService();
            String res = null;
            try {
                res = o.getData(url);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return res;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            Log.e("RES=>", "" + s);
            try {
                String result = s;
                if(s!=null){
                    JSONArray arrayplatform = new JSONArray(s);
                }else{

                }



            } catch (Exception e) {

            }

        }


    }

    private void setSpinnerAdapter() {
        operatorlist.add("Airtel Digital TV");
        operatorlist.add("Sun Direct TV");
        operatorlist.add("Tata Sky");
        operatorlist.add("Dish TV");
        operatorlist.add("Reliance Big Tv");
        operatorlist.add("Videocon D2h");
        Operator_Adapter opt_adapt = new Operator_Adapter(ValidateActivity.this, operatorlist);
        spinner_operator.setAdapter(opt_adapt);
    }

    private void events() {

        spinner_operator.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    operatorName = operatorlist.get(position);



            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //SecValue = getString(R.string.all);

            }
        });


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                validateOTP();

            }
        });
    }

    private void validateOTP() {
        String Result;
        subscriber_ID = ed_op_id.getText().toString();
        authtoken = ed_op_auth_token.getText().toString();
        if (authtoken.length() > 0 && subscriber_ID.length() > 0) {
            Const.messageViewRe(mainLay, getString(R.string.select));

        } else {
            if (authtoken.length() > 0) {
                Result = validation(operatorName, authtoken);
            } else {
                Result = validation(operatorName, subscriber_ID);
            }
            if (Result == "proceed") {
                if(operatorName.equalsIgnoreCase("Airtel Digital TV") || operatorName.equalsIgnoreCase("Tata Sky")){
                    //status = "error";
                    if (Const.isNetworkAvailable(ValidateActivity.this)) {

                        Prefs.putString(Const.OPERATORNAME, operatorName);

                        ValidateOTP o = new ValidateOTP(subscriber_ID, authtoken);
                        o.execute();
                    } else {
                        Const.messageViewRe(mainLay, getString(R.string.network));
                    }

                }else{

                    Const.messageViewRe(mainLay,"This is reference app currently only simulated for Airtel and Tata Sky");
                }



            } else {
                Const.messageViewRe(mainLay, getString(R.string.fieldarenotempty));
            }
        }


    }


    private class ValidateOTP extends AsyncTask<String, Void, String> {
        String subscriber_id;
        String url;
        String authToken;
        String Flag;

        ValidateOTP(String subscriber_ID, String authtoken) {
            this.subscriber_id = subscriber_ID;
            this.authToken = authtoken;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (subscriber_ID.length() > 0) {
                Global.CustomerID = subscriber_ID;
                if(operatorName.equalsIgnoreCase("Tata Sky")){
                    url = Const.validateOTPTata + subscriber_ID;
                }else{
                    url = Const.validateOTP + subscriber_ID;
                }

                Flag = "subsid";
            } else {
                if(operatorName.equalsIgnoreCase("Tata Sky")){
                    url = Const.validateauthTokenTata + authToken;
                }else{
                    url = Const.validateauthToken + authToken;
                }

                Flag = "authToken";
            }

            Log.e("URL=>", "" + url);
            progressDialog = Const.loader(progressDialog, ValidateActivity.this);
        }

        @Override
        protected String doInBackground(String... strings) {

            GetWebService o = new GetWebService();
            String res = null;
            try {
                res = o.getData(url);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return res;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            Log.e("RES=>", "" + s);
            try {
                String result = s;
                JSONObject object = new JSONObject(result);
                String status = object.getString("status");
                if (status.equalsIgnoreCase("200")) {
                    if (Flag.equalsIgnoreCase("subsid")) {
                        String message = object.getString("message");
                        String[] parts = message.split(" ");
                        String otp = parts[0];//"hello"
                        if (otp != null) {
                            showDailog(otp, subscriber_ID);
                        }
                        ed_op_id.setText("");
                        ed_op_auth_token.setText("");

                    } else {
                        String accessToken = object.getString("accessToken");
                        Prefs.putString(Const.ACCESSTOKEN, accessToken);
                        JSONArray jsonArray = object.getJSONArray("subscriber");
                        for(int i =0; i<jsonArray.length();i++){
                            JSONObject subsobj = jsonArray.getJSONObject(i);
                            for(int j =0; j<subsobj.length();j++){
                                String subscriptionId = subsobj.getString("subscriptionId");
                                String amount         = subsobj.getString("amount");
                                String type           = subsobj.getString("type");
                                String statuso        = subsobj.getString("status");
                                String activationDate = subsobj.getString("activationDate");
                                Prefs.putString(Const.SUBSCRIPTION_ID, subscriptionId);
                                Prefs.putString(Const.TOTAL_PRICE, amount);
                                Prefs.putString(Const.TYPE, type);
                            }
                        }
                        Intent intent = new Intent(ValidateActivity.this,MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                    }


                } else {
                    String message = object.getString("message");
                    ed_op_id.setText("");
                    ed_op_auth_token.setText("");
                    Const.messageViewRe(mainLay, message);
                    /*Intent intent = new Intent(ValidateActivity.this,MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();*/
                }

            } catch (Exception e) {

            }

        }


    }

    private void showDailog(final String otp, final String subscriber_id) {
        final Dialog dialog = new Dialog(ValidateActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custom_otp_dialog);

        final EditText ed__otp = (EditText) dialog.findViewById(R.id.ed__otp);
        ed__otp.setText(otp);
        Button dialogButton = (Button) dialog.findViewById(R.id.btn_submit);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String otp = ed__otp.getText().toString();

                getsubscriptiondetails(otp, subscriber_id);
                dialog.dismiss();

            }
        });
        Button dialogcancel = (Button) dialog.findViewById(R.id.btn_cancel);
        dialogcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ed_op_id.setText("");
                //startActivity(new Intent(OtpValidation.this, MainActivity.class));

                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void getsubscriptiondetails(String otp, String subscriber_id) {

        SuccedOTP o = new SuccedOTP(subscriber_ID, otp);
        o.execute();
    }


    private String validation(String operatorName, String customer_id) {

        String status = "";
        if (operatorName == null || operatorName.isEmpty()) {
            status = "error";
            Const.messageViewRe(mainLay, getString(R.string.selectoperator));
        } else if (customer_id.isEmpty()) {
            status = "error";
            Const.messageViewRe(mainLay, getString(R.string.fieldarenotempty));
        } else if (operatorName != null && customer_id != null) {
            status = "proceed";
        }
        return status;
    }


    private void view() {
        ed_op_id = (EditText) findViewById(R.id.ed_op_id);
        submit = (Button) findViewById(R.id.btn_validate);
        spinner_operator = (Spinner) findViewById(R.id.spinner_operator);
        mainLay = (RelativeLayout) findViewById(R.id.mainLay);
        ed_op_auth_token = (EditText) findViewById(R.id.ed_op_auth_token);
        if(Global.FlagMainActivity>0){
            Global.FlagMainActivity=0;
        }if(Global.countcheckIncrement>0){
            Global.countcheckIncrement=0;
        }if(Global.countcheckDecrement>0){
            Global.countcheckDecrement=0;
        }if(Global.setChannels!=null){
            Global.setChannels.clear();
        }if(Global.bouquetSubDtos!=null){
            Global.bouquetSubDtos.clear();
        }if(Global.FreshBouquet!=null){
            Global.FreshBouquet.clear();
        }


    }

    //........getsubscriptionid.............//

    private class SuccedOTP extends AsyncTask<String, Void, String> {
        String subscriber_id;
        String url;
        String otp;
        String Flag;

        SuccedOTP(String subscriber_ID, String otp) {
            this.subscriber_id = subscriber_ID;
            this.otp = otp;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
                    if(operatorName.equalsIgnoreCase("Tata Sky")){
                        url = Const.GetsubsIdTata + subscriber_id+"/"+otp;
                    }else{

                        url = Const.GetsubsId + subscriber_id+"/"+otp;
                    }


            Log.e("URL=>", "" + url);
            progressDialog = Const.loader(progressDialog, ValidateActivity.this);
        }

        @Override
        protected String doInBackground(String... strings) {

            GetWebService o = new GetWebService();
            String res = null;
            try {
                res = o.getData(url);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return res;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            Log.e("RES=>", "" + s);
            try {
                String result = s;
                JSONObject object = new JSONObject(result);
                String status = object.getString("status");
                if (status.equalsIgnoreCase("200")) {
                    JSONArray jsonArray = object.getJSONArray("subscriber");
                    String accessToken = object.getString("accessToken");
                    String tokenType   = object.getString("tokenType");
                    Prefs.putString(Const.ACCESSTOKEN, accessToken);
                    for(int i =0; i<jsonArray.length();i++){
                        JSONObject subsobj = jsonArray.getJSONObject(i);
                        for(int j =0; j<subsobj.length();j++){
                            String subscriptionId = subsobj.getString("subscriptionId");
                            String amount         = subsobj.getString("amount");
                            String type           = subsobj.getString("type");
                            String statuso        = subsobj.getString("status");
                            String activationDate = subsobj.getString("activationDate");
                            Prefs.putString(Const.SUBSCRIPTION_ID, subscriptionId);
                            Prefs.putString(Const.TOTAL_PRICE, amount);
                            Prefs.putString(Const.TYPE, type);
                        }
                    }

                    Intent intent = new Intent(ValidateActivity.this,MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();

                } else {
                    String message = object.getString("message");

                    Const.messageViewRe(mainLay, message);
                }

            } catch (Exception e) {

            }

        }


    }
}