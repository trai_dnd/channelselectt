package channelselector.in.gov.trai.channelselect.activity.activity.activity.classes;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import channelselector.in.gov.trai.channelselect.R;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.DTO.ChannelID;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.DTO.MySubsFree;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.DTO.MySubsPiad;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.DTO.MySubscriptionDto;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.Extras.Const;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.Extras.Global;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.adapters.PreviewRecycler;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.webservicehandler.GetResponse;

public class Review extends AppCompatActivity {
    private RecyclerView rec_preview;
    private Button btn_skip,btn_add;
    private PreviewRecycler adapter;
    List<String> listDataHeader;
    String Flag ,BouquetFlag;
    ArrayList<HashMap<String,String>> cofirmaddedData;
    ArrayList<HashMap<String,String>> confirmaddedBouquetDataHash;
    ArrayList<HashMap<String,String>> totalchannelDataHash;
    ArrayList<MySubscriptionDto> setData = new ArrayList<MySubscriptionDto>();
    ArrayList<MySubscriptionDto> setm = new ArrayList<MySubscriptionDto>();
    private ArrayList<MySubscriptionDto> userData ;
    private ArrayList<MySubscriptionDto> deletedData; ;
    private ProgressDialog prog_dialog;
    ArrayList<HashMap<String,String>> setfinal;
    ArrayList<HashMap<String,String>> checkdata = new ArrayList<HashMap<String,String>>();
    ArrayList<HashMap<String,String>> changesData = new ArrayList<HashMap<String,String>>();
    ArrayList<HashMap<String,String>> addedchanges = new ArrayList<HashMap<String,String>>();
    ArrayList<MySubscriptionDto> DeletedBouquetData = new ArrayList<MySubscriptionDto>();
    private Button btn_cancel;
    ArrayList<MySubsPiad> paidDto = new ArrayList<MySubsPiad>();
    ArrayList<MySubsFree> freeDto = new ArrayList<MySubsFree>();
    ArrayList<String> arrboquet = new ArrayList<String>();
    List<String> listheader =  new ArrayList<String>();
    private static TextView tv_free_channels;
    private static TextView tv_paid_channels;
    private static TextView tv_bouquet;
    private static TextView tv_price;
    private RelativeLayout mainLay;
    private RelativeLayout rel_top;
    String operator;
    ArrayList<String>array_data = new ArrayList<String>();
    ArrayList<String>put_data = new ArrayList<String>();
    Type type;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        operator = Prefs.getString(Const.OPERATORNAME, "operator");
        view();
        setAdapter();
        events();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Flag = Global.addtionPartFlag;
                if(Flag!=null && Flag.equalsIgnoreCase("Remove Added Channel")){
                    Intent intent = new Intent(Review.this, ChannelsBouquetActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                }else if(Flag!=null && Flag.equalsIgnoreCase("Remove Channel")){
                    Intent intent = new Intent(Review.this, MySubscription.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();

                }else{
                    finish();
                }

                return true;

        }
        return super.onOptionsItemSelected(item);
    }


    private void events() {

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Const.isNetworkAvailable(Review.this)){

                    if(Global.confirmAddedData!=null && Global.confirmAddedData.size()>0){
                        checkdata.addAll(Global.confirmAddedData);
                    }if(Global.setfinal!=null && Global.setfinal.size()>0){
                        checkdata.addAll(Global.setfinal);
                    }
                    if(checkdata!=null && checkdata.size()>0){
                        final Dialog dialog = new Dialog(Review.this); // Context, this, etc.
                        dialog.setContentView(R.layout.confirm_dilog);
                        dialog.setTitle(R.string.addchannel);
                        Button dialog_add = (Button)dialog.findViewById(R.id.dialog_yes);
                        dialog_add.setOnClickListener(new View.OnClickListener() {
                            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                                setSubscriptionTask();

                            }
                        });
                        Button dialog_cancel = (Button)dialog.findViewById(R.id.dialog_no);
                        dialog_cancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });
                        dialog.show();

                    }else{
                        final Dialog dialog = new Dialog(Review.this); // Context, this, etc.
                        dialog.setContentView(R.layout.custom_nodata);
                        dialog.setTitle(R.string.addchannel);
                        Button dialog_add = (Button)dialog.findViewById(R.id.dialog_yes);
                        dialog_add.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();

                            }
                        });

                        dialog.show();
                    }


                }else{
                    Const.messageViewRe(mainLay, getString(R.string.network));
                }

            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                gotomysub();


            }
        });

        btn_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                skipAction();



            }
        });

        rel_top.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(Review.this,MyselectionActivity.class));
            }
        });
    }

    private void skipAction() {

        Flag = Global.addtionPartFlag;
        BouquetFlag = Global.addtionalBouquetFlag;
        if(Flag!=null ){
            if(Flag.equalsIgnoreCase("Remove Added Channel")) {
                Intent intent = new Intent(Review.this, ChannelsBouquetActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }if(Flag.equalsIgnoreCase("Remove Channel")){
                Intent intent = new Intent(Review.this, MySubscription.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }else{
                if(BouquetFlag!=null ){
                    if(BouquetFlag.equalsIgnoreCase("Remove Added Bouquet")){
                        Intent intent = new Intent(Review.this, ChannelsBouquetActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                }else{
                    finish();
                }
            }
        }else if(BouquetFlag!=null ){
            if(BouquetFlag.equalsIgnoreCase("Remove Added Bouquet")){
                Intent intent = new Intent(Review.this, ChannelsBouquetActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }

        }else{
            finish();
        }
    }

    private void gotomysub() {

        //Global.ValidatedOtp = "Validate";
        Global.success = "setSuccess";
        Global.addtionPartFlag ="";
        if(Global.mysubscfree!=null){
            Global.mysubscfree.clear();
        }
        if(Global.mysubscpaid!=null){
            Global.mysubscpaid.clear();
        }if(Global.freemysubs!=null){
            Global.freemysubs.clear();
        }if(Global.paidmysubs!=null){
            Global.paidmysubs.clear();
        }if(Global.confirmAddedData!=null){
            Global.confirmAddedData.clear();
        }if(Global.channelData!=null){
            Global.channelData.clear();
        }
        if(Global.paidmysubs!=null){
            Global.paidmysubs.clear();
            paidDto = Global.paidmysubs;
        }if(Global.freemysubs!=null){
            Global.freemysubs.clear();
            freeDto = Global.freemysubs;
        }if(Global.listheader!=null){
            Global.listheader.clear();
            listheader = Global.listheader;
        }if(Global.arrboquet!=null){
            Global.arrboquet.clear();
            arrboquet = Global.arrboquet;
        }if(Global.confirmAddedBouquetData!=null) {
            Global.confirmAddedBouquetData.clear();
        }
        if(Global.confirmAddedData!=null){
            Global.confirmAddedData.clear();
        }if(Global.confirmAddedBouquetDataHash!=null){
            Global.confirmAddedBouquetDataHash.clear();
        }if(Global.confirmAddedBouquetDataHash!=null){
            Global.confirmAddedBouquetDataHash.clear();
        }if(Global.confirmAddedBouquetData!=null){
            Global.confirmAddedBouquetData.clear();
        }if(Global.deletedbouquesubscription!=null){
            Global.deletedbouquesubscription.clear();
        }if(Global.DeletedBouquetData!=null){
            Global.DeletedBouquetData.clear();
        }

        Intent intent = new Intent(Review.this, MySubscription.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("setSuccess","setSuccess");
        startActivity(intent);
        finish();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void setSubscriptionTask() {

        SetSubscriptionRequest setSubscriptionRequest = new SetSubscriptionRequest();


        ChannelRequest  chr=   new ChannelRequest();
        BouquetRequest  breq = new BouquetRequest();

      List<ChannelID> chreqADD= new ArrayList<ChannelID>();
      List<ChannelID> chreqDEL= new ArrayList<ChannelID>();

        List<BouquetID> bouqADD= new ArrayList<BouquetID>();
        List<BouquetID> bouqDEL= new ArrayList<BouquetID>();




                if(Global.setfinal!=null){
            for(int i =0; i<Global.setfinal.size();i++){
                if(Global.setfinal.get(i).get("Channel_Id")!=null) {
                    String channel_id = Global.setfinal.get(i).get("Channel_Id");
                    chreqDEL.add(new ChannelID(Long.parseLong(channel_id)));
                }
                if(Global.setfinal.get(i).get("Bouquet_Id")!=null) {
                    String bouquet_id = Global.setfinal.get(i).get("Bouquet_Id");
                    bouqDEL.add(new BouquetID(Long.parseLong(bouquet_id)));
                }
            }
        }
        /*if(Global.confirmAddedData!=null && Global.confirmAddedData.size()>0) {
            if (Global.confirmAddedData != null) {
                for (int i = 0; i < Global.confirmAddedData.size(); i++) {
                    for (int j = i + 1; j < Global.confirmAddedData.size(); j++) {
                        if (Global.confirmAddedData.get(i).get("Channel").equalsIgnoreCase(Global.confirmAddedData.get(j).get("Channel"))) {
                            Global.confirmAddedData.remove(j);
                            j--;
                        }
                    }
                }
            }
        }
*/
            if(Global.confirmAddedData!=null){
            for(int i =0; i<Global.confirmAddedData.size();i++){
                if(Global.confirmAddedData.get(i).get("Channel_Id")!=null){
                    String channel_id = Global.confirmAddedData.get(i).get("Channel_Id");
                    chreqADD.add(new ChannelID( Long.parseLong(channel_id)));
                }if(Global.confirmAddedData.get(i).get("Bouquet_Id")!=null) {
                    String bouquet_id = Global.confirmAddedData.get(i).get("Bouquet_Id");
                    bouqADD.add(new BouquetID(Long.parseLong(bouquet_id)));
                }

            }
        }

        chr.setAdded(chreqADD);
        chr.setDeleted(chreqDEL);

        breq.setAdded(bouqADD);
        breq.setDeleted(bouqDEL);

        setSubscriptionRequest.setChannels(chr);
        setSubscriptionRequest.setBouquet(breq);
        String subscription_id = Prefs.getString(Const.SUBSCRIPTION_ID, "id");
        setSubscriptionRequest.setSubscription_id(Long.parseLong(subscription_id));
        setSubscriptionRequest.setType("monthly");
        setSubscriptionRequest.setAmount(Global.TotalSubscriptionPrice);

        Gson gson = new GsonBuilder().create();
        JsonObject jsonObject = gson.toJsonTree(setSubscriptionRequest).getAsJsonObject();

       if(Const.isNetworkAvailable(Review.this)) {
           new UpdateTask().execute(jsonObject);
       }else {

           Const.messageViewRe(mainLay, getString(R.string.network));
       }

    }

    // setdata task work here......//

    public class UpdateTask extends AsyncTask<JsonObject, Integer, String> {

        String response;
        String Url;
        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();

            prog_dialog = new ProgressDialog(Review.this);
            prog_dialog.setMessage("Please wait....");
            prog_dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            prog_dialog.setCancelable(false);
            prog_dialog.show();

        }

        @Override
        protected String doInBackground(JsonObject... params) {
            try {
                    if(operator.equalsIgnoreCase("Tata Sky")){

                        Url = Const.setSubscriptionTata;
                    }else{

                        Url = Const.setSubscription;
                    }




                response = new GetResponse().putDataobject(Url, params[0]);

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return response;
        }



        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);

            prog_dialog.dismiss();

            if (result != null) {
                Log.e("RES=>", "" + result);
                try {
                    JSONObject object = new JSONObject(result);
                    String status = object.getString("status");
                    if (status.equalsIgnoreCase("200")) {
                        String message = object.getString("message");
                        String acknoledgement = object.getString("acknowledgmentNo");
                        Global.refreshToken = acknoledgement;
                        array_data = getArrayList("ArrayData");
                        if(array_data!=null ){
                            if( array_data.size()>=3) {

                                array_data.remove(0);
                                array_data.add(acknoledgement);
                            }else{
                                array_data.add(acknoledgement);
                            }
                        }else{
                            array_data = new ArrayList<String>();
                            array_data.add(acknoledgement);
                        }
                        saveArrayList(array_data, "ArrayData");
                        Const.messageViewRe(mainLay, message);
                        ViewDialog();
                    }else{

                        String message = object.getString("message");
                        if(Global.confirmAddedData!=null) {
                            Global.confirmAddedData.clear();
                        }if(Global.setfinal!=null){
                            Global.setfinal=setfinal;
                        }if(userData!=null){
                            userData.clear();
                        }if(setfinal!=null){
                            setfinal.clear();
                        }

                        Const.messageViewRe(mainLay, message);
                        gotomysub();
                    }


                    //ViewDialog();


                } catch (Exception e) {

                }

            } else {

                prog_dialog.dismiss();
            }

        }


    }

    public ArrayList<String> getArrayList(String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(Review.this);
        Gson gson = new Gson();
        String json = prefs.getString(key, null);
        Type type = new TypeToken<ArrayList<String>>() {
        }.getType();
        return gson.fromJson(json, type);
    }

    public void saveArrayList(ArrayList<String> list, String key){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(Review.this);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(key, json);
        editor.apply();     // This line is IMPORTANT !!!
    }

    private void ViewDialog() {

        final Dialog dialog = new Dialog(Review.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.customerdilog);

        TextView text = (TextView) dialog.findViewById(R.id.txt_show);
        text.setText("Acknowledgment No " + " " + " :" +" " +Global.refreshToken);

        Button dialogButton = (Button) dialog.findViewById(R.id.btn_save);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Global.confirmAddedData!=null) {
                    Global.confirmAddedData.clear();
                }if(Global.setfinal!=null){
                    Global.setfinal=setfinal;
                }if(userData!=null){
                    userData.clear();
                }if(setfinal!=null){
                    setfinal.clear();
                }if(Global.confirmAddedBouquetData!=null){
                    Global.confirmAddedBouquetData.clear();
                }if(Global.confirmAddedBouquetDataHash!=null){
                    Global.confirmAddedBouquetDataHash.clear();
                }if(Global.channelData!=null){
                    Global.channelData.clear();
                }if(Global.confirmAddedData!=null) {
                    Global.confirmAddedData.clear();
                }
                gotomysub();
                dialog.dismiss();
            }
        });


        dialog.show();

        //setAdapter();
    }


    private void setAdapter() {
        listDataHeader = new ArrayList<String>();
        listDataHeader.add("Dropped Current Subscription");
        listDataHeader.add("Added Channel / Bouquet");
        rel_top.setVisibility(View.VISIBLE);
        adapter = new PreviewRecycler(Review.this,listDataHeader);
        rec_preview.setAdapter(adapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Flag = Global.addtionPartFlag;
        if(Flag!=null && Flag.equalsIgnoreCase("Remove Added Channel")){
            Intent intent = new Intent(Review.this, ChannelsBouquetActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();

        }else if(Flag!=null && Flag.equalsIgnoreCase("Remove Channel")){
            Intent intent = new Intent(Review.this, MySubscription.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();

        }else{
            finish();
        }
    }
    private void view() {

        btn_skip = (Button)findViewById(R.id.btn_skip);
        btn_add = (Button)findViewById(R.id.btn_add);
        rec_preview = (RecyclerView)findViewById(R.id.rec_preview);
        btn_cancel = (Button)findViewById(R.id.btn_cancel);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rec_preview.setLayoutManager(layoutManager);
        tv_free_channels = (TextView)findViewById(R.id.tv_free_channels);
        tv_paid_channels = (TextView)findViewById(R.id.tv_paid_channels);
        tv_bouquet = (TextView)findViewById(R.id.tv_bouquet);
        tv_price = (TextView)findViewById(R.id.tv_price);
        mainLay = (RelativeLayout)findViewById(R.id.mainLay);
        rel_top = (RelativeLayout)findViewById(R.id.rel_top);

    }


    public static TextView getBouquetText() {

        return tv_bouquet;
    }
    public static TextView getFreeText() {

        return tv_free_channels;
    }

    public static TextView getPaidText() {

        return tv_paid_channels;
    }

    public static TextView getTv_price() {

        return tv_price;
    }
}
