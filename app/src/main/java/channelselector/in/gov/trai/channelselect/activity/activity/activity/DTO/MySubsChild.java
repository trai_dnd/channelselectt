package channelselector.in.gov.trai.channelselect.activity.activity.activity.DTO;

public class MySubsChild {

    private String bouquet_id;
    private String channel_id;
    private String broadcaster;
    private String channel;
    private String category;
    private String language;
    private String price;
    private String imageurl;
    private String hd;
    private String bouquet_name;
    private String bouquet_price;

    public String getBouquet_name() {
        return bouquet_name;
    }

    public void setBouquet_name(String bouquet_name) {
        this.bouquet_name = bouquet_name;
    }

    public String getBouquet_price() {
        return bouquet_price;
    }

    public void setBouquet_price(String bouquet_price) {
        this.bouquet_price = bouquet_price;
    }


    public String getBouquet_id() {
        return bouquet_id;
    }

    public void setBouquet_id(String bouquet_id) {
        this.bouquet_id = bouquet_id;
    }

    public String getChannel_id() {
        return channel_id;
    }

    public void setChannel_id(String channel_id) {
        this.channel_id = channel_id;
    }

    public String getBroadcaster() {
        return broadcaster;
    }

    public void setBroadcaster(String broadcaster) {
        this.broadcaster = broadcaster;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

    public String getHd() {
        return hd;
    }

    public void setHd(String hd) {
        this.hd = hd;
    }
}
