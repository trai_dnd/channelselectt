package channelselector.in.gov.trai.channelselect.activity.activity.activity.Extras;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import channelselector.in.gov.trai.channelselect.R;
import channelselector.in.gov.trai.channelselect.activity.activity.activity.DTO.ChannelDto;


public class Const {

    public static int TIMEOUT              = 16000;
    public static final String validateOTP = "http://59.179.24.45:8082/subscriber/doAuth/";
    public static final String validateauthToken ="http://59.179.24.45:8082/subscriber/doAuth/authtoken/";
    public static final String GetsubsId ="http://59.179.24.45:8082/subscriber/doAuth/";
    public static final String getSubscription ="http://59.179.24.45:8082/subscriber/getSubscription/";
    public static final String getChannels ="http://59.179.24.45:8082/provider/getChannels";
    public static final String getBouquet ="http://59.179.24.45:8082/provider/getBouquets";
    public static final String setSubscription ="http://59.179.24.45:8082/subscriber/setSubscription";
    public static final String getPlatform ="http://59.179.24.45:8084/api/platforms";
    public static final String getsubscriptionStatus ="http://59.179.24.45:8082/subscriber/getSubscriptionStatus/";


    //................... Tatasky........................................//

    public static final String validateOTPTata = "http://59.179.24.45:8083/subscriber/doAuth/";
    public static final String validateauthTokenTata ="http://59.179.24.45:8083/subscriber/doAuth/authtoken/";
    public static final String GetsubsIdTata ="http://59.179.24.45:8083/subscriber/doAuth/";
    public static final String getSubscriptionTata ="http://59.179.24.45:8083/subscriber/getSubscription/";
    public static final String getChannelsTata ="http://59.179.24.45:8083/provider/getChannels";
    public static final String getBouquetTaata ="http://59.179.24.45:8083/provider/getBouquets";
    public static final String setSubscriptionTata ="http://59.179.24.45:8083/subscriber/setSubscription";
    public static final String getsubscriptionStatusTata ="http://59.179.24.45:8083/subscriber/getSubscriptionStatus/";

    /********************  Preference Costants****************************/


    public static String ACCESSTOKEN                = "accessToken";
    public static String SUBSCRIPTION_ID            = "subscriptionId";
    public static String TOTAL_PRICE                = "amount";
    public static String TYPE                       = "type";
    public static String Total_Channel              = "Total_Channel";
    public static String Total_Bouquet              = "Total_Bouquet";
    public static String Amount                     = "Amount";
    public static String OPERATORNAME                   = "OPERATORNAME";
    public static String CURRENTLOCALE              = "locale";


    public static void saveTimeToSharedPreferences(SharedPreferences.Editor editor) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String currentDateandTime = sdf.format(new Date());
        Date date = null;
        try {
            date = sdf.parse(currentDateandTime);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.HOUR, 24);
            Date after24Hoursdateandtime = calendar.getTime();
            //editor.putLong(Global.timerFinishTime, after24Hoursdateandtime.getTime());
            saveValuesInSharedPreferences(editor);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public static void saveValuesInSharedPreferences(SharedPreferences.Editor editor) {
        editor.apply();
        editor.commit();
    }



    /*public static String getLanguage(Context context) {
        settings = PreferenceManager.getDefaultSharedPreferences(context);
        editor = settings.edit();
        String language = settings.getString(Global.chosenLanguage, "");
        return language;
    }*/



    public static String getStringResourceByName(Context context, String aString) {
        String packageName = context.getPackageName();
        int resId = context.getResources().getIdentifier(aString, "string", packageName);
        return context.getString(resId);
    }


    public static void messageView(LinearLayout layout, String message)
    {
        Snackbar snackbar = Snackbar
                .make(layout, message, Snackbar.LENGTH_LONG);

        TextView tv = (TextView) snackbar.getView().findViewById(android.support.design.R.id.snackbar_text);
        tv.setGravity(View.TEXT_ALIGNMENT_CENTER);
        snackbar.show();
    }

    public static void messageViewRe(RelativeLayout layout, String message)
    {
        Snackbar snackbar = Snackbar
                .make(layout, message, Snackbar.LENGTH_LONG);

        TextView tv = (TextView) snackbar.getView().findViewById(android.support.design.R.id.snackbar_text);
        tv.setGravity(View.TEXT_ALIGNMENT_CENTER);
        snackbar.show();
    }

    public static ProgressDialog loader(ProgressDialog progressDialog, Context context)
    {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(context.getString(R.string.loading)); // Setting Title
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);

        return progressDialog;
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static double getuniquechannelcount(double length) {

        if(length<100){
            return 130;
        }else{
            double l = 130 + Math.ceil((length-100)/25)*20;
            double Totalchannelbouquet = Global.paidchannelPrice+ Global.bouquetprice;
            double Final = l+Totalchannelbouquet;
            double gt = Final*18/100;
            double mainPrice = Final+gt;
            return mainPrice ;
        }
    }

}
