package channelselector.in.gov.trai.channelselect.activity.activity.activity.classes;

import java.util.List;

public class  BouquetRequest  {
	
	 private List<BouquetID> added;
	 private List<BouquetID> deleted;
	 
	 
	public List<BouquetID> getAdded() {
		return added;
	}
	public void setAdded(List<BouquetID> added) {
		this.added = added;
	}
	public List<BouquetID> getDeleted() {
		return deleted;
	}
	public void setDeleted(List<BouquetID> deleted) {
		this.deleted = deleted;
	} 
}
